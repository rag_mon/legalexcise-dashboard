<?php

namespace App\Models;

use Carbon\Carbon;
use Zizaco\Entrust\EntrustRole;

/**
 * Class Role
 *
 * @package App\Models
 *
 * @property int id
 * @property string name
 * @property string|null display_name
 * @property string|null description
 * @property Carbon created_at
 * @property Carbon updated_at
 */
class Role extends EntrustRole
{
    //
}