<?php

namespace App\Models;

use Zizaco\Entrust\EntrustPermission;

/**
 * Class Permission
 *
 * @package App\Models
 *
 * @property int id
 * @property string name
 * @property string|null display_name
 * @property string|null description
 */
class Permission extends EntrustPermission
{
    //
}