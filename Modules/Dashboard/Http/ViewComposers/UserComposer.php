<?php

namespace Modules\Dashboard\Http\ViewComposers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class UserComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        $user = $this->getUser();

        $view->with('user', $user);
    }

    /**
     * @return null|Model
     */
    private function getUser()
    {
        return Auth::guard('dashboard')->user();
    }
}