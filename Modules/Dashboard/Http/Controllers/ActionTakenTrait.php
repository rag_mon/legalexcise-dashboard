<?php

namespace Modules\Dashboard\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Modules\Dashboard\Models\ActionTaken;

/**
 * Trait ActionTakenTrait
 *
 * @package Modules\Dashboard\Http\Controllers
 */
trait ActionTakenTrait
{
    /**
     * Make action taken entity.
     *
     * @param array $data
     * @return ActionTaken
     */
    private function makeActionTakenEntity(array $data)
    {
        $data = array_filter($data);

        // Mutate dates
        $data['protocol_drawn_up'] = isset($data['protocol_drawn_up'])
            ? Carbon::createFromFormat(ActionTaken::PROTOCOL_DRAWN_UP_DATE_VIEW_FORMAT, $data['protocol_drawn_up'])
            : null;

        return ActionTaken::make($data);
    }

    /**
     * Make validator.
     *
     * @param array $data
     * @return \Illuminate\Validation\Validator
     */
    protected function makeValidator(array $data)
    {
        return Validator::make($data, [
            'confiscated_goods' => 'nullable|integer',
            'protocol_drawn_up' => 'nullable|date_format:d.m.Y',
            'financial_sanctions' => 'nullable|integer',
        ]);
    }
}