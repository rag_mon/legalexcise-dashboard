<?php

namespace Modules\Dashboard\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Collection;
use Modules\Dashboard\Charts\Builder\Builder as ChartBuilder;
use Modules\Dashboard\Models\ActionTaken;
use Modules\Dashboard\Models\Address;
use Modules\Dashboard\Models\Complaint;
use Modules\Dashboard\Models\Event;

class AnalyticsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // Statistic blocks
        $complaintsCount = Complaint::query()->count();
        $complaintReactionsCount = Event::type('dashboard::event.address.action.taken')->count();
        $addedSuspiciousCount = Address::suspicious()->count();
        $financialSanctionsSum = ActionTaken::complaints()->sum('financial_sanctions');

        //data for charts
        function chartComplaints(){
            $chartComplaintsData = Complaint::get(['id', 'created_at'])->groupBy(function($date) {
                return $date->created_at->format('m');
            });

            $chartComplaints = array();

            for($i = 1; $i <= 12; $i++){
                if(!empty($chartComplaintsData['0'.$i])) {
                    $chartComplaints[$i] = count($chartComplaintsData['0'.$i]);
                }else{
                    $chartComplaints[$i] = 0;
                }
            }

            return implode($chartComplaints, ',');
        }
        $chartComplaints = chartComplaints();

        function chartTreatments(){
            $chartTreatmentsData = ActionTaken::get(['id', 'created_at'])->groupBy(function($date) {
                return $date->created_at->format('m');
            });

            $chartTreatments = array();

            for($i = 1; $i <= 12; $i++){
                if(!empty($chartTreatmentsData['0'.$i])) {
                    $chartTreatments[$i] = count($chartTreatmentsData['0'.$i]);
                }else{
                    $chartTreatments[$i] = 0;
                }
            }

            return implode($chartTreatments, ',');
        }
        $chartTreatments = chartTreatments();

        return view('dashboard::analytics.index', compact('complaintsCount', 'complaintReactionsCount', 'addedSuspiciousCount', 'financialSanctionsSum', 'chartComplaints', 'chartTreatments'));
    }

    /**
     * Get chart analytics data.
     *
     * @param $dataProvider
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getData($dataProvider, Request $request)
    {
        $chartData = ChartBuilder::newInstance()
            ->setProvider(config("dashboard::chart.provider.aliases.{$dataProvider}"))
//            ->setTitle($request->input(''))
//            ->setSelector('')
//            ->setType($request->input('type'))
            ->build();

        return response()->json($chartData);
    }
}
