<?php

namespace Modules\Dashboard\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Dashboard\Models\Violation;

/**
 * Class ViolationController
 *
 * @package Modules\Dashboard\Http\Controllers
 */
class ViolationController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return response()->json(Violation::all()->toArray());
    }
}
