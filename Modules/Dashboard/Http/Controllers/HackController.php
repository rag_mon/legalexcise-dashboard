<?php

namespace Modules\Dashboard\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Dashboard\Models\ActionTaken;
use Modules\Dashboard\Models\Address;
use Modules\Dashboard\Models\Complaint;
use Modules\Dashboard\Models\Event;

class HackController extends Controller
{
    /**
     * Get counter data.
     *
     * @param $provider
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function getCounterData($provider, Request $request)
    {
        /** @var Carbon $from */
        /** @var Carbon $to */
        $from = $request->get('from') ? Carbon::createFromFormat('d.m.Y H:i:s', $request->get('from')) : null;
        $to = $request->get('to') ? Carbon::createFromFormat('d.m.Y H:i:s', $request->get('to')) : null;

        $scope = $this->makeScope($provider, $from, $to);
        $value = $scope instanceof Builder ? ($provider == 'financial_sanctions' ? $scope->sum('financial_sanctions') : $scope->count()) : $scope;

        return response($value);
    }

    /**
     * Make scope.
     *
     * @param string $provider
     * @param null|Carbon $filterFrom
     * @param null|Carbon $filterTo
     * @return \Illuminate\Database\Eloquent\Builder|null
     * @throws \Exception
     */
    private function makeScope($provider, $filterFrom = null, $filterTo = null)
    {
        $scope = null;

        switch ($provider) {
            // Сколько жалоб
            case 'complaints':
                $scope = Complaint::query();
                break;

            // Ответы на жалобы
            case 'complaint_reactions':
                $scope = Event::type('dashboard::event.address.action.taken');
                break;

            // Финансовые санкции
            case 'financial_sanctions':
                $scope = ActionTaken::query();
                break;

            // Добавленых красных
            case 'added_suspicious':
                $scope = Address::suspicious();
                break;

            default:
                throw new \Exception("Counter data provider \"$provider\" not support.");
        }

        if ($filterFrom) $scope->whereDate('created_at', '>=', $filterFrom);
        if ($filterTo) $scope->whereDate('created_at', '<=', $filterTo);

        return $scope;
    }
}
