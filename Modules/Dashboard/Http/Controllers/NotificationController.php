<?php

namespace Modules\Dashboard\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        /** @var Collection $notifications */
        $notifications = $this->guard()->user()->notifications;

        return view('dashboard::notification.index', compact('notifications'));
    }

    /**
     * Mark user notification as read.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function markAsRead()
    {
        /** @var Collection $notifications */
        $notifications = $this->guard()->user()->unreadNotifications;

        $notifications->markAsRead();

        return response()->json();
    }

    private function guard()
    {
        return Auth::guard('dashboard');
    }
}
