<?php

namespace Modules\Dashboard\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Modules\Dashboard\Events\Complaint\ComplaintDeleted;
use Modules\Dashboard\Events\Complaint\ComplaintTransformed;
use Modules\Dashboard\Events\Complaint\ComplaintUpdated;
use Modules\Dashboard\Models\ActionTaken;
use Modules\Dashboard\Models\Address;
use Modules\Dashboard\Models\Complaint;
use Modules\Dashboard\Models\Violation;
use Modules\Dashboard\Transformers\ComplaintTransformer;
use Yajra\DataTables\Facades\DataTables;

class ComplaintController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function index(Request $request)
    {
        if ($request->expectsJson()) {
            return DataTables::of(Complaint::query())
                ->setTransformer(new ComplaintTransformer())
                ->toJson();
        } else {
            $totalActionTaken = ActionTaken::complaints()->count();
            $totalFinancialSanction = ActionTaken::complaints()->sum('financial_sanctions');
            $totalConfiscatedGoods = ActionTaken::complaints()->sum('confiscated_goods');

            return view('dashboard::complaint.index', compact('totalActionTaken', 'totalFinancialSanction', 'totalConfiscatedGoods'));
        }
    }

    /**
     * Show the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        /** @var Complaint $complaint */
        $complaint = Complaint::where('id', $id)->firstOrFail();
        $violations = Violation::all();

        return view('dashboard::complaint.show', compact('complaint', 'violations'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        /** @var Complaint $address */
        $complaint = Complaint::where('id', $id)->firstOrFail();
        $violations = Violation::all();

        return view('dashboard::complaint.edit', compact('complaint', 'violations'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @param int $id
     * @return Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        $this->makeValidator($data)->validate();

        $data['verified_at'] = isset($data['verified_at']) ? Carbon::createFromFormat('d.m.Y', $data['verified_at']) : null;

        /** @var Complaint $complaint */
        $complaint = Complaint::where('id', $id)->firstOrFail();
        $complaint->update($data);

        // Associate address ID.
        if (!is_null($addressId = $request->input('address_id'))) {
            $complaint->addressEntry()->associate($addressId);
        } else {
            $complaint->address_id = null;
            $complaint->save();
        }

        event(new ComplaintUpdated($complaint));

        return response()->json();
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        /** @var Complaint $complaint */
        $complaint = Complaint::where('id', $id)->firstOrFail();
        $complaint->delete();

        event(new ComplaintDeleted($complaint));

        return response()->json();
    }

    /**
     * Transform complaint to address.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function transformToAddress($id)
    {
        /** @var Complaint $complaint */
        $complaint = Complaint::where('id', $id)->firstOrFail();

        /** @var Address $address */

        // Search for exists address entry from DB
        if (isset($complaint->address_id) || isset($complaint->address)) {
            $address = $this->getAddressByComplaintCreditals($complaint->address_id, $complaint->address);
        }

        // Address entry not found then create new from complaint data
        if (!isset($address)) {
            $address = $this->getAddressFromComplaint($complaint);
            $address->save();
        }

        $this->attachActionTakenToAddressFromComplaint($address, $complaint);
        $this->updateAddressPublicNotices($address);

        event(new ComplaintTransformed($complaint, $address));

        return response()->json($address->toArray());
    }

    /**
     * Update address public notices (resource action taken cache).
     *
     * @param Address $address
     */
    private function updateAddressPublicNotices(Address $address)
    {
        $publicNotices = array_fill_keys(ActionTaken::getActionKeys(), null);

        if ($actionTaken = $address->actionTaken()->orderByDesc('created_at')->first()) {
            $publicNotices['confiscated_goods'] = $actionTaken->confiscated_goods;
            $publicNotices['protocol_drawn_up'] = $actionTaken->protocol_drawn_up
                ? $actionTaken->protocol_drawn_up->format('d.m.Y')
                : null;
            $publicNotices['financial_sanctions'] = $actionTaken->financial_sanctions;
        }

        $address->publicNotices = $publicNotices;
        $address->save();
    }

    /**
     * Attach action taken to address from complaint.
     *
     * @param Address $address
     * @param Complaint $complaint
     */
    private function attachActionTakenToAddressFromComplaint($address, $complaint)
    {
        foreach ($complaint->actionTaken as $complaintActionTaken) {
            /** @var ActionTaken $complaintActionTaken */
            $addressActionTaken = ActionTaken::make($complaintActionTaken->toArray());

            $address->actionTaken()->save($addressActionTaken);
        }
    }

    /**
     * Get address entry by complaint credentials.
     *
     * @param null|int $addressId
     * @param null|string $address
     * @return Address
     */
    private function getAddressByComplaintCreditals($addressId = null, $address = null)
    {
        if (isset($addressId)) {
            return Address::where('id', $addressId)->first();
        } else {
            return Address::where('address', $address)->first();
        }
    }

    /**
     * Get address from complaint.
     *
     * @param Complaint $complaint
     * @return Address
     */
    private function getAddressFromComplaint(Complaint $complaint)
    {
        $address = new Address();
        $address->region_id = 0;
        $address->company = $complaint->company;
        $address->address = $complaint->address ?: '';
        $address->lng = $complaint->lng;
        $address->lat = $complaint->lat;
        $address->geocoding_at = $complaint->geocoding_at;
        $address->status = config('dashboard::complaint.after_transform_status', Address::STATUS_SUSPICIOUS);
        $address->license_type = Address::TYPE_MIXED;

        return $address;
    }

    /**
     * Make validator.
     *
     * @param array $data
     * @return \Illuminate\Validation\Validator
     */
    protected function makeValidator(array $data)
    {
        return Validator::make($data, [
            'np_code' => 'nullable|string|max:255',
            'to_code' => 'nullable|string|max:255',
            'status' => 'nullable|in:' . implode(',', Complaint::$statuses),
            'dfs_department_code' => 'nullable|string|max:191',
            'dfs_executor' => 'nullable|string|max:191',
            'name' => 'nullable|string|max:255',
            'telephone' => 'nullable|string|max:255',
            'company' => 'required|string|max:255',
            'email' => 'nullable|email',
            'message' => 'required|string',
            'type' => 'required|string',
//            'is_anonymously' => '',
            'address_id' => 'nullable|integer|exists:addresses,id',
            'address' => 'nullable|string',
            'lng' => 'nullable|numeric',
            'lat' => 'nullable|numeric',
            'protocol' => 'nullable|string',
            'violations' => 'nullable|string',
            'verified_at' => 'nullable',
            'spd_license' => 'string|max:191',
            'spd_address' => 'string|max:191',
            'spd_code' => 'string|max:191',
            'spd_name' => 'string|max:191',
            'comment' => 'string|max:10000',
        ]);
    }
}
