<?php

namespace Modules\Dashboard\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Modules\Dashboard\Events\Report\ReportDeleted;
use Modules\Dashboard\Events\Report\ReportDownloaded;
use Modules\Dashboard\Events\Report\ReportStored;
use Modules\Dashboard\Models\Report;
use Modules\Dashboard\Transformers\ReportTransformer;
use Yajra\DataTables\Facades\DataTables;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function index(Request $request)
    {
        if ($request->expectsJson()) {
            return DataTables::of(Report::query())
                ->setTransformer(new ReportTransformer())
                ->toJson();
        } else {
            return view('dashboard::report.index');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * Process analytics report request.
     *
     * @param  Request $request
     * @return Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $this->makeValidator($data)->validate();

        // Mutate dates
        $data['from_dt'] = isset($data['from_dt']) ? Carbon::createFromFormat(Report::VIEW_DATETIME_FORMAT, $data['from_dt']) : null;
        $data['to_dt'] = isset($data['to_dt']) ? Carbon::createFromFormat(Report::VIEW_DATETIME_FORMAT, $data['to_dt']) : null;

        /** @var Report $report */
        $report = Report::create($data);

        event(new ReportStored($report));

        if ($request->expectsJson()) {
            return response()->json();
        } else {
            return redirect()
                ->route('dashboard.report.show', [$report->id])
                ->with('success', __('dashboard::report.created_success', ['name' => $report->name]));
        }
    }

//    /**
//     * Show the specified resource.
//     *
//     * @param int id
//     * @return Response
//     */
//    public function show($id)
//    {
//        /** @var Report $report */
//        $report = Report::where('id', $id)->firstOrFail();
//
//        return view('dashboard::report.show', compact('report'));
//    }

    /**
     * Remove the specified resource from storage.
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Report $report */
        $report = Report::where('id', $id)->firstOrFail();

        event(new ReportDeleted($report));

        // TODO: remove report xlsx file after ReportDelete event dispatched. NO PRIORITY!

        return response()->json();
    }

    /**
     * Download report file.
     *
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function download($id)
    {
        /** @var Report $report */
        $report = Report::where('id', $id)->firstOrFail();

        event(new ReportDownloaded($report));

        return response()->download($report->filename, $report->name.'.xlsx');
    }

    /**
     * Make validator.
     *
     * @param array $data
     * @return \Illuminate\Validation\Validator
     */
    protected function makeValidator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'from_dt' => 'nullable|date_format:"d.m.Y H:i:s"',
            'to_dt' => 'nullable|date_format:"d.m.Y H:i:s"',
        ]);
    }

    protected function guard()
    {
        return Auth::guard('dashboard');
    }
}
