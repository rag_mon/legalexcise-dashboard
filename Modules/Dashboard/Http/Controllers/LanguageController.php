<?php

namespace Modules\Dashboard\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class LanguageController extends Controller
{
    /**
     * Get current application locale.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getCurrentLocale()
    {
        return response(App::getLocale());
    }

    /**
     * Get application support language keys list.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSupportList()
    {
        /** @var array $languageList */
        $languageList = config('dashboard.language.list');

        return response()->json($languageList);
    }

    /**
     * Change application language.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeLanguage(Request $request)
    {
        $locale = $request->input('lang');

        $this->guard()->user()->update(['locale' => $locale]);

        App::setLocale($locale);

        return response()->json();
    }

    /**
     * @return \Illuminate\Contracts\Auth\Guard
     */
    protected function guard()
    {
        return Auth::guard('dashboard');
    }
}
