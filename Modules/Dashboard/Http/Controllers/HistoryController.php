<?php

namespace Modules\Dashboard\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Dashboard\Models\Event;
use Modules\Dashboard\Transformers\EventTransformer;
use Yajra\DataTables\Facades\DataTables;

class HistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function index(Request $request)
    {
        if ($request->expectsJson()) {
            return DataTables::of(Event::query())
                ->setTransformer(new EventTransformer())
                ->toJson();
        } else {
            return view('dashboard::history.index');
        }
    }
}
