<?php

namespace Modules\Dashboard\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Dashboard\Models\ActionTaken;
use Modules\Dashboard\Models\Bookkeeping;
use Modules\Dashboard\Models\Complaint;

/**
 * Class DashboardController
 *
 * @package Modules\Dashboard\Http\Controllers
 */
class DashboardController extends Controller
{
    /**
     * Get dashboard homepage.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getHomepage()
    {
        /** @var Bookkeeping $bookkeeping */
        $bookkeeping = Bookkeeping::actual()->first();
        $totalComplaints = Complaint::count();
        $totalComplaintsVerificationImpossible = Complaint::verificationImpossible()->count();
        $totalActionTaken = ActionTaken::complaints()->count();
        $totalFinancialSanction = ActionTaken::complaints()->sum('financial_sanctions');
        $totalConfiscatedGoods = ActionTaken::complaints()->sum('confiscated_goods');

        return view('dashboard::homepage', compact(
            'bookkeeping',
            'totalComplaints',
            'totalComplaintsVerificationImpossible',
            'totalActionTaken',
            'totalFinancialSanction',
            'totalConfiscatedGoods'
        ));
    }
}
