<?php

namespace Modules\Dashboard\Http\Controllers\Address;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Dashboard\Events\Address\AddressUpdateReleased;
use Modules\Dashboard\Models\AddressUpdated;
use Modules\Dashboard\Transformers\AddressUpdatedTransformer;
use Yajra\DataTables\Facades\DataTables;

class UpdatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function index(Request $request)
    {
        if ($request->expectsJson()) {
            return DataTables::of(AddressUpdated::query())
                ->setTransformer(new AddressUpdatedTransformer())
                ->toJson();
        } else {
            return view('dashboard::address.updates.index');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        /** @var AddressUpdated $addressUpdate */
        $addressUpdate = AddressUpdated::create(['timestamp' => Carbon::now()]);

        event(new AddressUpdateReleased($addressUpdate));

        return response()->json($addressUpdate->toArray());
    }
}
