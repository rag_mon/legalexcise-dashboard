<?php

namespace Modules\Dashboard\Http\Controllers\Address;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Modules\Dashboard\Events\Address\AddressActionTaken;
use Modules\Dashboard\Events\Address\AddressDeleted;
use Modules\Dashboard\Events\Address\AddressStored;
use Modules\Dashboard\Events\Address\AddressUpdated;
use Modules\Dashboard\Http\Controllers\ActionTakenTrait;
use Modules\Dashboard\Models\ActionTaken;
use Modules\Dashboard\Models\Address;
use Modules\Dashboard\Transformers\AddressTransformer;
use Yajra\DataTables\Facades\DataTables;

/**
 * Class AddressController
 *
 * @package Modules\Dashboard\Http\Controllers\Address
 */
class AddressController extends Controller
{
    use ActionTakenTrait;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function index(Request $request)
    {
        if ($request->expectsJson()) {
            return DataTables::of(Address::query())
                ->setTransformer(new AddressTransformer())
                ->make(true);
        } else {
            return view('dashboard::address.index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('dashboard::address.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $this->makeValidator($data)->validate();

        /** @var Address $address */
        $address = Address::create($data);

        event(new AddressStored($address));

        $this->processAddressActionTaken($address, []);

        if ($request->expectsJson()) {
            return response()->json($address->toArray());
        } else {
            return redirect()
                ->route('dashboard.address.index')
                ->with('success', __('dashboard::address.created_success', ['address' => $address->address]));
        }
    }

    /**
     * Show the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        /** @var Address $address */
        $address = Address::where('id', $id)->firstOrFail();

        return view('dashboard::address.show', compact('address'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        /** @var Address $address */
        $address = Address::where('id', $id)->firstOrFail();

        return view('dashboard::address.edit', compact('address'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        $this->makeValidator($data)->validate();

        /** @var Address $address */
        $address = Address::where('id', $id)->firstOrFail();
        $prevPublicNotices = $address->publicNotices;
        $address->update($data);

        event(new AddressUpdated($address));

        $this->processAddressActionTaken($address, $prevPublicNotices);

        if ($request->expectsJson()) {
            return response()->json();
        } else {
            return redirect()
                ->route('dashboard.address.index')
                ->with('success', __('dashboard::address.updated_success', ['address' => $address->address]));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        /** @var Address $address */
        $address = Address::where('id', $id)->firstOrFail();
        $address->delete();

        event(new AddressDeleted($address));

        return response()->json();
    }

    /**
     * Make validator.
     *
     * @param array $data
     * @return \Illuminate\Validation\Validator
     */
    protected function makeValidator(array $data)
    {
        return Validator::make($data, [
            'id_code' => 'nullable|string|max:255',
            'address' => 'required|string|max:255',
            'lng' => 'nullable|numeric',
            'lat' => 'nullable|numeric',
            'license' => 'nullable|string|max:255',
            'company' => 'required|string|max:255',
            'company_type' => 'nullable|string|max:255',
            'license_start_at' => 'nullable',
            'license_end_at' => 'nullable',
            'license_type' => 'nullable|in:' . implode(',', Address::$types),
            'status' => 'required|in:' . implode(',', Address::$statuses),
            'public_notices' => 'nullable',
        ]);
    }

    /**
     * Detect and process address action taken.
     *
     * @param Address $address
     * @param array|null $prevPublicNotices
     * @return array|null
     */
    private function processAddressActionTaken(Address $address, $prevPublicNotices)
    {
        (!is_array($prevPublicNotices) || !count($prevPublicNotices)) && ($prevPublicNotices = array_fill_keys(ActionTaken::getActionKeys(), null));
        $currentPublicNotices = is_array($address->publicNotices)
            ? $address->publicNotices
            : array_fill_keys(ActionTaken::getActionKeys(), null);

        if (count($publicNoticesDiff = array_diff_assoc($prevPublicNotices, $currentPublicNotices))) {
            $address->actionTaken()->save($this->makeActionTakenEntity($currentPublicNotices));

            event(new AddressActionTaken($address));
        }

        return $publicNoticesDiff;
    }
}
