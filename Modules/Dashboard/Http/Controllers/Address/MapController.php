<?php

namespace Modules\Dashboard\Http\Controllers\Address;

use Illuminate\Routing\Controller;
use Illuminate\Support\Collection;
use Modules\Dashboard\Models\Address;

class MapController extends Controller
{
    /**
     * Get addresses list.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAddresses()
    {
        /** @var Collection $addresses */
        $addresses = Address::all();

        return response()->json($addresses->toArray());
    }
}
