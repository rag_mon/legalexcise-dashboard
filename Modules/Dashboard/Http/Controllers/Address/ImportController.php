<?php

namespace Modules\Dashboard\Http\Controllers\Address;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Modules\Dashboard\Events\Address\AddressesUploaded;
use Modules\Dashboard\Models\AddressImport;

class ImportController extends Controller
{
    const UPLOADED_FILE_KEY = 'uploaded_file';
    const TYPE_KEY = 'type';

    /**
     * ImportController constructor.
     */
    public function __construct()
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        throw new \RuntimeException('Not released yet!');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('dashboard::address.import.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->makeValidator($request->toArray())->validate();

        $originalFilename = $request->file(self::UPLOADED_FILE_KEY)->getClientOriginalName();

        if ($path = $request->file(self::UPLOADED_FILE_KEY)->store('addresses/imports')) {
            $absolutePath = storage_path("app/{$path}");
            $metaData = $this->getImportFileMetaData($absolutePath);
            $configs = $this->getImportConfigs($request->toArray());

            /** @var AddressImport $addressImport */
            $addressImport = AddressImport::create(array_merge(
                $request->toArray(),
                [
                    'uploaded_filename' => $originalFilename,
                    'storage_filename' => $absolutePath,
                    'meta_data' => json_encode($metaData, JSON_UNESCAPED_UNICODE),
                    'configs' => json_encode($configs, JSON_UNESCAPED_UNICODE),
                ]
            ));

            event(new AddressesUploaded($addressImport));

            return redirect()
                ->route('dashboard.address.import.create')
                ->with('success', "Файл \"{$originalFilename}\" успешно загружен и обрабатывается...");

        } else {
            return redirect()
                ->route('dashboard.address.import.create')
                ->with('error', "Ошибка загрузки файла \"{$originalFilename}\".");
        }
    }

    /**
     * Get import file meta data.
     *
     * @param string $filename
     * @return array
     */
    private function getImportFileMetaData($filename)
    {
        //TODO: release logic
        return [
            //
        ];
    }

    /**
     * Get import configs.
     *
     * @param array $data
     * @return array
     */
    private function getImportConfigs(array $data)
    {
        //TODO: release logic
        return [
            'type' => $data['type'],
        ];
    }

    /**
     * Show the specified resource.
     */
    public function show()
    {
        throw new \RuntimeException('Not released yet!');
    }

    /**
     * Make the validator instance.
     *
     * @param array $data
     * @return \Illuminate\Validation\Validator
     */
    protected function makeValidator(array $data)
    {
        return Validator::make($data, [
            self::TYPE_KEY => 'required|in:' . implode(',', AddressImport::$types),
            self::UPLOADED_FILE_KEY => 'required|file|mimes:xlsx',
        ]);
    }
}
