<?php

namespace Modules\Dashboard\Http\Controllers\Address;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Modules\Dashboard\Events\Address\CommentPosted;
use Modules\Dashboard\Models\Address;
use Modules\Dashboard\Models\AddressComment;

class CommentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param int $addressId
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request, $addressId)
    {
        $this->makeValidator($request->all())->validate();

        /** @var Address $address */
        /** @var AddressComment $comment */
        $address = Address::where('id', $addressId)->firstOrFail();
        $comment = $address->comments()->create($request->all());

        event(new CommentPosted($comment));

        return response()->json($comment->toArray());
    }

    /**
     * Make the validator instance.
     *
     * @param array $data
     * @return \Illuminate\Validation\Validator
     */
    protected function makeValidator(array $data)
    {
        return Validator::make($data, [
            'message' => 'required|string|max:1000',
        ]);
    }
}
