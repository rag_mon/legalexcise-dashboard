<?php

namespace Modules\Dashboard\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Modules\Dashboard\Events\Bookkeeping\BookkeepingUpdated;
use Modules\Dashboard\Models\Bookkeeping;

/**
 * Class BookkeepingController
 *
 * @package Modules\Dashboard\Http\Controllers
 */
class BookkeepingController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        /** @var Bookkeeping $bookkeeping */
        $bookkeeping = Bookkeeping::actual()->first();

        return view('dashboard::bookkeeping.create', compact('bookkeeping'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        /** @var Bookkeeping $bookkeeping */
        $bookkeeping = Bookkeeping::create($data);

        event(new BookkeepingUpdated($bookkeeping));

        return response()->json();
    }

    /**
     * Make validator.
     *
     * @param array $data
     * @return \Illuminate\Validation\Validator
     */
    protected function makeValidator(array $data)
    {
        return Validator::make($data, [
            'examination_of_excisable_goods' => 'nullable|numeric',
            'transferred_to_the_oy' => 'nullable|numeric',
            'sale_to_minors' => 'nullable|numeric',
        ]);
    }
}
