<?php

namespace Modules\Dashboard\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Dashboard\Models\IActionTaken;
use Symfony\Component\Routing\Exception\InvalidParameterException;

/**
 * Class ActionTakenController
 *
 * @package Modules\Dashboard\Http\Controllers
 */
class ActionTakenController extends Controller
{
    use ActionTakenTrait;

    /**
     * Post action taken.
     *
     * @param string $resourceType
     * @param int $resourceId
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postAction($resourceType, $resourceId, Request $request)
    {
        $this->validateResourceType($resourceType);
        $this->validateResourceId($resourceId);

        $data = $request->all();

        $resource = $this->findResourceEntity($resourceType, $resourceId);

        $resource->actionTaken()->save($this->makeActionTakenEntity($data));

        return response()->json();
    }

    /**
     * Find resource entity by type and id.
     *
     * @param string $resourceType
     * @param int $resourceId
     * @return Model|IActionTaken
     * @throws ModelNotFoundException
     */
    protected function findResourceEntity($resourceType, $resourceId)
    {
        $class = $this->getResourceClass($resourceType);

        return $class::where((new $class())->getKeyName(), $resourceId)->firstOrFail();
    }

    /**
     * Get resource class name by type.
     *
     * @param string $resourceType
     * @return string|null
     */
    private function getResourceClass($resourceType)
    {
        return config("dashboard.morph_map.$resourceType");
    }

    /**
     * Validate resource type.
     *
     * @param string $resourceType
     * @throws InvalidParameterException
     */
    private function validateResourceType($resourceType)
    {
        if (!array_key_exists($resourceType, config('dashboard.morph_map', []))) {
            throw new InvalidParameterException("Resource type \"$resourceType\" is not supported.");
        }
    }

    /**
     * Validate resource Id.
     *
     * @param int $resourceId
     * @throws InvalidParameterException
     */
    private function validateResourceId($resourceId)
    {
        if (!is_numeric($resourceId)) {
            throw new InvalidParameterException("Resource ID \"\" is invalid.");
        }
    }
}
