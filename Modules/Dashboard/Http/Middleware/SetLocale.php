<?php

namespace Modules\Dashboard\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Modules\Dashboard\Models\User;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        /** @var User $user */
        if ($user = $this->guard()->user()) {
            App::setLocale($user->locale);
        }

        return $next($request);
    }

    /**
     * @return
     */
    protected function guard()
    {
        return Auth::guard('dashboard');
    }
}
