<?php

Route::group(['middleware' => 'web', 'prefix' => '', 'namespace' => 'Modules\Dashboard\Http\Controllers'], function()
{
    Route::group(['middleware' => 'auth:dashboard'], function () {
        Route::get('/', 'DashboardController@getHomepage')->name('dashboard.homepage');

        // Address & address comments
        Route::group(['namespace' => 'Address'], function () {
            // Address
            Route::get('address/map', 'MapController@getAddresses')->name('dashboard.address.map');
            Route::resource('address', 'AddressController')
                ->name('index', 'dashboard.address.index')
                ->name('create', 'dashboard.address.create')
                ->name('edit', 'dashboard.address.edit');

            // Comments
            Route::resource('address.comment', 'CommentController', [
                'only' => ['store']
            ]);

            // DB updates
            Route::resource('updates', 'UpdatesController', [
                'only' => ['index', 'store']
            ])->name('index', 'dashboard.address.updates.index');

            // Import excel files
            Route::resource('imports', 'ImportController', [
                'only' => ['create', 'store']
            ])
                ->name('create', 'dashboard.address.import.create')
                ->name('store', 'dashboard.address.import.store');
        });

        // Complaint
        Route::post('complaint/transform/{id}', 'ComplaintController@transformToAddress');
        Route::post('complaint/{complaint}/action_taken', function ($complaint) {
            return redirect()->route('dashboard.action_taken.store', [
                'resource_type' => 'complaints',
                'resource_id' => $complaint,
            ], 308);
        })->name('dashboard.complaint.action_taken');
        Route::resource('complaint', 'ComplaintController', [
            'only' => ['index', 'update', 'destroy', 'show', 'edit']
        ])->name('index', 'dashboard.complaint.index');

        // Violation
        Route::get('violations', 'ViolationController@index')->name('dashboard.violation.index');

        // Action taken
        Route::post('action_taken/{resource_type}/{resource_id}', 'ActionTakenController@postAction')->name('dashboard.action_taken.store');

        // History
        Route::resource('history', 'HistoryController', [
            'only' => ['index']
        ])->name('index', 'dashboard.history.index');

        // Report
        Route::get('report/download/{id}', 'ReportController@download')->name('dashboard.report.download');
        Route::resource('report', 'ReportController', [
            'only' => ['index', 'store', 'destroy']
        ])->name('index', 'dashboard.report.index');

        // Notifications
        Route::get('notification', 'NotificationController@index')->name('dashboard.notification.index');
        // TODO: need to link controller method to this route
        Route::post('notification/mark_as_read')->name('dashboard.notification.mark_as_ready');

        // Analytics
        Route::resource('analytics', 'AnalyticsController', [
            'only' => ['index']
        ])->name('index', 'dashboard.analytics.index');

        // Language
        Route::get('/locale', 'LanguageController@getCurrentLocale');
        Route::get('/lang_list', 'LanguageController@getSupportList');
        Route::post('/change_lang', 'LanguageController@changeLanguage');

        // Bookkeeping
        Route::resource('bookkeeping', 'BookkeepingController', [
            'only' => [
                'create', 'store'
            ]
        ])->name('create', 'dashboard.bookkeeping.create')->name('store', 'dashboard.bookkeeping.store');


        /*-----------------------------------------------------------------------------------------------------------
         * HACK
         -----------------------------------------------------------------------------------------------------------*/
        Route::get('/hack/anal/count/{provider}', 'HackController@getCounterData');
        Route::post('/hack/anal/common', function () {
            return redirect()->route('dashboard.bookkeeping.store', [], 308);
        });

    });

    Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function () {
        // Authentication Routes...
        Route::get('login', 'LoginController@showLoginForm')->name('dashboard.login');
        Route::post('login', 'LoginController@login');
        Route::get('logout', 'LoginController@logout')->middleware('auth:dashboard')->name('dashboard.logout');

        // Password Reset Routes...
        Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('dashboard.password.request');
        Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('dashboard.password.email');
        Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('dashboard.password.reset');
        Route::post('password/reset', 'ResetPasswordController@reset');
    });
});
