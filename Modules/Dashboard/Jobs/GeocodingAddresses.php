<?php

namespace Modules\Dashboard\Jobs;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Modules\Dashboard\Events\Address\AddressesGeocoded;
use Modules\Dashboard\Models\Address;
use Modules\Dashboard\Models\AddressImport;
use Spatie\Geocoder\Facades\Geocoder;

class GeocodingAddresses implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var AddressImport
     */
    private $addressImport;

    /**
     * @var int
     */
    private $sleep;

    /**
     * Create a new job instance.
     *
     * @param AddressImport $addressImport
     */
    public function __construct(AddressImport $addressImport)
    {
        $this->addressImport = $addressImport;

        $this->sleep = 1;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        /** @var Address $addressEntry */
        foreach (Address::withoutCoordinates()->cursor() as $addressEntry) {
            // Similar address entry found with set of coordinates.
            if ($similarAddress = Address::address($addressEntry->address)->withCoordinates()->first()) {
                /** @var Address $similarAddress */

                Log::debug("Found similar textual address (ID: #{$similarAddress->id}) for entry ID #{$addressEntry->id}.");

                if ($addressEntry->update([
                    'lat' => $similarAddress->lat,
                    'lng' => $similarAddress->lng,
                ])) {
                    Log::debug("Address entry ID #{$addressEntry->id} coordinates was changed from similar address with ID #{$similarAddress->id}");
                } else {
                    Log::warning("Address entry ID #{$addressEntry->id} coordinates was not changed.");
                }
            } else {
                $this->getGeocodingWithAttempts($addressEntry);
            }
        }

        Log::debug("Finish fetching addresses geocoding coordinates for import ID: #{$this->addressImport->id}");

        event(new AddressesGeocoded($this->addressImport));
    }

    /**
     * Get GEO coordinates with attempts.
     *
     * @param Address $addressEntry
     */
    protected function getGeocodingWithAttempts(&$addressEntry)
    {
        $address = $addressEntry->address;

        do {
            /** @var Address $addressEntry */
            $address = $this->filterAddress($address);

            // Incorrect address
            if (!$address)
                break;

            $geocoding_payload = Geocoder::getCoordinatesForAddress($address);

            if ($isSuccess = $geocoding_payload['accuracy'] != 'result_not_found') {
                $addressEntry->update([
                    'geocoding_payload' => json_encode($geocoding_payload, JSON_UNESCAPED_UNICODE),
                    'lng' => $geocoding_payload['lng'],
                    'lat' => $geocoding_payload['lat'],
                    'geocoding_at' => Carbon::now(),
                ]);

                Log::debug("Address entry ID #{$addressEntry->id} coordinates was changed.");
            } else {
                Log::warning("Not found geocoding coordinates for address \"$address\" (Entry ID: #{$addressEntry->id}).");
            }

            // Script delay
            if ($this->sleep)
                sleep($this->sleep);

        } while (!$isSuccess);
    }

    /**
     * Prepare text address for geocoding request.
     *
     * @param string $address
     * @return bool|string
     */
    private function filterAddress($address)
    {
        return ($index = strrpos($address, ',')) >= 0 ? substr($address, 0, $index) : null;
    }
}
