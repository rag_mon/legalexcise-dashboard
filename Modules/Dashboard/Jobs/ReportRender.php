<?php

namespace Modules\Dashboard\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Database\Query\Builder;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Modules\Dashboard\Events\Report\ReportRendered;
use Modules\Dashboard\Models\ActionTaken;
use Modules\Dashboard\Models\Bookkeeping;
use Modules\Dashboard\Models\Complaint;
use Modules\Dashboard\Models\Report;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Exception as SpreadsheetException;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ReportRender implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    const CELL_TOTAL_COMPLAINTS = 'A7';
    const CELL_TOTAL_EXAMINATION_OF_EXCISABLE_GOODS  = 'B7';
    const CELL_TOTAL_TRANSFERRED_TO_THE_OY = 'C7';
    const CELL_TOTAL_SALE_TO_MINORS = 'D7';
    const CELL_TOTAL_COMPLAINTS_VERIFICATION_IMPOSSIBLE = 'E7';
    const CELL_TOTAL_ACTION_TAKEN = 'F7';
    const CELL_TOTAL_FINANCIAL_SANCTION = 'G7';
    const CELL_TOTAL_CONFISCATED_GOODS = 'H7';

    /**
     * @var Report
     */
    protected $report;

    /**
     * @var string
     */
    protected $inputFileName;

    /**
     * @var string
     */
    protected $inputFileType;

    /**
     * @var string
     */
    protected $outputFileName;

    /**
     * @var string
     */
    protected $outputFileType;

    /**
     * @var string|array
     */
    protected $sheetName;

    /**
     * Create a new job instance.
     *
     * @param Report $report
     */
    public function __construct(Report $report)
    {
        $this->report = $report;

        $this->report->status = 'rendering';
        $this->report->save();

        $this->inputFileName = resource_path('excel/report_template.xlsx');
        $this->inputFileType = 'Xlsx';
        $this->outputFileName = storage_path('reports/' . str_random() . '.xlsx');
        $this->outputFileType = 'Xlsx';
        $this->sheetName = 'Лист3';
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws Exception
     * @throws SpreadsheetException
     */
    public function handle()
    {
        try {
            // Load report template
            $reader = IOFactory::createReader($this->inputFileType);
            $reader->setReadDataOnly(false);
            $reader->setLoadSheetsOnly($this->sheetName);
            $spreadsheet = $reader->load($this->inputFileName);

            // Set report values
            $this->setWorksheetValues($spreadsheet->getActiveSheet(), $this->getValues());

            // Store report file to storage
            $writer = IOFactory::createWriter($spreadsheet, $this->outputFileType);
            $writer->save($this->outputFileName);

            // Update report entry data
            $this->report->filename = $this->outputFileName;
            $this->report->status = 'ready';
            $this->report->save();

            event(new ReportRendered($this->report));

        } catch (SpreadsheetException $e) {
            throw $e;
        } catch (Exception $e) {
            throw $e;
        }
    }

    private function setWorksheetValues(Worksheet $worksheet, array $values)
    {
        $worksheet->setCellValue(self::CELL_TOTAL_COMPLAINTS, $values['totalComplaints']);
        $worksheet->setCellValue(self::CELL_TOTAL_EXAMINATION_OF_EXCISABLE_GOODS, $values['examination_of_excisable_goods']);
        $worksheet->setCellValue(self::CELL_TOTAL_TRANSFERRED_TO_THE_OY, $values['transferred_to_the_oy']);
        $worksheet->setCellValue(self::CELL_TOTAL_SALE_TO_MINORS, $values['sale_to_minors']);
        $worksheet->setCellValue(self::CELL_TOTAL_COMPLAINTS_VERIFICATION_IMPOSSIBLE, $values['totalComplaintsVerificationImpossible']);
        $worksheet->setCellValue(self::CELL_TOTAL_ACTION_TAKEN, $values['totalActionTaken']);
        $worksheet->setCellValue(self::CELL_TOTAL_FINANCIAL_SANCTION, $values['totalFinancialSanction']);
        $worksheet->setCellValue(self::CELL_TOTAL_CONFISCATED_GOODS, $values['totalConfiscatedGoods']);
    }

    protected function getValues()
    {
        /** @var Bookkeeping $bookkeeping */
        $bookkeeping = $this->applyScopeFilters(Bookkeeping::actual())->first();
        
        $examination_of_excisable_goods = isset($bookkeeping) ? $bookkeeping->examination_of_excisable_goods : '0';
        $transferred_to_the_oy = isset($bookkeeping) ? $bookkeeping->transferred_to_the_oy : '0';
        $sale_to_minors = isset($bookkeeping) ? $bookkeeping->sale_to_minors : '0';
        $totalComplaints = $this->applyScopeFilters(Complaint::query())->count();
        $totalComplaintsVerificationImpossible = $this->applyScopeFilters(Complaint::verificationImpossible())->count();
        $totalActionTaken = $this->applyScopeFilters(ActionTaken::query())->count();
        $totalFinancialSanction = $this->applyScopeFilters(ActionTaken::query())->sum('financial_sanctions');
        $totalConfiscatedGoods = $this->applyScopeFilters(ActionTaken::query())->sum('confiscated_goods');

        return compact(
            'examination_of_excisable_goods',
            'transferred_to_the_oy',
            'sale_to_minors',
            'totalComplaints',
            'totalComplaintsVerificationImpossible',
            'totalActionTaken',
            'totalFinancialSanction',
            'totalConfiscatedGoods'
        );
    }

    /**
     * Apply scope filters.
     *
     * @param mixed $scope
     * @param null $from_dt
     * @param null $to_dt
     * @return Builder
     */
    private function applyScopeFilters($scope, $from_dt = null, $to_dt = null)
    {
        if (!isset($from_dt)) $from_dt = $this->report->from_dt;
        if (!isset($to_dt)) $to_dt = $this->report->to_dt;

        if (isset($from_dt)) $scope = $scope->whereDate('created_at', '>=', $from_dt);
        if (isset($to_dt)) $scope = $scope->whereDate('created_at', '<=', $to_dt);

        return $scope;
    }
}
