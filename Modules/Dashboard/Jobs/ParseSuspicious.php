<?php

namespace Modules\Dashboard\Jobs;

use App\Parser\ChunkReadFilter;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Modules\Dashboard\Events\Address\AddressesParsed;
use Modules\Dashboard\Models\Address;
use Modules\Dashboard\Models\AddressImport;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Exception as SpreadsheetException;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Exception;

class ParseSuspicious implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Cell code constants.
     */
    const CELL_COMPANY = 2;
    const CELL_LICENSE = 3;
    const CELL_ADDRESS = 4;
    const CELL_COMMENT = 5;

    /**
     * @var AddressImport
     */
    private $addressImport;

    /**
     * @var array
     */
    private $actualAddresseIdCodes = [];

    /**
     * @var string
     */
    private $inputFileName;

    /**
     * @var string
     */
    private $inputFileType;

    /**
     * @var int
     */
    private $chunkSize;

    /**
     * @var string|array
     */
    private $sheetName;

    /**
     * @var int
     */
    private $startRow;

    /**
     * @var int
     */
    private $endRow;

    /**
     * @var int
     */
    private $legacySuspiciousChunkSize;

    /**
     * Create a new job instance.
     *
     * @param AddressImport $addressImport
     */
    public function __construct(AddressImport $addressImport)
    {
        $this->addressImport = $addressImport;

        $this->inputFileName = $addressImport->storage_filename;
        $this->inputFileType = 'Xlsx';
        $this->sheetName = 'Лист2';
        $this->chunkSize = 2048;
        $this->startRow = 2;
        $this->endRow = 65536;
        $this->legacySuspiciousChunkSize = 50;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function handle()
    {
        Log::debug("Start parsing suspicious addresses excel file \"$this->inputFileName\".");

        try {
            $filter = $this->getFilter();

            $reader = IOFactory::createReader($this->inputFileType);
            $reader->setReadDataOnly(true);
            $reader->setLoadSheetsOnly($this->sheetName);
            $reader->setReadFilter($filter);

            $worksheetData = $reader->listWorksheetInfo($this->inputFileName);

            $this->endRow = (int)$worksheetData[0]['totalRows'];

            // TODO: release columns order validation

            // Loop to read our worksheet in "chunk size" blocks
            for ($startRow = $this->startRow; $startRow <= $this->endRow; $startRow += $this->chunkSize) {
                Log::debug("Loading chunk rows from file (index: $startRow).");

                $filter->setRows($startRow, $this->chunkSize);
                $spreadsheet = $reader->load($this->inputFileName);

                $this->processSpreadsheetRows($spreadsheet);
            }

            // Update addresses import parsed timestamp
            $this->addressImport->update(['parsed_at' => Carbon::now()]);

            Log::debug('Finish parse suspicious addresses table.');

            $this->removeLegacyAddressesFromDB(
                $this->actualAddresseIdCodes,
                // TODO: need fix after will be released regions support
                0
            );

            event(new AddressesParsed($this->addressImport));

        } catch (SpreadsheetException $e) {
            throw $e;
        }
    }

    /**
     * Process the spreadsheet rows.
     *
     * @param \PhpOffice\PhpSpreadsheet\Spreadsheet $spreadsheet
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws Exception
     */
    protected function processSpreadsheetRows($spreadsheet)
    {
        Log::debug("Starting process the spreadsheet...");

        $worksheet = $spreadsheet->getActiveSheet();
        $highestRow = $worksheet->getHighestRow();

        for ($row = 1; $row <= $highestRow; ++$row) {
            Log::debug("Extract row #$row");

            $address = $this->extractAddressByRow($worksheet, $row);

            if ($address) {
                $this->storeOrIgnoreAddress($address);
            } else {
                Log::warning("Incorrect address data in row #$row.");
            }
        }
    }

    /**
     * Extract address entity from worksheet by row.
     *
     * @param Worksheet $worksheet
     * @param int $row
     * @return array|null
     * @throws Exception
     */
    private function extractAddressByRow($worksheet, $row)
    {
        // If address not set then return null object
        // NOTICE: Must do calculate excel formula
        if (($license = $worksheet->getCellByColumnAndRow(self::CELL_LICENSE, $row)->getCalculatedValue()) === NULL) {
            Log::debug("Address row #$row is incorrect. Skipping...");

            return NULL;
        }

        // Store address license ID code to list
        $this->actualAddresseIdCodes[] = $license;

        return [
            'region_id' => 0,
            'company' => $worksheet->getCellByColumnAndRow(self::CELL_COMPANY, $row)->getValue(),
            'status' => Address::STATUS_SUSPICIOUS,
            'license_type' => Address::TYPE_MIXED,
            'license' => $license,
            'address' => $worksheet->getCellByColumnAndRow(self::CELL_ADDRESS, $row)->getValue(),
            'comment' => $worksheet->getCellByColumnAndRow(self::CELL_COMMENT, $row)->getValue(),
        ];
    }

    /**
     * Remove legacy addresses from database.
     *
     * @param array $actualLicenses
     * @param int $regionId
     */
    private function removeLegacyAddressesFromDB(array &$actualLicenses, $regionId)
    {
        Log::debug('Remove legacy addresses from database...');

        /** @var Collection $legacyLicenses */
        $legacyLicenses = Address::region($regionId)
            ->suspicious()
            ->pluck('license')
            ->diff($actualLicenses);

        Log::debug("Total legacy licenses: {$legacyLicenses->count()}");

        if ($legacyLicenses->count() > 0) {
            DB::beginTransaction();

            foreach ($legacyLicenses->chunk($this->legacySuspiciousChunkSize) as $chunkLegacyLicenses) {
                Address::region($regionId)
                    ->withoutLicense()
                    ->whereIn('license', $chunkLegacyLicenses)
                    ->delete();
            }

            DB::commit();

            Log::debug('Finished removing legacy addresses from database.');
        } else {
            Log::debug('Nothing to remove. Skipping...');
        }
    }

    /**
     * @return ChunkReadFilter
     */
    private function getFilter()
    {
        return new ChunkReadFilter();
    }

    /**
     * Store or ignore the address.
     *
     * @param array $address
     * @return Address|null
     */
    protected function storeOrIgnoreAddress(array $address)
    {
        if (Address::where('license', $address['license'])->exists()) {
            Log::debug("Address with license #{$address['license']} already exists. Skipping...");

            return null;
        } else {
            Log::debug("Create address entry for license: #{$address['license']}");

            return Address::create($address);
        }
    }
}
