<?php

namespace Modules\Dashboard\Providers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Database\Eloquent\Relations\Relation;

class DashboardServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
        $this->registerViewComposerProvider();
        $this->registerEventProvider();
        $this->registerMorphMap();
        $this->enableDBQueryLog();
    }

    protected function enableDBQueryLog()
    {
        if (! app()->environment('production')) {
            DB::enableQueryLog();
        }
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    protected function registerMorphMap()
    {
        Relation::morphMap(config('dashboard.morph_map', []));
    }

    /**
     * Register view composer provider.
     */
    protected function registerViewComposerProvider()
    {
        $this->app->register(ComposerServiceProvider::class);
    }

    /**
     * Register event provider.
     */
    protected function registerEventProvider()
    {
        $this->app->register(EventServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('dashboard.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'dashboard'
        );
        $this->mergeConfigFrom(
            __DIR__.'/../Config/auth.php', 'dashboard.auth'
        );
        $this->mergeConfigFrom(
            __DIR__.'/../Config/address.php', 'dashboard.address'
        );
        $this->mergeConfigFrom(
            __DIR__.'/../Config/complaint.php', 'dashboard.complaint'
        );
        $this->mergeConfigFrom(
            __DIR__.'/../Config/event.php', 'dashboard.event'
        );

        $this->mergeAuthConfig();
    }

    /**
     * Merge global application auth inner/cross configs with dashboard auth configs.
     *
     * This method run subtle merging operations,
     * because wee need only to merge little portion of "dashboard:auth" configs with global key "auth".
     */
    private function mergeAuthConfig()
    {
        $this->mergeConfig('auth.guards', config('dashboard.auth.guards', []));
        $this->mergeConfig('auth.providers', config('dashboard.auth.providers', []));
        $this->mergeConfig('auth.passwords', config('dashboard.auth.passwords', []));
    }

    /**
     * Merge config helper.
     * Support to merge inner/cross configs.
     *
     * @param string $key
     * @param array $data
     */
    private function mergeConfig($key, $data)
    {
        $config = $this->app['config']->get($key, []);

        $this->app['config']->set($key, array_merge($data, $config));
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/dashboard');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/dashboard';
        }, \Config::get('view.paths')), [$sourcePath]), 'dashboard');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/dashboard');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'dashboard');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'dashboard');
        }
    }

    /**
     * Register an additional directory of factories.
     * @source https://github.com/sebastiaanluca/laravel-resource-flow/blob/develop/src/Modules/ModuleServiceProvider.php#L66
     */
    public function registerFactories()
    {
        if (! app()->environment('production')) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
