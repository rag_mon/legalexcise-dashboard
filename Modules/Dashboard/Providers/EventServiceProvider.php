<?php

namespace Modules\Dashboard\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

//    /**
//     * The event listener mappings for the application.
//     *
//     * @var array
//     */
//    protected $listen = [
//        'Modules\Dashboard\Events\Event' => [
//            'Modules\Dashboard\Listeners\HistoryEventSubscriber',
//        ],
//    ];

    /**
     * The subscriber classes to register.
     *
     * @var array
     */
    protected $subscribe = [
        'Modules\Dashboard\Listeners\HistoryEventSubscriber',
        'Modules\Dashboard\Listeners\AddressEventSubscriber',
        'Modules\Dashboard\Listeners\ComplaintEventSubscriber',
        'Modules\Dashboard\Listeners\ReportEventSubscriber',
    ];

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
