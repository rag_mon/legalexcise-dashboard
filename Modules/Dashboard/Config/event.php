<?php

return [

    //
    //  Events types map
    //
    'address' => [
        'created' => Modules\Dashboard\Events\Address\AddressStored::class,
        'updated' => Modules\Dashboard\Events\Address\AddressUpdated::class,
        'deleted' => Modules\Dashboard\Events\Address\AddressDeleted::class,

        'comment' => [
            'posted' => Modules\Dashboard\Events\Address\CommentPosted::class,
        ]
    ],

    'complaint' => [
        'updated' => Modules\Dashboard\Events\Complaint\ComplaintUpdated::class,
        'deleted' => Modules\Dashboard\Events\Complaint\ComplaintDeleted::class,
    ],

    'report' => [
        'rendering' => Modules\Dashboard\Events\Report\ReportRendered::class,
        'created' => Modules\Dashboard\Events\Report\ReportStored::class,
        'deleted' => Modules\Dashboard\Events\Report\ReportDeleted::class,
        'downloaded' => Modules\Dashboard\Events\Report\ReportDownloaded::class,
    ]

];
