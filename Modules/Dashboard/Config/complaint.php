<?php

return [

    'after_transform_status' => \Modules\Dashboard\Models\Address::STATUS_SUSPICIOUS,

    'widget' => [
        'total_value_color' => 'blue',
        'total_not_processed_value_color' => 'red',
        'total_processed_value_color' => 'green',
    ],

];