<?php

return [

    /*
     * Analytics data provider configs.
     */
    'provider' => [
        /*
         * Analytics data providers aliases.
         */
        'aliases' => [
            'adding_addresses' => 'Modules\\Dashboard\\Charts\\Providers\\AddingAddressProvider',
            'suspicious_to_active_addresses' => 'Modules\\Dashboard\\Charts\\Providers\\SuspiciousToActiveAddressProvider',
            'complaint_reactions' => 'Modules\\Dashboard\\Charts\\Providers\\ComplaintReactionProvider',
            'percent_reactions' => 'Modules\\Dashboard\\Charts\\Providers\\PercentReactionProvider',
        ],
    ],

];