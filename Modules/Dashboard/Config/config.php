<?php

return [
    'name' => 'Dashboard',
    'base_url' => '/',

    'language' => [
        'list' => [
            'ru',
            'uk',
        ]
    ],

    'morph_map' => [
        'addresses' => 'Modules\Dashboard\Models\Address',
        'complaints' => 'Modules\Dashboard\Models\Complaint',
    ],
];
