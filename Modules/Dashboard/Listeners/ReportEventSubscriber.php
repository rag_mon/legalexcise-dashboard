<?php

namespace Modules\Dashboard\Listeners;

use Modules\Dashboard\Events\Report\ReportDeleted;
use Modules\Dashboard\Events\Report\ReportDownloaded;
use Modules\Dashboard\Events\Report\ReportRendered;
use Modules\Dashboard\Events\Report\ReportStored;
use Modules\Dashboard\Jobs\ReportRender;

class ReportEventSubscriber
{
    use HistoryEventTrait;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ReportStored  $event
     * @return void
     */
    public function onReportStored(ReportStored $event)
    {
        dispatch(new ReportRender($event->report));

        $this->dispatchHistoryEvent(
            'dashboard::event.report.created',
            'dashboard::history.report.created',
            $event->report->toArray(),
            $this->getUser()
        );
    }

    /**
     * Handle the event.
     *
     * @param  ReportDownloaded  $event
     * @return void
     */
    public function onReportDownloaded(ReportDownloaded $event)
    {
        $this->dispatchHistoryEvent(
            'dashboard::event.report.downloaded',
            'dashboard::history.report.downloaded',
            $event->report->toArray(),
            $this->getUser()
        );
    }

    /**
     * Handle the event.
     *
     * @param  ReportDeleted  $event
     * @return void
     */
    public function onReportDeleted(ReportDeleted $event)
    {
        $this->dispatchHistoryEvent(
            'dashboard::event.report.deleted',
            'dashboard::history.report.deleted',
            $event->report->toArray(),
            $this->getUser()
        );
    }

    /**
     * Handle the event.
     *
     * @param  ReportRendered  $event
     * @return void
     */
    public function onReportRendered(ReportRendered $event)
    {
        $this->dispatchHistoryEvent(
            'dashboard::event.report.rendered',
            'dashboard::history.report.rendered',
            $event->report->toArray(),
            $this->getUser()
        );
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'Modules\Dashboard\Events\Report\ReportStored',
            'Modules\Dashboard\Listeners\ReportEventSubscriber@onReportStored'
        );

        $events->listen(
            'Modules\Dashboard\Events\Report\ReportDownloaded',
            'Modules\Dashboard\Listeners\ReportEventSubscriber@onReportDownloaded'
        );

        $events->listen(
            'Modules\Dashboard\Events\Report\ReportDeleted',
            'Modules\Dashboard\Listeners\ReportEventSubscriber@onReportDeleted'
        );

        $events->listen(
            'Modules\Dashboard\Events\Report\ReportRendered',
            'Modules\Dashboard\Listeners\ReportEventSubscriber@onReportRendered'
        );
    }
}
