<?php

namespace Modules\Dashboard\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Dashboard\Events\Complaint\ComplaintDeleted;
use Modules\Dashboard\Events\Complaint\ComplaintTransformed;
use Modules\Dashboard\Events\Complaint\ComplaintUpdated;

class ComplaintEventSubscriber
{
    use HistoryEventTrait;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ComplaintUpdated  $event
     * @return void
     */
    public function onComplaintUpdated(ComplaintUpdated $event)
    {
        $this->dispatchHistoryEvent(
            'dashboard::event.complaint.updated',
            'dashboard::history.complaint.updated',
            $event->complaint->toArray(),
            $this->getUser()
        );
    }

    /**
     * Handle the event.
     *
     * @param  ComplaintDeleted  $event
     * @return void
     */
    public function onComplaintDeleted(ComplaintDeleted $event)
    {
        $this->dispatchHistoryEvent(
            'dashboard::event.complaint.deleted',
            'dashboard::history.complaint.deleted',
            $event->complaint->toArray(),
            $this->getUser()
        );
    }

    /**
     * Handle the event.
     *
     * @param  ComplaintTransformed  $event
     * @return void
     */
    public function onComplaintTransformed(ComplaintTransformed $event)
    {
        $this->dispatchHistoryEvent(
            'dashboard::event.complaint.transformed',
            'dashboard::history.complaint.transformed',
            $event->complaint->toArray(),
            $this->getUser()
        );
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'Modules\Dashboard\Events\Complaint\ComplaintUpdated',
            'Modules\Dashboard\Listeners\ComplaintEventSubscriber@onComplaintUpdated'
        );

        $events->listen(
            'Modules\Dashboard\Events\Complaint\ComplaintDeleted',
            'Modules\Dashboard\Listeners\ComplaintEventSubscriber@onComplaintDeleted'
        );

        $events->listen(
            'Modules\Dashboard\Events\Complaint\ComplaintTransformed',
            'Modules\Dashboard\Listeners\ComplaintEventSubscriber@onComplaintTransformed'
        );
    }
}
