<?php

namespace Modules\Dashboard\Listeners;

use Modules\Dashboard\Events\Address\AddressActionTaken;
use Modules\Dashboard\Events\Address\AddressDeleted;
use Modules\Dashboard\Events\Address\AddressesGeocoded;
use Modules\Dashboard\Events\Address\AddressesImported;
use Modules\Dashboard\Events\Address\AddressesParsed;
use Modules\Dashboard\Events\Address\AddressesUploaded;
use Modules\Dashboard\Events\Address\AddressStored;
use Modules\Dashboard\Events\Address\AddressUpdated;
use Modules\Dashboard\Events\Address\AddressUpdateReleased;
use Modules\Dashboard\Events\Address\CommentPosted;
use Modules\Dashboard\Jobs\GeocodingAddresses;
use Modules\Dashboard\Jobs\ParseAddresses;
use Modules\Dashboard\Jobs\ParseSuspicious;
use Modules\Dashboard\Models\AddressImport;

class AddressEventSubscriber
{
    use HistoryEventTrait;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AddressStored  $event
     * @return void
     */
    public function onAddressStored(AddressStored $event)
    {
        $this->dispatchHistoryEvent(
            'dashboard::event.address.created',
            'dashboard::history.address.created',
            $this->clearAddressData($event->address->toArray()),
            $this->getUser()
        );
    }

    /**
     * Handle the event.
     *
     * @param  AddressDeleted  $event
     * @return void
     */
    public function onAddressDeleted(AddressDeleted $event)
    {
        $this->dispatchHistoryEvent(
            'dashboard::event.address.deleted',
            'dashboard::history.address.deleted',
            $this->clearAddressData($event->address->toArray()),
            $this->getUser()
        );
    }

    /**
     * Handle the event.
     *
     * @param  AddressUpdated  $event
     * @return void
     */
    public function onAddressUpdated(AddressUpdated $event)
    {
        $this->dispatchHistoryEvent(
            'dashboard::event.address.updated',
            'dashboard::history.address.updated',
            $this->clearAddressData($event->address->toArray()),
            $this->getUser()
        );
    }

    /**
     * Handle the event.
     *
     * @param  AddressUpdateReleased  $event
     * @return void
     */
    public function onAddressUpdateReleased(AddressUpdateReleased $event)
    {
        $this->dispatchHistoryEvent(
            'dashboard::event.address.update_released',
            'dashboard::history.address.update_released',
            $this->clearAddressData($event->addressUpdated->toArray()),
            $this->getUser()
        );
    }

    /**
     * Handle the event.
     *
     * @param  CommentPosted  $event
     * @return void
     */
    public function onCommentPosted(CommentPosted $event)
    {
        $this->dispatchHistoryEvent(
            'dashboard::event.address.comment.posted',
            'dashboard::history.address.comment.posted',
            // we need piece of data from address entry too
            array_merge($this->clearAddressData($event->comment->address->toArray()), $event->comment->toArray()),
            $this->getUser()
        );
    }

    /**
     * Handle the event.
     *
     * @param  AddressActionTaken  $event
     * @return void
     */
    public function onActionTaken(AddressActionTaken $event)
    {
        $this->dispatchHistoryEvent(
            'dashboard::event.address.action.taken',
            'dashboard::history.address.action.taken',
            $this->clearAddressData($event->address->toArray()),
            $this->getUser()
        );
    }

    /**
     * Handle the event.
     *
     * @param  AddressesUploaded  $event
     * @return void
     */
    public function onAddressesUploaded(AddressesUploaded $event)
    {
        $this->dispatchHistoryEvent(
            'dashboard::event.address.addresses_uploaded',
            'dashboard::history.address.addresses_uploaded',
            $this->clearAddressData($event->addressImport->toArray()),
            $this->getUser()
        );

        dispatch($this->makeParseAddressesJob($event->addressImport));
    }

    /**
     * Make parse addresses job.
     *
     * @param AddressImport $addressImport
     * @return ParseAddresses|ParseSuspicious
     */
    private function makeParseAddressesJob($addressImport)
    {
        return $addressImport->type == AddressImport::TYPE_LICENSES
            ? new ParseAddresses($addressImport)                        // common addresses (licenses)
            : new ParseSuspicious($addressImport);                      // suspicious addresses
    }

    /**
     * Handle the event.
     *
     * @param  AddressesParsed  $event
     * @return void
     */
    public function onAddressesParsed(AddressesParsed $event)
    {
        $this->dispatchHistoryEvent(
            'dashboard::event.address.addresses_parsed',
            'dashboard::history.address.addresses_parsed',
            $this->clearAddressData($event->addressImport->toArray()),
            $this->getUser()
        );

        dispatch(new GeocodingAddresses($event->addressImport));
    }

    /**
     * Handle the event.
     *
     * @param  AddressesGeocoded  $event
     * @return void
     */
    public function onAddressesGeocoded(AddressesGeocoded $event)
    {
        $this->dispatchHistoryEvent(
            'dashboard::event.address.addresses_geocoded',
            'dashboard::history.address.addresses_geocoded',
            $this->clearAddressData($event->addressImport->toArray()),
            $this->getUser()
        );

        event(new AddressesImported($event->addressImport));
    }

    /**
     * Handle the event.
     *
     * @param  AddressesImported  $event
     * @return void
     */
    public function onAddressesImported(AddressesImported $event)
    {
        $this->dispatchHistoryEvent(
            'dashboard::event.address.addresses_imported',
            'dashboard::history.address.addresses_imported',
            $this->clearAddressData($event->addressImport->toArray()),
            $this->getUser()
        );
    }

    /**
     * Clear address data.
     *
     * NOTICE: this method remove address key-value pair with object type.
     *
     * @param array $data
     * @return array
     */
    private function clearAddressData(array $data)
    {
        return array_except($data, ['public_notices']);
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'Modules\Dashboard\Events\Address\AddressStored',
            'Modules\Dashboard\Listeners\AddressEventSubscriber@onAddressStored'
        );

        $events->listen(
            'Modules\Dashboard\Events\Address\AddressDeleted',
            'Modules\Dashboard\Listeners\AddressEventSubscriber@onAddressDeleted'
        );

        $events->listen(
            'Modules\Dashboard\Events\Address\AddressUpdated',
            'Modules\Dashboard\Listeners\AddressEventSubscriber@onAddressUpdated'
        );

        $events->listen(
            'Modules\Dashboard\Events\Address\AddressUpdateReleased',
            'Modules\Dashboard\Listeners\AddressEventSubscriber@onAddressUpdateReleased'
        );

        $events->listen(
            'Modules\Dashboard\Events\Address\CommentPosted',
            'Modules\Dashboard\Listeners\AddressEventSubscriber@onCommentPosted'
        );

        $events->listen(
            'Modules\Dashboard\Events\Address\AddressActionTaken',
            'Modules\Dashboard\Listeners\AddressEventSubscriber@onActionTaken'
        );

        $events->listen(
            'Modules\Dashboard\Events\Address\AddressesUploaded',
            'Modules\Dashboard\Listeners\AddressEventSubscriber@onAddressesUploaded'
        );

        $events->listen(
            'Modules\Dashboard\Events\Address\AddressesParsed',
            'Modules\Dashboard\Listeners\AddressEventSubscriber@onAddressesParsed'
        );

        $events->listen(
            'Modules\Dashboard\Events\Address\AddressesGeocoded',
            'Modules\Dashboard\Listeners\AddressEventSubscriber@onAddressesGeocoded'
        );

        $events->listen(
            'Modules\Dashboard\Events\Address\AddressesImported',
            'Modules\Dashboard\Listeners\AddressEventSubscriber@onAddressesImported'
        );
    }
}
