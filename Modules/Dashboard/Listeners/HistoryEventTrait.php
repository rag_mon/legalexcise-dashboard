<?php

namespace Modules\Dashboard\Listeners;

use Illuminate\Support\Facades\Auth;
use Modules\Dashboard\Events\History\EventDispatched;
use Modules\Dashboard\Models\Event;
use Modules\Dashboard\Models\User;

/**
 * Trait HistoryEventTransformerTrait
 *
 * @package Modules\Dashboard\Listeners
 */
trait HistoryEventTrait
{
    /**
     * Create and dispatch history event.
     *
     * @param string $type
     * @param string $langKey
     * @param array $data
     * @param null $user
     * @param int $dataOptions
     */
    protected function dispatchHistoryEvent($type, $langKey, array $data = [], $user = null, $dataOptions = 0)
    {
        event(new EventDispatched($this->createHistoryEventEntry($type, $langKey, $data, $user, $dataOptions)));
    }

    /**
     * Create history event.
     *
     * @param string $type
     * @param string $langKey
     * @param array $data
     * @param null|User $user
     * @param int $dataOptions
     * @return Event
     */
    private function createHistoryEventEntry($type, $langKey, array $data = [], $user = null, $dataOptions = 0)
    {
        return Event::create([
            'type' => $type,
            'lang_key' => $langKey,
            'data' => isset($data) ? json_encode($data, $dataOptions) : null,
            'user_id' => $user instanceof User ? $user->getKey() : $user,
        ]);
    }

    /**
     * Get current authorized user entry.
     *
     * @return User
     */
    private function getUser()
    {
        return Auth::guard('dashboard')->user();
    }
}