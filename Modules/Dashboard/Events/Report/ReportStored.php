<?php

namespace Modules\Dashboard\Events\Report;

use Illuminate\Queue\SerializesModels;
use Modules\Dashboard\Models\Report;

class ReportStored
{
    use SerializesModels;

    /**
     * @var Report
     */
    public $report;

    /**
     * Create a new event instance.
     *
     * @param Report $report
     */
    public function __construct(Report $report)
    {
        $this->report = $report;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
