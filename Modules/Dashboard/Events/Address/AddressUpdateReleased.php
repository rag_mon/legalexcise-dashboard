<?php

namespace Modules\Dashboard\Events\Address;

use Illuminate\Queue\SerializesModels;
use Modules\Dashboard\Models\AddressUpdated;

class AddressUpdateReleased
{
    use SerializesModels;

    /**
     * @var AddressUpdated
     */
    public $addressUpdated;

    /**
     * Create a new event instance.
     *
     * @param AddressUpdated $addressUpdated
     */
    public function __construct(AddressUpdated $addressUpdated)
    {
        $this->addressUpdated = $addressUpdated;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
