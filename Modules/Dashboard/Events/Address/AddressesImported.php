<?php

namespace Modules\Dashboard\Events\Address;

use Illuminate\Queue\SerializesModels;
use Modules\Dashboard\Models\AddressImport;

class AddressesImported
{
    use SerializesModels;

    /**
     * @var AddressImport
     */
    public $addressImport;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(AddressImport $addressImport)
    {
        $this->addressImport = $addressImport;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
