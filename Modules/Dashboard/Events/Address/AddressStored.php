<?php

namespace Modules\Dashboard\Events\Address;

use Illuminate\Queue\SerializesModels;
use Modules\Dashboard\Models\Address;

class AddressStored
{
    use SerializesModels;

    /**
     * @var Address
     */
    public $address;

    /**
     * Create a new event instance.
     *
     * @param Address $address
     */
    public function __construct(Address $address)
    {
        $this->address = $address;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
