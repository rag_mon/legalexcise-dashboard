<?php

namespace Modules\Dashboard\Events\Address;

use Illuminate\Queue\SerializesModels;
use Modules\Dashboard\Models\AddressComment;

class CommentPosted
{
    use SerializesModels;

    /**
     * @var AddressComment
     */
    public $comment;

    /**
     * Create a new event instance.
     *
     * @param AddressComment $comment
     */
    public function __construct(AddressComment $comment)
    {
        $this->comment = $comment;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
