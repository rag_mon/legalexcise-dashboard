<?php

namespace Modules\Dashboard\Events\History;

use Illuminate\Queue\SerializesModels;
use Modules\Dashboard\Models\Event;

class EventDispatched
{
    use SerializesModels;

    /**
     * @var Event
     */
    public $event;

    /**
     * Create a new event instance.
     *
     * @param Event $event
     */
    public function __construct(Event $event)
    {
        $this->event = $event;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
