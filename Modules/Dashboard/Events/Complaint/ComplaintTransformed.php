<?php

namespace Modules\Dashboard\Events\Complaint;

use Illuminate\Queue\SerializesModels;
use Modules\Dashboard\Models\Address;
use Modules\Dashboard\Models\Complaint;

class ComplaintTransformed
{
    use SerializesModels;

    /**
     * @var Complaint
     */
    public $complaint;

    /**
     * @var Address
     */
    public $address;

    /**
     * Create a new event instance.
     *
     * @param Complaint $complaint
     * @param Address $address
     */
    public function __construct(Complaint $complaint, Address $address)
    {
        $this->complaint = $complaint;
        $this->address = $address;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
