<?php

namespace Modules\Dashboard\Events\Complaint;

use Illuminate\Queue\SerializesModels;
use Modules\Dashboard\Models\Complaint;

class ComplaintDeleted
{
    use SerializesModels;

    /**
     * @var Complaint
     */
    public $complaint;

    /**
     * Create a new event instance.
     *
     * @param Complaint $complaint
     */
    public function __construct(Complaint $complaint)
    {
        $this->complaint = $complaint;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
