<?php

namespace Modules\Dashboard\Events\Bookkeeping;

use Illuminate\Queue\SerializesModels;
use Modules\Dashboard\Models\Bookkeeping;

class BookkeepingUpdated
{
    use SerializesModels;

    /**
     * @var Bookkeeping
     */
    public $bookkeeping;

    /**
     * Create a new event instance.
     *
     * @param Bookkeeping $bookkeeping
     */
    public function __construct(Bookkeeping $bookkeeping)
    {
        $this->bookkeeping = $bookkeeping;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
