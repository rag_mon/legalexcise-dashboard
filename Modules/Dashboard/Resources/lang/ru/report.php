<?php

return [

    'status' => [
        'not_ready' => 'Не готов',
        'ready' => 'Готов',
        'rendering' => 'Обрабатывается',
    ],

    'created_success' => 'Отчёт ":name" в процессе создания',

    'create_report_modal_title' => 'Создать отчет',

    // Index
    'list_title' => 'Отчёты',
    'create_report_btn' => 'Создать отчёт',
    'user' => 'Пользователь',
    'status_title' => 'Статус',
    'date' => 'Дата',
    'action' => 'Операция',

];