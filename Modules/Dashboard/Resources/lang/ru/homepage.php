<?php

return [

    'title_address_map' => 'Карта адресов',

    'license_information' => 'Информация о лицензии',

    // Basic statistic columns
    'received_complaints' => 'Поступило<br> жалоб,<br> из них',
    'in_process' => 'На рассмотрении в<br> управлении контроля<br> за оборотом и<br> налогообложением<br> подакцизных товаров',
    'post_to_oy' => 'Передано в ОУ',
    'in_np' => 'Находятся в ГУ ЧП <br> (продажа несовершеннолетним)',
    'verification_imposable' => 'Нецелесообразности <br> проведения <br> проверок',
    'total_verification' => 'Проведено <br> проверок',
    'total_financial_sanctions' => 'Применено штрафных <br> санкций, грн',
    'total_confiscated_goods' => 'Изъято продукции, <br> бут. / Пач.',

];