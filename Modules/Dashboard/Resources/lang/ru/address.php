<?php

return [

    'widget' => [
        'title_total' => 'Всего адресов',
        'title_total_active' => 'Зеленные адреса',
        'title_total_suspicious' => 'Красные адреса',
        'title_additions' => 'Добавления адресов',
    ],

    'title_list' => 'Список адресов',
    'create_address' => 'Создать адрес',
    'label_address' => 'Адрес',
    'label_id_code' => 'ИНН',
    'label_lat' => 'Широта',
    'label_lng' => 'Долгота',
    'label_license' => 'Код лицензии',
    'label_company' => 'Название',
    'label_company_type' => 'Тип объекта',
    'label_license_type' => 'Тип лицензии',
    'label_status' => 'Статус',
    'label_start_license' => 'Начало лицензии',
    'label_end_license' => 'Конец лицензии',
    'label_comment' => 'Комментарий',
    'label_comment_hint' => 'Опишите детально предпринятые действия.',
    'label_created_at' => 'Дата создания',
    'label_updated_at' => 'Дата редактирования',
    'label_actions' => 'Действие',
    'lat_lng' => 'Широта/Долгота',
    'company_type' => 'Тип объекта',
    'company' => 'Название объекта',

    'title_new_comment' => 'Новый комментарий',

    'btn_new_comment' => 'Новый комментарий',
    'btn_show' => 'Показать',
    'btn_show_on_map' => 'Показать на карте',
    'btn_control' => 'Управление',
    'btn_delete' => 'Удалить',
    'comment' => 'Комментарий',
    'action_describe' => 'Опишите детально предпринятые действия.',
    'delete_confirmation' => 'Подтверждение удаления адреса',
    'question_delete_address' => 'Вы действительно хотите удалить адрес',

    'license_type' => [
        'alcohol' => 'Алкогольная',
        'tobacco' => 'Табачная',
        'mixed' => 'Смешанная/Неизвестная',
        'beer' => 'Пиво',
    ],

    'status' => [
        'active' => 'Активный',
        'canceled' => 'Не активный',
        're-arranged' => 'Переиздана',
        'suspicious' => 'Подозрительный',
    ],

    'created_success' => 'Адрес ":address" успешно создан.',
    'updated_success' => 'Адрес ":address" успешно обновлён.',

    'control_address' => 'Управление адресом: :address',
    'type_id_code' => 'Введите ИНН',
    'type_address' => 'Введите адрес',
    'type_license' => 'Введите код лицензии',
    'type_lat' => 'Введите широту',
    'type_lng' => 'Введите долготу',
    'type_company_type' => 'Введите тип объекта',
    'type_company' => 'Введите название объекта',
    'datetime_format' => 'гггг-мм-дд',
    'select_on_map' => 'Выбрать на карте',
    'save' => 'Сохранить',
    'save_and_close' => 'Сохранить и закрыть',
    'delete' => 'Удалить',
    'cancel' => 'Отмена',

    // Public notices
    'confiscated_goods' => 'Конфискованная продукция',
    'protocol_drawn_up' => 'Составлен протокол от',
    'financial_sanctions' => 'Применена финансовая санкция',

    'new_address' => 'Новый адрес',

    // Import addresses
    'import' => [
        'title' => 'Импорт таблицы адресов',
        'support_formats' => 'Поддерживаемые форматы: xlsx.',
        'type_licenses' => 'База адресов (по умолчанию)',
        'type_suspicious' => 'Подозреваемые адреса',
        'upload' => 'Загрузить',
        'types' => [
            'licenses' => 'Лицензии',
            'suspicious' => 'Подозрительные',
        ]
    ],

    // Release database updates
    'updates' => [
        // Confirmation popup
        'title' => 'Подтверждение обновления',
        'question' => 'Вы действительно хотите издать обновление?',
        'update' => 'Издать обновление',

        // Page
        'updates_list' => 'Список обновлений',
        'update_id' => 'ID обновления',
        'timestamp' => 'Дата издания обновления',
    ],

    // Map popup
    'map' => [
        'change_pick_point' => 'Выберите точку на карте',
        'tooltip' => 'Нажмите на карту чтобы получить координаты.',
    ],

];