<?php

return [

    'events' => 'События',
    'user' => 'Пользователь',
    'user_group' => 'Группа',
    'action' => 'Действие',
    'date' => 'Дата',

    'address' => [
        'created' => 'Создан адрес ":address".',
        'deleted' => 'Удалён адрес ":address".',
        'updated' => 'Обновлён адрес ":address".',
        'update_released' => 'Издано обновление базы данных адресов.',
        'addresses_uploaded' => 'Таблица адресов ":uploaded_filename" успешно загружена.',
        'addresses_parsed' => 'Таблица адресов ":uploaded_filename" успешно обработана и импортирована.',
        'addresses_geocoded' => 'Задача определения координат адресов успешно завершена.',
        'addresses_imported' => 'Адреса успешно импортированы и обработаны.',
        'comment' => [
            'posted' => 'Добавлен комментарий к адресу ":address".',
        ],
        'action' => [
            'taken' => 'Предпринято действие к адресу ":address".',
        ],
    ],

    'complaint' => [
        'updated' => 'Обновлены данные по жалобе.',
        'deleted' => 'Удалена жалоба.',
        'transformed' => 'Перенос жалобы на карту.',
    ],

    'report' => [
        'created' => 'Подан запрос на создание отчёта ":name".',
        'downloaded' => 'Скачан отчёт ":name".',
        'deleted' => 'Удалён отчёт ":name".',
        'rendered' => 'Отчёт ":name" успешно создан и готов к скачиванию.',
    ],

    'user_groups' => [
        'sfs' => 'фискальная служба',
        'police' => 'нац. полиция',
    ],

];