<?php

return [

    'statistic_complaints' => 'Статистика жалоб',
    'total_complaints' => 'Кол-во полученных жалоб',

    'statistic_action_taken' => 'Статистика реакций на жалобы',
    'total_action_taken' => 'Кол-во реакций на жалобы',

    'statistic_suspicious' => 'Добавления подозрительных адресов',
    'total_suspicious' => 'Кол-во подозрительных адресов',

    'statistic_financial_sanctions' => 'Финансовые санкции',
    'total_financial_sanctions' => 'Сумма финансовых санкций',

    'statistic_complaints_and_action_taken' => 'Статистика жалоб/обработок',

    'complaints' => 'Жалобы',
    'processed' => 'Обработанные',

];