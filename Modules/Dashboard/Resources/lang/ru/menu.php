<?php

return [

    'home' => 'Главная',
    'addresses' => 'Адреса',
    'addresses_import' => 'Импорт',
    'analytics' => 'Аналитика',
    'history' => 'История',
    'reports' => 'Отчёты',
    'complaint' => 'Жалобы',
    'db_updates' => 'Обновления',

];