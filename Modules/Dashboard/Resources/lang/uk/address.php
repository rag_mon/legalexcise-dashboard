<?php

return [

    'widget' => [
        'title_total' => 'Ввсього адрес',
        'title_total_active' => 'Зелені адреси',
        'title_total_suspicious' => 'Червоні адреси',
        'title_additions' => 'Додавання адрес',
    ],

    'title_list' => 'Список адрес',
    'create_address' => 'Створити адресу',
    'label_address' => 'Адреса',
    'label_id_code' => 'ИНН',
    'label_lat' => 'Широта',
    'label_lng' => 'Довгота',
    'label_license' => 'Код ліцензії',
    'label_company' => 'Назва',
    'label_company_type' => 'Тип объекта',
    'label_license_type' => 'Тип об\'єкту',
    'label_status' => 'Статус',
    'label_start_license' => 'Початок ліцензії',
    'label_end_license' => 'Кінець ліцензії',
    'label_comment' => 'Коментар',
    'label_comment_hint' => 'Опишіть детально вжиті заходи.',
    'label_created_at' => 'Дата створення',
    'label_updated_at' => 'Дата редактирования',
    'label_actions' => 'Дія',
    'lat_lng' => 'Широта довгота',
    'company_type' => 'Тип об\'єкта',
    'company' => 'Назва об\'єкту',

    'title_new_comment' => 'Новий коментар',

    'btn_new_comment' => 'Новий коментар',
    'btn_show' => 'Показати',
    'btn_show_on_map' => 'Показати на мапі',
    'btn_control' => 'Управління',
    'btn_delete' => 'Видалити',
    'comment' => 'Коментар',
    'action_describe' => 'Опишіть детально вжиті заходи.',
    'delete_confirmation' => 'Підтвердження видалення адреси',
    'question_delete_address' => 'Ви дійсно хочете видалити адресу',

    'license_type' => [
        'alcohol' => 'Алкогольна',
        'tobacco' => 'Тютюнова',
        'mixed' => 'Змішана / Невідома',
        'beer' => 'Пиво',
    ],

    'status' => [
        'active' => 'Активний',
        'canceled' => 'Не активний',
        're-arranged' => 'Перевидана',
        'suspicious' => 'Підозрілий',
    ],

    'created_success' => 'Адреса ": address" успішно створена.',
    'updated_success' => 'Адреса ":address" успішно оновлена.',

    'control_address' => 'Управління адресою: :address',
    'type_id_code' => 'Введіть ИНН',
    'type_address' => 'Введіть адресу',
    'type_license' => 'Введіть код ліцензії',
    'type_lat' => 'Введіть широту',
    'type_lng' => 'Введіть довготу',
    'type_company_type' => 'Введіть тип об\'єкта',
    'type_company' => 'Введіть назву об\'єкта',
    'datetime_format' => 'рррр-мм-дд',
    'select_on_map' => 'Вибрати на карті',
    'save' => 'Зберегти',
    'save_and_close' => 'Зберегти і закрити',
    'delete' => 'Видалити',
    'cancel' => 'Відміна',

    // Public notices
    'confiscated_goods' => 'Конфіскована продукція',
    'protocol_drawn_up' => 'Складено протокол від',
    'financial_sanctions' => 'Применена финансовая санкция',

    'new_address' => 'Новый адрес',

    // Import addresses
    'import' => [
        'title' => 'Імпорт таблиці адрес',
        'support_formats' => 'Підтримувані формати: xlsx.',
        'type_licenses' => 'База адресов (по умолчанию)',
        'type_suspicious' => 'Підозрювані адреси',
        'upload' => 'Завантажити',
    ],

    // Release database updates
    'updates' => [
        // Confirmation popup
        'title' => 'Підтвердження оновлення',
        'question' => 'Ви дійсно хочете видати оновлення?',
        'update' => 'Видати оновлення',

        // Page
        'updates_list' => 'Список оновлень',
        'update_id' => 'ID оновлення',
        'timestamp' => 'Дата видання оновлення',
    ],

    // Map popup
    'map' => [
        'change_pick_point' => 'Виберіть точку на карті',
        'tooltip' => 'Натисніть на карту щоб отримати координати.',
    ],

];