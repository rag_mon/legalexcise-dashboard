<?php

return [

    'title_address_map' => 'Карта адрес',

    'license_information' => 'Інформація про ліцензії',

    // Basic statistic columns
    'received_complaints' => 'Надійшло<br> скарг,<br>з них',
    'in_process' => 'На розгляді в <br>управлінні контролю <br>за обігом та <br>оподаткуванням<br>підакцизних товарів',
    'post_to_oy' => 'Передано до ОУ',
    'in_np' => 'Знаходяться в ГУ НП <br>(продаж неповнолітнім)',
    'verification_imposable' => 'Недоцільність<br> проведення <br>перевірок',
    'total_verification' => 'Проведено<br> перевірок',
    'total_financial_sanctions' => 'Застосовано штрафних <br>санкцій, грн',
    'total_confiscated_goods' => 'Вилучено продукції, <br>пляш./пач.',

];