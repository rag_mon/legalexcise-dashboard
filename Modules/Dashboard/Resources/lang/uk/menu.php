<?php

return [

    'home' => 'Головна',
    'addresses' => 'Адреса',
    'addresses_import' => 'Імпорт',
    'analytics' => 'Аналітика',
    'history' => 'Історія',
    'reports' => 'Звіти',
    'complaint' => 'Скарги',
    'db_updates' => 'Оновлення',

];