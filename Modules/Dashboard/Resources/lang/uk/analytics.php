<?php

return [

    'statistic_complaints' => 'Статистика скарг',
    'total_complaints' => 'Кількість отриманих скарг',

    'statistic_action_taken' => 'Статистика реакцій на скарги',
    'total_action_taken' => 'Кількість реакцій на скарги',

    'statistic_suspicious' => 'Додавання підозрілих адрес',
    'total_suspicious' => 'Кількість підозрілих адрес',

    'statistic_financial_sanctions' => 'Фінансові санкції',
    'total_financial_sanctions' => 'Сума фінансових санкцій',

    'statistic_complaints_and_action_taken' => 'Статистика скарг / обробок',

    'complaints' => 'Скарги',
    'processed' => 'Оброблені',

];