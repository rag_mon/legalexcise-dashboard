<?php

return [

    'status' => [
        'not_ready' => 'Не готовий',
        'ready' => 'Готовий',
        'rendering' => 'Обробляється',
    ],

    'created_success' => 'Звіт ":name" в процесі створення',

    'create_report_modal_title' => 'Створити звіт',

    // Index
    'list_title' => 'Звіти',
    'create_report_btn' => 'Створити звіт',
    'user' => 'Користувач',
    'status_title' => 'Статус',
    'date' => 'Дата',
    'action' => 'Операція',

];