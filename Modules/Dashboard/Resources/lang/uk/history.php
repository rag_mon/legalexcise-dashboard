<?php

return [

    'events' => 'Події',
    'user' => 'Користувач',
    'user_group' => 'Група',
    'action' => 'Дія',
    'date' => 'Дата',

    'address' => [
        'created' => 'Створено адреса ":address".',
        'deleted' => 'Видалена адреса ":address".',
        'updated' => 'Оновлена адреса ":address".',
        'update_released' => 'Видано оновлення бази даних адрес.',
        'addresses_uploaded' => 'Таблиця адрес ":uploaded_filename" успішно завантажена.',
        'addresses_parsed' => 'Таблиця адрес ":uploaded_filename" успішно оброблена і імпортована.',
        'addresses_geocoded' => 'Завдання визначення координат адрес успішно завершена.',
        'addresses_imported' => 'Адреси успішно імпортовані і оброблені.',
        'comment' => [
            'posted' => 'Доданий коментар до адреси ":address".',
        ],
        'action' => [
            'taken' => 'Розпочато дію до адреси ":address".',
        ],
    ],

    'complaint' => [
        'updated' => 'Оновлені дані по скарзі.',
        'deleted' => 'Вилучена скарга.',
        'transformed' => 'Перенесення скарги на карту.',
    ],

    'report' => [
        'created' => 'Подан запрос на создание отчёта ":name".',
        'downloaded' => 'Викачаний звіт ":name".',
        'deleted' => 'Видалений звіт ":name".',
        'rendered' => 'Звіт ":name" успішно створений і готовий до завантаження.',
    ],

    'user_groups' => [
        'sfs' => 'фіскальна служба',
        'police' => 'нац. поліція',
    ],

];