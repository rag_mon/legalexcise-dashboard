@extends('dashboard::layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="row">
            {{--<div class="col-md-12">--}}
                {{--<nav class="breadcrumb"><a class="breadcrumb-item" href="#">Главная</a> <a class="breadcrumb-item active"--}}
                                                                                           {{--href="#">Отчеты</a></nav>--}}
            {{--</div>--}}
            <div class="col-md-12">
                <div class="bgc-white bd bdrs-3 p-20 mB-20"><h4 class="c-grey-900 mB-20">{{ __('dashboard::report.list_title') }}</h4>
                    <div class="peer">
                        <button data-toggle="modal" data-target="#modalCreateReport" style="margin-bottom:1rem" type="button"
                                class="btn cur-p btn-primary" id="btnCreateReport">
                            {{ __('dashboard::report.create_report_btn') }}
                        </button>
                    </div>
                    <table id="reportListTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>{{ __('dashboard::report.user') }}</th>
                            {{--<th>Название отчёта</th>--}}
                            <th>{{ __('dashboard::report.status_title') }}</th>
                            <th>{{ __('dashboard::report.date') }}</th>
                            <th>{{ __('dashboard::report.action') }}</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>{{ __('dashboard::report.user') }}</th>
                            {{--<th>Название отчёта</th>--}}
                            <th>{{ __('dashboard::report.status_title') }}</th>
                            <th>{{ __('dashboard::report.date') }}</th>
                            <th>{{ __('dashboard::report.action') }}</th>
                        </tr>
                        </tfoot>
                        <tbody>
                            <!-- Server-side processing -->
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modals')
    @include('dashboard::report.modal.create_report')
    @include('dashboard::modal.submit_message')
@endsection