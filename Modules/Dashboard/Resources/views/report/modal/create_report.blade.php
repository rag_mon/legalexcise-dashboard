<div class="modal fade" id="modalCreateReport" tabindex="-1" role="dialog" aria-labelledby="modalCreateReport"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header"><h5 class="modal-title">{{ __('dashboard::report.create_report_modal_title') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
            </div>
            <div id="createReportDataPicker"></div>
        </div>
    </div>
</div>