@extends('dashboard::layouts.master')

@section('content')
    <div id="mainContent">
        <div class="row gap-20 masonry pos-r">
            <div class="masonry-sizer col-md-6"></div>
            {{--<div class="masonry-item w-100 col-12">--}}
            {{--<div class="row gap-20">--}}
            {{--<div class="col-md-2">--}}
            {{--@widget('modules.dashboard.widgets.totalAddresses')--}}
            {{--</div>--}}
            {{--<div class="col-md-2">--}}
            {{--@widget('modules.dashboard.widgets.totalComplaints')--}}
            {{--</div>--}}
            {{--<div class="col-md-2">--}}
            {{--@widget('modules.dashboard.widgets.totalProcessedComplaints')--}}
            {{--</div>--}}
            {{--<div class="col-md-2">--}}
            {{--@widget('modules.dashboard.widgets.totalNotProcessedComplaints')--}}
            {{--</div>--}}
            {{--<div class="col-md-2">--}}
            {{--@widget('modules.dashboard.widgets.totalActiveAddresses')--}}
            {{--</div>--}}
            {{--<div class="col-md-2">--}}
            {{--@widget('modules.dashboard.widgets.totalSuspiciousAddresses')--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}

            <div class="masonry-item w-100 col-12">
                <div class="row gap-20">
                    <div class="col-md-12">
                        <style type="text/css">
                            .tg {
                                border-collapse: collapse;
                                border-spacing: 0;
                            }

                            .tg td {
                                font-family: Arial, sans-serif;
                                font-size: 14px;
                                padding: 10px 5px;
                                border-style: solid;
                                border-width: 1px;
                                overflow: hidden;
                                word-break: normal;
                                border-color: black;
                            }

                            .tg th {
                                font-family: Arial, sans-serif;
                                font-size: 14px;
                                font-weight: normal;
                                padding: 10px 5px;
                                border-style: solid;
                                border-width: 1px;
                                overflow: hidden;
                                word-break: normal;
                                border-color: black;
                            }

                            .tg .tg-hgcj {
                                font-weight: bold;
                                text-align: center
                            }

                            .tg .tg-amwm {
                                font-weight: bold;
                                text-align: center;
                                vertical-align: top
                            }

                            .tg .tg-yw4l {
                                vertical-align: top
                            }
                        </style>
                        <table class="tg">
                            <tr>
                                <th class="tg-hgcj">{!! __('dashboard::homepage.received_complaints') !!}</th>
                                <th class="tg-amwm">{!! __('dashboard::homepage.in_process') !!}</th>
                                <th class="tg-amwm">{!! __('dashboard::homepage.post_to_oy') !!}</th>
                                <th class="tg-amwm">{!! __('dashboard::homepage.in_np') !!}</th>
                                <th class="tg-amwm">{!! __('dashboard::homepage.verification_imposable') !!}</th>
                                <th class="tg-amwm">{!! __('dashboard::homepage.total_verification') !!}</th>
                                <th class="tg-amwm">{!! __('dashboard::homepage.total_financial_sanctions') !!}</th>
                                <th class="tg-amwm">{!! __('dashboard::homepage.total_confiscated_goods') !!}</th>
                            </tr>
                            <tr>
                                <td class="tg-yw4l">{{ $totalComplaints }}</td>
                                <td class="tg-yw4l">{{ isset($bookkeeping) ? $bookkeeping->examination_of_excisable_goods : '0' }}</td>
                                <td class="tg-yw4l">{{ isset($bookkeeping) ? $bookkeeping->transferred_to_the_oy : '0' }}</td>
                                <td class="tg-yw4l">{{ isset($bookkeeping) ? $bookkeeping->sale_to_minors : '0' }}</td>
                                <td class="tg-yw4l">{{ $totalComplaintsVerificationImpossible }}</td>
                                <td class="tg-yw4l">{{ $totalActionTaken }}</td>
                                <td class="tg-yw4l">{{ $totalFinancialSanction }} грн.</td>
                                <td class="tg-yw4l">{{ $totalConfiscatedGoods }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="masonry-item w-100 col-12">
                <div class="bd bgc-white">
                    <div class="peers fxw-nw@lg+ ai-s">
                        <div class="peer peer-greed w-100p@lg+ w-100@lg- p-20">
                            <div class="layers">
                                <div class="layer w-100 mB-10"><h6 class="lh-1">{{ __('dashboard::homepage.title_address_map') }}</h6></div>
                                <div class="layer w-100">
                                    <div id="google-map-addresses" style="padding-bottom:75%"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modals')
    @include('dashboard::modal.address_marker_map')
@endsection