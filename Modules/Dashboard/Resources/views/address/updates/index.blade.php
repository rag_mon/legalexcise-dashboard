@extends('dashboard::layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="row">
            {{--<div class="col-md-12">--}}
                {{--<nav class="breadcrumb"><a class="breadcrumb-item" href="#">Главная</a> <a class="breadcrumb-item active"--}}
                                                                                           {{--href="#">Список адресов</a>--}}
                {{--</nav>--}}
            {{--</div>--}}
            <div class="col-md-12">
                <div class="bgc-white bd bdrs-3 p-20 mB-20"><h4 class="c-grey-900 mB-20">{{ __('dashboard::address.updates.updates_list') }}</h4>
                    <table id="updatesListTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>{{ __('dashboard::address.updates.update_id') }}</th>
                            <th>{{ __('dashboard::address.updates.timestamp') }}</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>{{ __('dashboard::address.updates.update_id') }}</th>
                            <th>{{ __('dashboard::address.updates.timestamp') }}</th>
                        </tr>
                        </tfoot>
                        <tbody>
                            <!-- Server-side processing -->
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modals')
    @include('dashboard::address.updates.modal.confirm_update_db')
@endsection