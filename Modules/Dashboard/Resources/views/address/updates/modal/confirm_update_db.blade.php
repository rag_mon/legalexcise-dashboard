<div class="modal fade" id="modalUpdateDBConfirm" tabindex="-1" role="dialog"
     aria-labelledby="modalAddressDeleteConfirm" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header"><h5 class="modal-title" id="modalUpdateDBConfirmLabel">{{ __('dashboard::address.updates.title') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body"><p>{{ __('dashboard::address.updates.question') }}</p></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('dashboard::popup.cancel') }}</button>
                <button type="button" class="btn btn-danger btn-updates" data-dismiss="modal">{{ __('dashboard::address.updates.update') }}</button>
            </div>
        </div>
    </div>
</div>