<div class="modal fade" id="modalAddressDeleteConfirm" tabindex="-1" role="dialog"
     aria-labelledby="modalAddressDeleteConfirm" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header"><h5 class="modal-title" id="modalAddressDeleteConfirmLabel">{{ __('dashboard::address.delete_confirmation') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body"><p>{{ __('dashboard::address.question_delete_address') }} <b class="address-value">...</b> ?</p></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('dashboard::popup.cancel') }}</button>
                <button type="button" class="btn btn-danger btn-delete">{{ __('dashboard::popup.delete') }}</button>
            </div>
        </div>
    </div>
</div>