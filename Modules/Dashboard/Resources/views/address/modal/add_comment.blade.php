<div class="modal fade" id="modalAddressAddComment" tabindex="-1" role="dialog" aria-labelledby="modalAddressAddComment"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header"><h5 class="modal-title" id="modalAddressAddCommentLabel">{{ __('dashboard::address.title_new_comment') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form class="addressCommentForm">
                    <div class="form-group"><label for="addressCommentText">{{ __('dashboard::address.comment') }}</label>
                        <textarea id="addressCommentText" class="form-control" rows="8"></textarea>
                        <small class="form-text text-muted">{{ __('dashboard::address.action_describe') }}</small>
                        <div id="addressCommentValidMessage" class="alert alert-danger d-none" role="alert"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('dashboard::popup.close') }}</button>
                <button type="button" class="btn btn-success btn-add">{{ __('dashboard::popup.add') }}</button>
            </div>
        </div>
    </div>
</div>