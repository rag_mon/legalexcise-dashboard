@extends('dashboard::layouts.master')

@section('content')
    <div class="container-fluid">

        @if (session('success'))
            <div class="alert alert-success" role="alert">{{ session('success') }}</div>
        @endif

        <div class="row">
            {{--<div class="col-md-12">--}}
                {{--<nav class="breadcrumb"><a class="breadcrumb-item" href="#">Главная</a> <a class="breadcrumb-item active"--}}
                                                                                           {{--href="#">Список адресов</a>--}}
                {{--</nav>--}}
            {{--</div>--}}
            <div class="col-md-12">
                <div class="bgc-white bd bdrs-3 p-20 mB-20">
                    <div class="d-flex flex-row justify-content-between">
                        <div class=""><h4 class="c-grey-900 mB-20">{{ __('dashboard::address.title_list') }}</h4></div>
                        <div class=""><a href="{{ route('dashboard.address.create') }}" class="btn btn-primary">{{ __('dashboard::address.create_address') }}</a></div>
                    </div>
                    <table id="addressListTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>{{ __('dashboard::address.label_address') }}</th>
                            <th>{{ __('dashboard::address.label_license') }}</th>
                            <th>{{ __('dashboard::address.label_start_license') }}</th>
                            <th>{{ __('dashboard::address.label_end_license') }}</th>
                            <th>{{ __('dashboard::address.label_status') }}</th>
                            <th>{{ __('dashboard::address.label_actions') }}</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>{{ __('dashboard::address.label_address') }}</th>
                            <th>{{ __('dashboard::address.label_license') }}</th>
                            <th>{{ __('dashboard::address.label_start_license') }}</th>
                            <th>{{ __('dashboard::address.label_end_license') }}</th>
                            <th>{{ __('dashboard::address.label_status') }}</th>
                            <th>{{ __('dashboard::address.label_actions') }}</th>
                        </tr>
                        </tfoot>
                        <tbody>
                            <!-- Server-side processing -->
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modals')
    @include('dashboard::address.modal.delete_confirmation')

    @include('dashboard::address.modal.add_comment')

    @include('dashboard::modal.pre_loader')

    @include('dashboard::modal.submit_message')
@endsection

@section('scripts')
    <!-- Address list row operations dropdown template -->
    {{--<script id="addressListRowOptionsDropdownTemplate" type="application/xhtml+xml">--}}
        {{--<div class="dropdown">--}}
            {{--<button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton"--}}
                    {{--data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                {{--Действие--}}
            {{--</button>--}}
            {{--<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">--}}
                {{--<a class="dropdown-item " href="#"--}}
                   {{--data-address-id="#"--}}
                   {{--data-toggle="modal"--}}
                   {{--data-target="#modalAddressAddComment">{{ __('dashboard::address.btn_new_comment') }}</a>--}}
                {{--<a class="dropdown-item" href="#">{{ __('dashboard::address.btn_show') }}</a>--}}
                {{--<a class="dropdown-item" href="#">{{ __('dashboard::address.btn_show_on_map') }}</a>--}}
                {{--<a class="dropdown-item" href="#">{{ __('dashboard::address.btn_control') }}</a>--}}
                {{--<a class="dropdown-item btn-delete-address" href="#"--}}
                   {{--data-toggle="modal"--}}
                   {{--data-target="#modalAddressDeleteConfirm"--}}
                   {{--data-title="">{{ __('dashboard::address.btn_delete') }}</a>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</script>--}}
@endsection