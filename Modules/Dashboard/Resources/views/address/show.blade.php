@extends('dashboard::layouts.master')

@section('content')
    <div class="full-container">

        <div class="bgc-white bd bdrs-3 p-20 mB-20">

            <h4 class="c-grey-900 mB-20">{{ __('dashboard::address.label_address') }}: {{ $address->address }}</h4>
            <table class="table table-bordered">
                <tr>
                    <th scope="col">{{ __('dashboard::address.label_id_code') }}</th>
                    <td>{{ $address->id_code }}</td>
                </tr>
                <tr>
                    <th scope="col">{{ __('dashboard::address.label_lng') }}</th>
                    <td>{{ $address->lng }}</td>
                </tr>
                <tr>
                    <th scope="col">{{ __('dashboard::address.label_lat') }}</th>
                    <td>{{ $address->lat }}</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <a href="{{ $address->googleMapUrl }}" target="_blank" class="btn btn-primary">{{ __('dashboard::address.btn_show_on_map') }}</a>
                    </td>
                </tr>
                <tr>
                    <th scope="col">{{ __('dashboard::address.label_license') }}</th>
                    <td>{{ $address->license }}</td>
                </tr>
                <tr>
                    <th scope="col">{{ __('dashboard::address.label_company') }}</th>
                    <td>{{ $address->company }}</td>
                </tr>
                <tr>
                    <th scope="col">{{ __('dashboard::address.label_company_type') }}</th>
                    <td>{{ $address->company_type }}</td>
                </tr>
                <tr>
                    <th scope="col">{{ __('dashboard::address.label_license_type') }}</th>
                    <td>{{ __("dashboard::address.license_type.{$address->license_type}") }}</td>
                </tr>
                <tr>
                    <th scope="col">{{ __('dashboard::address.label_status') }}</th>
                    <td>{{ __("dashboard::address.status.{$address->status}") }}</td>
                </tr>
                <tr>
                    <th scope="col">{{ __('dashboard::address.label_start_license') }}</th>
                    <td>{{ $address->license_start_at }}</td>
                </tr>
                <tr>
                    <th scope="col">{{ __('dashboard::address.label_end_license') }}</th>
                    <td>{{ $address->license_end_at }}</td>
                </tr>
                <tr>
                    <th scope="col">{{ __('dashboard::address.label_comment') }}</th>
                    <td>
                        {!! $address->comment !!}
                    </td>
                </tr>
                <tr>
                    <th scope="col">{{ __('dashboard::address.label_created_at') }}</th>
                    <td>{{ $address->created_at }}</td>
                </tr>
                <tr>
                    <th scope="col">{{ __('dashboard::address.label_updated_at') }}</th>
                    <td>{{ $address->updated_at }}</td>
                </tr>
                </tbody>
            </table>

        </div>

    </div>
@endsection