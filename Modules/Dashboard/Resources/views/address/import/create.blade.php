@extends('dashboard::layouts.master')

@section('content')
    <div class="container-fluid">
        <h1>{{ __('dashboard::address.import.title') }}</h1>
        <hr>

        <div class="panel panel-default">
            <form action="{{ route('dashboard.address.import.store') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="alert alert-info">
                    {{ __('dashboard::address.import.support_formats') }}
                    <ul title="Примеры структур excel таблиц">
                        <li><a href="{{ asset('downloads/licenses_example.xlsx') }}" target="_blank">Скачать пример таблицы лицензий (адресов)</a></li>
                        <li><a href="{{ asset('downloads/no_licenses_example.xlsx') }}" target="_blank">Скачать пример таблицы подозрительных адресов (красные адреса)</a></li>
                    </ul>
                </div>

                <hr>

                <p>
                    <input type="file" name="uploaded_file">
                </p>

                <div class="btn-group" data-toggle="buttons">
                    <label class="btn btn-primary active">
                        <input type="radio" name="type" autocomplete="off" value="licenses" checked>
                        {{ __('dashboard::address.import.type_licenses') }}
                    </label>
                    <label class="btn btn-warning">
                        <input type="radio" name="type" autocomplete="off" value="suspicious">
                        {{ __('dashboard::address.import.type_suspicious') }}
                    </label>
                </div>

                {{-- Info-messages --}}
                @if (session('success'))
                    <div class="alert alert-info" role="alert">
                        {{session('success')}}
                    </div>
                @endif

                {{-- Other errors --}}
                @if (isset($error))
                    <div class="alert alert-danger" role="alert">
                        {{ $error }}
                    </div>
                @endif

                {{-- Validation errors --}}
                @foreach($errors->all() as $error)
                    <div class="alert alert-danger" role="alert">
                        {{ $error }}
                    </div>
                @endforeach

                <div>
                    <input type="submit" value="{{ __('dashboard::address.import.upload') }}" class="btn btn-success">
                </div>
            </form>

        </div>
    </div>
@endsection