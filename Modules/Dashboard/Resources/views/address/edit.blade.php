@extends('dashboard::layouts.master')

@section('content')
    <div class="full-container">
        <div class="bgc-white bd bdrs-3 p-20 mB-20">
            {{--<nav class="breadcrumb"><a class="breadcrumb-item" href="#">Главная</a> <a class="breadcrumb-item active"--}}
            {{--href="#">Список адресов</a></nav>--}}
            <h4 class="c-grey-900 mB-20">{{ __('dashboard::address.control_address', ['address' => $address->address]) }}</h4>
            <form id="formAddressEdit" class="row">
                <div class="col-md-3 form-group"><label
                            for="inputINN">{{ __('dashboard::address.label_id_code') }}</label> <input type="text"
                                                                                                       name="id_code"
                                                                                                       class="form-control"
                                                                                                       id="inputINN"
                                                                                                       value="{{ $address->id_code }}"
                                                                                                       placeholder="{{ __('dashboard::address.type_id_code') }}">
                    <div class="invalid-feedback"></div>
                </div>
                <div class="col-md-3 form-group"><label
                            for="inputAddress">{{ __('dashboard::address.label_address') }}</label> <input type="text"
                                                                                                           name="address"
                                                                                                           class="form-control"
                                                                                                           id="inputAddress"
                                                                                                           value="{{ $address->address }}"
                                                                                                           placeholder="{{ __('dashboard::address.type_address') }}">
                    <div class="invalid-feedback"></div>
                </div>
                <div class="col-md-3 form-group"><label
                            for="inputLicenseCode">{{ __('dashboard::address.label_license') }}</label> <input
                            type="text"
                            name="license"
                            class="form-control"
                            id="inputLicenseCode"
                            value="{{ $address->license }}"
                            placeholder="{{ __('dashboard::address.type_license') }}">
                    <div class="invalid-feedback"></div>
                </div>
                <div class="col-md-3 form-group"><label
                            for="inputStatus">{{ __('dashboard::address.label_status') }}</label>
                    <select id="inputStatus"
                            name="status"
                            class="form-control">
                        @foreach(\Modules\Dashboard\Models\Address::$statuses as $status)
                            <option value="{{ $status }}"{{ $address->status == $status ? ' selected' : '' }}>{{ __("dashboard::address.status.{$status}") }}</option>
                        @endforeach
                    </select>
                    <div class="invalid-feedback"></div>
                </div>
                <div class="col-md-3 form-group"><label
                            for="inputLicenseCode">{{ __('dashboard::address.label_lat') }}</label> <input type="text"
                                                                                                           name="lat"
                                                                                                           class="form-control"
                                                                                                           id="inputLat"
                                                                                                           value="{{ $address->lat }}"
                                                                                                           placeholder="{{ __('dashboard::address.type_lat') }}">
                    <div class="invalid-feedback"></div>
                </div>
                <div class="col-md-3 form-group"><label
                            for="inputLicenseCode">{{ __('dashboard::address.label_lng') }}</label> <input type="text"
                                                                                                           name="lng"
                                                                                                           class="form-control"
                                                                                                           id="inputLong"
                                                                                                           value="{{ $address->lng }}"
                                                                                                           placeholder="{{ __('dashboard::address.type_lng') }}">
                    <div class="invalid-feedback"></div>
                </div>
                <div class="col-md-3 form-group"><label
                            for="inputLicenseCode">{{ __('dashboard::address.lat_lng') }}</label> <a
                            id="get-cords-from-map" class="form-control btn btn-primary" href="#" data-toggle="modal"
                            data-target="#modalAddressMap">{{ __('dashboard::address.select_on_map') }}</a></div>
                <div class="col-md-3 form-group">
                    <label for="inputCompany">{{ __('dashboard::address.company') }}</label>
                    <input type="text" name="company" class="form-control" id="inputCompany"
                           placeholder="{{ __('dashboard::address.type_company') }}"
                           value="{{ $address->company }}">
                    <div class="invalid-feedback"></div>
                </div>
                <div class="col-md-3 form-group"><label
                            for="inputLicenseCode">{{ __('dashboard::address.company_type') }}</label> <input
                            type="text"
                            name="company_type"
                            class="form-control"
                            id="inputObjectType"
                            value="{{ $address->company_type }}"
                            placeholder="{{ __('dashboard::address.type_company_type') }}">
                    <div class="invalid-feedback"></div>
                </div>
                <div class="col-md-3 form-group"><label
                            for="inputLicenseCode">{{ __('dashboard::address.label_start_license') }}</label> <input
                            type="text"
                            name="license_start_at"
                            class="form-control start-date"
                            value="{{ $address->license_start_at }}"
                            placeholder="{{ __('dashboard::address.datetime_format') }}"
                            data-provide="datepicker">
                    <div class="invalid-feedback"></div>
                </div>
                <div class="col-md-3 form-group"><label
                            for="inputLicenseCode">{{ __('dashboard::address.label_end_license') }}</label> <input
                            type="text"
                            name="license_end_at"
                            class="form-control start-date"
                            value="{{ $address->license_end_at }}"
                            placeholder="{{ __('dashboard::address.datetime_format') }}"
                            data-provide="datepicker">
                    <div class="invalid-feedback"></div>
                </div>
                <div class="col-md-3 form-group"><label
                            for="inputConfiscatedGoods">{{ __('dashboard::address.confiscated_goods') }}</label>
                    <input type="text" id="inputConfiscatedGoods" name="confiscated_goods" class="form-control" value="{{ $address->public_notices['confiscated_goods'] }}"
                           placeholder="{{ __('dashboard::address.confiscated_goods') }}">
                    <div class="invalid-feedback"></div>
                </div>
                <div class="col-md-3 form-group"><label
                            for="inputProtocolDrawnUp">{{ __('dashboard::address.protocol_drawn_up') }}</label> <input
                            type="text" name="protocol_drawn_up" id="inputProtocolDrawnUp"
                            class="form-control start-date"
                            value="{{ $address->public_notices['protocol_drawn_up'] }}"
                            placeholder="{{ __('dashboard::address.datetime_format') }}" data-provide="datepicker">
                    <div class="invalid-feedback"></div>
                </div>
                <div class="col-md-3 form-group"><label
                            for="inputFinancialSanctions">{{ __('dashboard::address.financial_sanctions') }}</label>
                    <input type="text" name="financial_sanctions" id="inputFinancialSanctions" class="form-control"
                           value="{{ $address->public_notices['financial_sanctions'] }}"
                           placeholder="{{ __('dashboard::address.financial_sanctions') }}">
                    <div class="invalid-feedback"></div>
                </div>
                <div class="col-md-12"><a href="{{ route('dashboard.address.index') }}"
                                          class="btn btn-secondary btn-cancel">{{ __('dashboard::address.cancel') }}</a>
                    <a
                            class="btn btn-danger dropdown-item-delete" href="#" data-toggle="modal"
                            data-target="#modalAddressDeleteConfirm" data-address-id="{{ $address->id }}"
                            data-redirect="{{ route('dashboard.address.index') }}"
                            data-title="{{ $address->address }}">{{ __('dashboard::address.delete') }}</a>
                    <button type="button" data-address-id="{{ $address->id }}" data-title="{{ $address->address }}"
                            class="btn btn-primary address-submit">{{ __('dashboard::address.save') }}
                    </button>
                    <button type="button" data-address-id="{{ $address->id }}" data-title="{{ $address->address }}"
                            data-redirect="{{ route('dashboard.address.index') }}"
                            class="btn btn-success address-submit">{{ __('dashboard::address.save_and_close') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('modals')
    <div class="modal fade" id="modalSubmitMessage" tabindex="-1" role="dialog" aria-labelledby="modalSubmitMessage"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header"><h5 class="modal-title" id="modalSubmitMessageLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body"><p class="data-message-value"></p></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">{{ __('dashboard::popup.close') }}</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalAddressDeleteConfirm" tabindex="-1" role="dialog"
         aria-labelledby="modalAddressDeleteConfirm" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header"><h5 class="modal-title" id="modalAddressDeleteConfirmLabel">{{ __('dashboard::address.delete_confirmation') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body"><p>{{ __('dashboard::address.question_delete_address') }} <b class="address-value">...</b> ?</p></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('dashboard::popup.cancel') }}</button>
                    <button type="button" class="btn btn-danger btn-delete" data-redirect="">{{ __('dashboard::popup.delete') }}</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalAddressMap" tabindex="-1" role="dialog" aria-labelledby="modalAddressMap"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header"><h5 class="modal-title" id="modalAddressMapLabel">{{ __('dashboard::address.map.change_pick_point') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div id="google-map" style="padding-bottom:75%"></div>
                    <div class="alert alert-light" id="get-cords-result" role="alert">
                        {{ __('dashboard::address.map.tooltip') }}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">{{ __('dashboard::popup.close') }}</button>
                </div>
            </div>
        </div>
    </div>
@endsection