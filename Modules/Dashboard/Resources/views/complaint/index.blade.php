@extends('dashboard::layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="row">
            {{--<div class="col-md-12">--}}
                {{--<nav class="breadcrumb"><a class="breadcrumb-item" href="#">Главная</a> <a class="breadcrumb-item active"--}}
                                                                                           {{--href="#">Список жалоб</a></nav>--}}
            {{--</div>--}}
            <div class="col-md-12">
                <div class="layers bd bgc-white p-20">
                    <table id="homepageTable" class="table table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>{!! __('dashboard::complaint.verifications') !!}</th>
                            <th>{!! __('dashboard::complaint.financial_sanction_taken') !!}</th>
                            <th>{!! __('dashboard::complaint.confiscated_goods_alt') !!}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{ $totalActionTaken }}</td>
                            <td>{{ $totalFinancialSanction }} грн.</td>
                            <td>{{ $totalConfiscatedGoods }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-12">
                <div class="bgc-white bd bdrs-3 p-20 mB-20"><h4 class="c-grey-900 mB-20">{{ __('dashboard::complaint.title_list') }}</h4>
                    <table id="complaintListTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>{{ __('dashboard::complaint.receive_date') }}</th>
                            <th>{{ __('dashboard::complaint.address') }}</th>
                            <th>{{ __('dashboard::complaint.company') }}</th>
                            <th>{{ __('dashboard::complaint.complaint_text') }}</th>
                            <th>{{ __('dashboard::complaint.code_np') }}</th>
                            <th>{{ __('dashboard::complaint.code_to') }}</th>
                            <th>{{ __('dashboard::complaint.dfs_department_code') }}</th>
                            <th>{{ __('dashboard::complaint.dfs_executor_alt') }}</th>
                            <th>
                              {{ __('dashboard::complaint.status') }}
                              <br>
                              <select id="complaintListTableStatusFilter">
                                <option value="">{{ __('dashboard::complaint.all') }}</option>
                                <option value="not_processed">{{ __('dashboard::complaint.statuses.not_processed') }}</option>
                                <option value="verification_possible">{{ __('dashboard::complaint.statuses.verification_possible') }}</option>
                                <option value="verification_impossible">{{ __('dashboard::complaint.statuses.verification_impossible') }}</option>
                                <option value="processed">{{ __('dashboard::complaint.statuses.processed') }}</option>
                                <option value="transferred_to_oy">{{ __('dashboard::complaint.statuses.transferred_to_oy') }}</option>
                                <option value="in_processing">{{ __('dashboard::complaint.statuses.in_processing') }}</option>
                              </select>
                            </th>
                            <th>{{ __('dashboard::complaint.confiscated_goods') }}</th>
                            <th>{{ __('dashboard::complaint.protocol_drawn_up') }}</th>
                            <th>{{ __('dashboard::complaint.financial_sanctions') }}</th>
                            <th>{{ __('dashboard::complaint.operations') }}</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>{{ __('dashboard::complaint.receive_date') }}</th>
                            <th>{{ __('dashboard::complaint.address') }}</th>
                            <th>{{ __('dashboard::complaint.company') }}</th>
                            <th>{{ __('dashboard::complaint.complaint_text') }}</th>
                            <th>{{ __('dashboard::complaint.code_np') }}</th>
                            <th>{{ __('dashboard::complaint.code_to') }}</th>
                            <th>{{ __('dashboard::complaint.dfs_department_code') }}</th>
                            <th>{{ __('dashboard::complaint.dfs_executor_alt') }}</th>
                            <th>{{ __('dashboard::complaint.status') }}</th>
                            <th>{{ __('dashboard::complaint.confiscated_goods') }}</th>
                            <th>{{ __('dashboard::complaint.protocol_drawn_up') }}</th>
                            <th>{{ __('dashboard::complaint.financial_sanctions') }}</th>
                            <th>{{ __('dashboard::complaint.operations') }}</th>
                        </tr>
                        </tfoot>
                        <tbody>
                            <!-- Server-side processing -->
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modals')
    @include('dashboard::complaint.modal.set_np_code')

    @include('dashboard::complaint.modal.set_to_code')

    @include('dashboard::complaint.modal.delete_confirmation')

    @include('dashboard::complaint.modal.transformation_confirmation')

    @include('dashboard::complaint.modal.confiscated_goods')

    @include('dashboard::modal.pre_loader')

    @include('dashboard::modal.submit_message')

    @include('dashboard::complaint.modal.protocol_drawn_up')

    @include('dashboard::complaint.modal.financial_sanctions')

    @include('dashboard::complaint.modal.dfs_department_code')

    @include('dashboard::complaint.modal.dfs_executor')
@endsection
