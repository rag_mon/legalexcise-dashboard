<div class="modal fade" id="modalComplaintProtocolDrawnUp" tabindex="-1" role="dialog"
     aria-labelledby="modalComplaintProtocolDrawnUp" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalComplaintProtocolDrawnUpLabel">{{ __('dashboard::complaint.change_protocol_drawn_up') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form class="addressCommentForm">
                    <div class="form-group"><label>{{ __('dashboard::complaint.change_protocol_drawn_up') }}</label> <input type="text"
                                                                                                     name="protocol_drawn_up"
                                                                                                     id="inputProtocolDrawnUp"
                                                                                                     class="form-control start-date"
                                                                                                     placeholder="дд.мм.гггг"
                                                                                                     data-provide="datepicker">
                        <div class="invalid-feedback"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('dashboard::popup.close') }}</button>
                <button type="button" class="btn btn-success btn-add">{{ __('dashboard::popup.add') }}</button>
            </div>
        </div>
    </div>
</div>