<div class="modal fade" id="modalComplaintDfsDepartmentCode" tabindex="-1" role="dialog"
     aria-labelledby="modalComplaintDfsDepartmentCode" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header"><h5 class="modal-title">{{ __('dashboard::complaint.dfs_department_code') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form class="addressCommentForm">
                    <div class="form-group"><label>{{ __('dashboard::complaint.change_dfs_department_code') }}</label> <input type="text" name="dfs_department_code"
                                                                                       class="form-control" placeholder="{{ __('dashboard::complaint.placeholder_change_dfs_department_code') }}">
                        <div class="invalid-feedback"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('dashboard::popup.close') }}</button>
                <button type="button" class="btn btn-success btn-add">{{ __('dashboard::popup.add') }}</button>
            </div>
        </div>
    </div>
</div>