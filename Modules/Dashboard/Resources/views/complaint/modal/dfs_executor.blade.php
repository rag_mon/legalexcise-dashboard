<div class="modal fade" id="modalComplaintDfsExecutor" tabindex="-1" role="dialog"
     aria-labelledby="modalComplaintDfsExecutor" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header"><h5 class="modal-title">{{ __('dashboard::complaint.dfs_executor_alt') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form class="addressCommentForm">
                    <div class="form-group"><label>{{ __('dashboard::complaint.set_dfs_executor') }}</label> <input type="text" name="dfs_executor"
                                                                                          class="form-control" placeholder="{{ __('dashboard::complaint.placeholder_set_dfs_executor') }}">
                        <div class="invalid-feedback"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('dashboard::popup.close') }}</button>
                <button type="button" class="btn btn-success btn-add">{{ __('dashboard::popup.add') }}</button>
            </div>
        </div>
    </div>
</div>