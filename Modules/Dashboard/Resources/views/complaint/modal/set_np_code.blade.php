<div class="modal fade" id="modalAddNpCode" tabindex="-1" role="dialog" aria-labelledby="modalAddNpCode"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header"><h5 class="modal-title" id="modalAddNpCodeLabel">{{ __('dashboard::complaint.type_np_code') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form class="addressCommentForm">
                    <div class="form-group"><label for="NpCodeText">{{ __('dashboard::complaint.code_np') }}</label> <input type="text" id="NpCodeText"
                                                                                                                            class="form-control">
                        <div class="invalid-feedback"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('dashboard::popup.close') }}</button>
                <button type="button" class="btn btn-success btn-add">{{ __('dashboard:popup.add') }}</button>
            </div>
        </div>
    </div>
</div>