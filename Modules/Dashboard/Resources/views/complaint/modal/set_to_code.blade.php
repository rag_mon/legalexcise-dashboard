<div class="modal fade" id="modalAddToCode" tabindex="-1" role="dialog" aria-labelledby="modalAddToCode"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header"><h5 class="modal-title" id="modalAddToCodeLabel">{{ __('dashboard::complaint.type_to_code') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form class="addressCommentForm">
                    <div class="form-group"><label for="ToCodeText">{{ __('dashboard::complaint.code_to') }}</label> <input type="text" id="ToCodeText"
                                                                                                                            class="form-control">
                        <div class="invalid-feedback"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('dashboard::popup.close') }}</button>
                <button type="button" class="btn btn-success btn-add">{{ __('dashboard:popup.add') }}</button>
            </div>
        </div>
    </div>
</div>