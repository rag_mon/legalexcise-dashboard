<div class="modal fade" id="modalComplaintToAddressConfirm" tabindex="-1" role="dialog"
     aria-labelledby="modalComplaintToAddressConfirm" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header"><h5 class="modal-title" id="modalComplaintToAddressConfirmLabel">{{ __('dashboard::complaint.transformation_confirmation') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body"><p>{{ __('dashboard::complaint.question_transformation') }}</p></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('dashboard::popup.cancel') }}</button>
                <button type="button" class="btn btn-danger btn-delete">{{ __('dashboard::complaint.transform') }}</button>
            </div>
        </div>
    </div>
</div>