@extends('dashboard::layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="bgc-white bd bdrs-3 p-20 mB-20">
            {{--<nav class="breadcrumb"><a class="breadcrumb-item" href="#">{{  }}</a> <a class="breadcrumb-item active"--}}
                                                                                       {{--href="#">{{ __('dashboard::complaint.title_list') }}</a></nav>--}}
            <h4 class="c-grey-900 mB-20">{{ __('dashboard::complaint.short_complaint_description') }}</h4>
            <table class="table table-bordered">
                <tr>
                    <th scope="col">{{ __('dashboard::complaint.postman_name') }}</th>
                    <td>{{ $complaint->name }}</td>
                </tr>
                <tr>
                    <th scope="col">{{ __('dashboard::complaint.telephone') }}</th>
                    <td>{{ $complaint->telephone }}</td>
                </tr>
                <tr>
                    <th scope="col">{{ __('dashboard::complaint.company') }}</th>
                    <td>{{ $complaint->company }}</td>
                </tr>
                <tr>
                    <th scope="col">{{ __('dashboard::complaint.email') }}</th>
                    <td>{{ $complaint->email }}</td>
                </tr>
                <tr>
                    <th scope="col">{{ __('dashboard::complaint.message') }}</th>
                    <td>{{ $complaint->message }}</td>
                </tr>
                <tr>
                    <th scope="col">{{ __('dashboard::complaint.type') }}</th>
                    <td>{{ $complaint->type }}</td>
                </tr>
                {{--<tr>--}}
                    {{--<th scope="col">Статус анонимности</th>--}}
                    {{--<td>{{ $complaint->is_anonymously }}</td>--}}
                {{--</tr>--}}
                <tr>
                    <th scope="col">{{ __('dashboard::complaint.code_to') }}</th>
                    <td>{{ $complaint->to_code }}</td>
                </tr>
                <tr>
                    <th scope="col">{{ __('dashboard::complaint.code_np') }}</th>
                    <td>{{ $complaint->np_code }}</td>
                </tr>
                <tr>
                    <th scope="col">{{ __('dashboard::complaint.dfs_executor_alt') }}</th>
                    <td>{{ $complaint->dfs_executor }}</td>
                </tr>
                <tr>
                    <th scope="col">{{ __('dashboard::complaint.dfs_department_code') }}</th>
                    <td>{{ $complaint->dfs_department_code }}</td>
                </tr>
                <tr>
                    <th scope="col">{{ __('dashboard::complaint.receive_date') }}</th>
                    <td>{{ $complaint->created_at }}</td>
                </tr>
                <tr>
                    <th scope="col">{{ __('dashboard::complaint.comment') }}</th>
                    <td>{!! $complaint->comment !!}</td>
                </tr>
                <tr>
                    <th>=== {{ __('dashboard::complaint.common_information') }} ===</th>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <th scope="col">{{ __('dashboard::complaint.spd_name') }}</th>
                    <td>{{ $complaint->spd_name }}</td>
                </tr>
                <tr>
                    <th scope="col">{{ __('dashboard::complaint.spd_code') }}</th>
                    <td>{{ $complaint->spd_code }}</td>
                </tr>
                <tr>
                    <th scope="col">{{ __('dashboard::complaint.spd_address') }}</th>
                    <td>{{ $complaint->spd_address }}</td>
                </tr>
                <tr>
                    <th scope="col">{{ __('dashboard::complaint.spd_license') }}</th>
                    <td>{{ $complaint->spd_license }}</td>
                </tr>
                <tr>
                    <th>=== {{ __('dashboard::complaint.action_taken') }} ===</th>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <th scope="col">{{ __('dashboard::complaint.verified_at') }}</th>
                    <td>{{ $complaint->verified_at }}</td>
                </tr>
                <tr>
                    <th scope="col">{{ __('dashboard::complaint.explain_violations') }}</th>
                    <td>
                        @foreach ($violations as $violation)
                            @if ($complaint->inViolations($violation->id))
                                <div title="{{ $violation->description }}">{{ $violation->name }}</div>
                            @endif
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th scope="col">{{ __('dashboard::complaint.protocol') }}</th>
                    <td>{{ $complaint->protocol }}</td>
                </tr>
                {{--<tr>
                    <th scope="col">ФИО проверяющего</th>
                    <td>{{ $complaint->dfs_executor }}</td>
                </tr>--}}
                <tr>
                    <th>=== {{ __('dashboard::complaint.analytics') }} ===</th>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <th scope="col">{{ __('dashboard::complaint.financial_sanction_taken_alt') }}</th>
                    <td>{{ $complaint->actionTaken->sum('financial_sanctions') }} грн</td>
                </tr>
                <tr>
                    <th scope="col">{{ __('dashboard::complaint.confiscated_goods') }}</th>
                    <td>{{ $complaint->actionTaken->sum('confiscated_goods') }}</td>
                </tr>
            </table>
        </div>
    </div>
@endsection
