@extends('dashboard::layouts.master')

@section('content')
    <div class="full-container">
        <div class="bgc-white bd bdrs-3 p-20 mB-20">
            {{--<nav class="breadcrumb"><a class="breadcrumb-item" href="#">Главная</a> <a class="breadcrumb-item active"--}}
            {{--href="#">Список адресов</a></nav>--}}
            <h4 class="c-grey-900 mB-20">{{ __('dashboard::complaint.edit_complaint') }}</h4>
            <form id="formComplaintEdit" class="row">
                <div class="col-md-12"><h5>{{ __('dashboard::complaint.contacts') }}</h5></div>
                <div class="col-md-3 form-group"><label for="name">{{ __('dashboard::complaint.postman_name') }}</label> <input type="text"
                                                                                                  name="name"
                                                                                                  class="form-control"
                                                                                                  id="name"
                                                                                                  placeholder="{{ __('dashboard::complaint.placeholder_postman_name') }}"
                                                                                                  value="{{ $complaint->name }}">
                    <div class="invalid-feedback"></div>
                </div>
                <div class="col-md-3 form-group"><label for="telephone">{{ __('dashboard::complaint.telephone') }}</label> <input type="phone"
                                                                                               name="telephone"
                                                                                               class="form-control"
                                                                                               id="telephone"
                                                                                               placeholder="{{ __('dashboard::complaint.placeholder_telephone') }}"
                                                                                                value="{{ $complaint->telephone }}">
                    <div class="invalid-feedback"></div>
                </div>
                <div class="col-md-3 form-group"><label for="company">{{ __('dashboard::complaint.company') }}</label> <input type="text" name="company"
                                                                                            class="form-control"
                                                                                            id="company"
                                                                                            placeholder="{{ __('dashboard::complaint.placeholder_company') }}"
                                                                                            value="{{ $complaint->company }}">
                    <div class="invalid-feedback"></div>
                </div>
                <div class="col-md-3 form-group"><label for="email">{{ __('dashboard::complaint.email') }}</label> <input type="email" name="email"
                                                                                         class="form-control" id="email"
                                                                                         placeholder="{{ __('dashboard::complaint.placeholder_email') }}"
                                                                                        value="{{ $complaint->email }}">
                    <div class="invalid-feedback"></div>
                </div>
                <div class="col-md-3 form-group"><label for="message">{{ __('dashboard::complaint.message') }}</label> <input type="text"
                                                                                               name="message"
                                                                                               class="form-control"
                                                                                               id="message"
                                                                                               placeholder="{{ __('dashboard::complaint.placeholder_message') }}"
                                                                                               value="{{ $complaint->message }}">
                    <div class="invalid-feedback"></div>
                </div>
                <div class="col-md-3 form-group"><label for="type">{{ __('dashboard::complaint.type') }}</label> <input type="text" name="type"
                                                                                             class="form-control"
                                                                                             id="type"
                                                                                             placeholder="{{ __('dashboard::complaint.placeholder_type') }}"
                                                                                                value="{{ $complaint->type }}">
                    <div class="invalid-feedback"></div>
                </div>
                <div class="col-md-6 form-group"><label for="status">{{ __('dashboard::complaint.status') }}</label> <select id="status"
                                                                                            class="form-control"
                                                                                            name="status">
                        <option value="">{{ __('dashboard::complaint.change_status') }}</option>
                        <option value="verification_possible" {{ $complaint->status == 'verification_possible' ? 'selected' : '' }}>{{ __('dashboard::complaint.statuses.verification_possible') }}</option>
                        <option value="verification_impossible" {{ $complaint->status == 'verification_impossible' ? 'selected' : '' }}>{{ __('dashboard::complaint.statuses.verification_impossible') }}</option>
                        <option value="processed" {{ $complaint->status == 'processed' ? 'selected' : '' }}>{{ __('dashboard::complaint.statuses.processed') }}</option>
                        <option value="transferred_to_oy" {{ $complaint->status == 'transferred_to_oy' ? 'selected' : '' }}>{{ __('dashboard::complaint.statuses.transferred_to_oy') }}</option>
                        <option value="in_processing" {{ $complaint->status == 'in_processing' ? 'selected' : '' }}>{{ __('dashboard::complaint.statuses.in_processing') }}</option>
                    </select>`
                    <div class="invalid-feedback"></div>
                </div>
                <div class="col-md-3 form-group"><label for="to_code">{{ __('dashboard::complaint.npy_code') }}</label> <input type="text" name="to_code"
                                                                                            class="form-control"
                                                                                            id="to_code" placeholder="{{ __('dashboard::complaint.placeholder_npy_code') }}"
                                                                                            value="{{ $complaint->to_code }}">
                    <div class="invalid-feedback"></div>
                </div>
                <div class="col-md-3 form-group"><label for="np_code">{{ __('dashboard::complaint.np_code') }}</label> <input type="text" name="np_code"
                                                                                            class="form-control"
                                                                                            id="np_code" placeholder="{{ __('dashboard::complaint.placeholder_np_code') }}"
                                                                                            value="{{ $complaint->np_code }}">
                    <div class="invalid-feedback"></div>
                </div>
                <div class="col-md-3 form-group"><label for="dfs_department_code">{{ __('dashboard::complaint.dfs_department_code') }}</label> <input
                            type="text" name="dfs_department_code" class="form-control" id="dfs_department_code"
                            placeholder="{{ __('dashboard::complaint.placeholder_dfs_department_code') }}"
                            value="{{ $complaint->dfs_department_code }}">
                    <div class="invalid-feedback"></div>
                </div>
                <input
                        name="lat" type="hidden">
                <div class="col-md-12"><h5>{{ __('dashboard::complaint.common_information') }}</h5></div>
                <div class="col-md-3 form-group"><label for="spd_name">{{ __('dashboard::complaint.spd_name') }}</label> <input type="text"
                                                                                                   name="spd_name"
                                                                                                   class="form-control"
                                                                                                   id="spd_name"
                                                                                                   placeholder="{{ __('dashboard::complaint.placeholder_spd_name') }}"
                                                                                                    value="{{ $complaint->spd_name }}">
                    <div class="invalid-feedback"></div>
                </div>
                <div class="col-md-3 form-group"><label for="spd_code">{{ __('dashboard::complaint.spd_code') }}</label> <input type="text"
                                                                                              name="spd_code"
                                                                                              class="form-control"
                                                                                              id="spd_code"
                                                                                              placeholder="{{ __('dashboard::complaint.placeholder_spd_code') }}"
                                                                                                value="{{ $complaint->spd_code }}">
                    <div class="invalid-feedback"></div>
                </div>
                <div class="col-md-3 form-group"><label for="spd_address">{{ __('dashboard::complaint.spd_address') }}</label> <input
                            type="text" name="spd_address" class="form-control" id="spd_address"
                            placeholder="{{ __('dashboard::complaint.placeholder_spd_address') }}"
                            value="{{ $complaint->spd_address }}">
                    <div class="invalid-feedback"></div>
                </div>
                <div class="col-md-3 form-group"><label for="spd_license">{{ __('dashboard::complaint.spd_license') }}</label> <input type="text"
                                                                                                        name="spd_license"
                                                                                                        class="form-control"
                                                                                                        id="spd_license"
                                                                                                        placeholder="{{ __('dashboard::complaint.placeholder_spd_license') }}"
                                                                                                        value="{{ $complaint->spd_license }}">
                    <div class="invalid-feedback"></div>
                </div>
                <div class="col-md-12"><h5>{{ __('dashboard::complaint.action_taken') }}</h5></div>
                <div class="col-md-3 form-group"><label for="verified_at">{{ __('dashboard::complaint.verified_at') }}</label> <input type="text"
                                                                                                       id="verified_at"
                                                                                                       name="verified_at"
                                                                                                       class="form-control"
                                                                                                        value="{{ $complaint->verified_at ? $complaint->verified_at->format('d.m.Y') : '' }}">
                    <div class="invalid-feedback"></div>
                </div>
                <div class="col-md-3 form-group"><label for="protocol">{{ __('dashboard::complaint.protocol') }}</label> <input type="text"
                                                                                               name="protocol"
                                                                                               class="form-control"
                                                                                               id="protocol"
                                                                                               placeholder="{{ __('dashboard::complaint.placeholder_protocol') }}"
                                                                                                value="{{ $complaint->protocol }}">
                    <div class="invalid-feedback"></div>
                </div>
                <div class="col-md-3 form-group"><label for="dfs_executor">{{ __('dashboard::complaint.dfs_executor') }}</label> <input
                            type="text" name="dfs_executor" class="form-control" id="dfs_executor"
                            placeholder="{{ __('dashboard::complaint.placeholder_dfs_executor') }}"
                            value="{{ $complaint->dfs_executor }}">
                    <div class="invalid-feedback"></div>
                </div>
                <div class="col-md-12 form-group"><label>{{ __('dashboard::complaint.explain_violations') }}</label>
                    <ul id="violationsList">
                        @foreach ($violations as $index => $violation)
                            <li class="form-check">
                                <label class="form-check-label" for="Violation-{{ $index }}" title="{{ $violation->description }}">
                                    <input
                                            id="Violation-{{ $index }}"
                                            class="form-check-input"
                                            type="checkbox"
                                            name="violations"
                                            value="{{ $violation->id }}"
                                            {{ $complaint->inViolations($violation->id) ? 'checked' : '' }}
                                    >
                                    {{ $violation->name }}
                                </label>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <!--<div class="col-md-12">
                  <p>id адреса: {{ $complaint->address_id ? $complaint->address_id : 'отсутствует' }}</p>
                </div>-->
                <div class="col-md-12 form-group"><label for="comment">{{ __('dashboard::complaint.comment') }}</label> <textarea
                            class="form-control"
                            placeholder="{{ __('dashboard::complaint.placeholder_comment') }}"
                            name="comment"
                            id="comment">{{ $complaint->comment }}</textarea>
                    <div class="invalid-feedback"></div>
                </div>
                <div class="col-md-12">
                    <button type="button" data-complaint-id="{{ $complaint->id }}" class="btn btn-primary complaintSubmit">
                        {{ __('dashboard::popup.save') }}
                    </button>
                    {{--<button type="button" data-redirect="/complaint-view.html" data-complaint-id="143534"--}}
                            {{--class="btn btn-primary complaintSubmit">Сохранить и закрыть--}}
                    {{--</button>--}}
                </div>
            </form>
        </div>
    </div>
@endsection
