<ul class="sidebar-menu scrollable pos-r">
    <li class="nav-item mT-30 active">
        <a class="sidebar-link" href="{{ route('dashboard.homepage') }}">
            <span class="icon-holder"><i class="c-blue-500 fas fa-home"></i> </span>
            <span class="title">{{ __('dashboard::menu.home') }}</span>
        </a>
    </li>
    <li class="nav-item dropdown">
        <a class="dropdown-toggle" href="{{ route('dashboard.address.index') }}">
            <span class="icon-holder"><i class="c-purple-500 fas fa-map-marked-alt"></i> </span>
            <span class="title">{{ __('dashboard::menu.addresses') }}</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="sidebar-link" href="{{ route('dashboard.address.import.create') }}">
            <span class="icon-holder"><i class="c-indigo-500 fas fa-file-import"></i> </span>
            <span class="title">{{ __('dashboard::menu.addresses_import') }}</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="sidebar-link" href="{{ route('dashboard.analytics.index') }}">
            <span class="icon-holder"><i class="c-indigo-500 far fa-chart-bar"></i> </span>
            <span class="title">{{ __('dashboard::menu.analytics') }}</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="sidebar-link" href="{{ route('dashboard.history.index') }}">
            <span class="icon-holder"><i class="c-indigo-500 fas fa-history"></i> </span>
            <span class="title">{{ __('dashboard::menu.history') }}</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="sidebar-link" href="{{ route('dashboard.report.index') }}">
            <span class="icon-holder"><i class="c-indigo-500 far fa-file-excel"></i> </span>
            <span class="title">{{ __('dashboard::menu.reports') }}</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="sidebar-link" href="{{ route('dashboard.complaint.index') }}">
            <span class="icon-holder"><i class="c-indigo-500 fas fa-exclamation-circle"></i> </span>
            <span class="title">{{ __('dashboard::menu.complaint') }}</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="sidebar-link" href="{{ route('dashboard.address.updates.index') }}">
            <span class="icon-holder"><i class="c-indigo-500 fas fa-sync-alt"></i> </span>
            <span class="title">{{ __('dashboard::menu.db_updates') }}</span>
        </a>
    </li>
</ul>