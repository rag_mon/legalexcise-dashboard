@extends('dashboard::layouts.master')

@section('content')
    <div id='mainContent '>
        <div class="row gap-20">
            <div class="col-md-4">
                <div class="bgc-white p-20 bd">
                    <h3 class="lh-1">
                        {{ __('dashboard::analytics.statistic_complaints') }}
                    </h3>
                    <br>
                    <div class="form-group">
                        <label>{{ __('dashboard::analytics.total_complaints') }}</label>
                        <input
                                id="selector-of-count1"
                                type="text"
                                name="count"
                                class="form-control"
                                placeholder="">
                    </div>
                    <div id="filter-of-count1"></div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="bgc-white p-20 bd">
                    <h3 class="lh-1">
                        {{ __('dashboard::analytics.statistic_action_taken') }}
                    </h3>
                    <br>
                    <div class="form-group">
                        <label>{{ __('dashboard::analytics.total_action_taken') }}</label>
                        <input
                                id="selector-of-count2"
                                type="text"
                                name="count"
                                class="form-control"
                                placeholder=""
                        >
                    </div>
                    <div id="filter-of-count2"></div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="bgc-white p-20 bd">
                    <h3 class="lh-1">
                        {{ __('dashboard::analytics.statistic_suspicious') }}
                    </h3>
                    <br>
                    <div class="form-group">
                        <label>{{ __('dashboard::analytics.total_suspicious') }}</label>
                        <input
                                id="selector-of-count3"
                                type="text"
                                name="count"
                                class="form-control"
                                placeholder=""
                        >
                    </div>
                    <div id="filter-of-count3"></div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="bgc-white p-20 bd">
                    <h3 class="lh-1">
                        {{ __('dashboard::analytics.statistic_financial_sanctions') }}
                    </h3>
                    <br>
                    <div class="form-group">
                        <label>{{ __('dashboard::analytics.total_financial_sanctions') }}</label>
                        <input
                                id="selector-of-count4"
                                type="text"
                                name="count"
                                class="form-control"
                                placeholder=""
                        >
                    </div>
                    <div id="filter-of-count4"></div>
                </div>
            </div>
        </div>

        <div class="row gap-20">
            <!--Line Chart-->
            <div class="col-md-12">
                <!-- #Monthly Stats ==================== -->
                <div class="bgc-white p-20 bd">
                    <div class="layers">
                        <h6 id="title-of-chart-line" class="lh-1">{{ __('dashboard::analytics.statistic_complaints_and_action_taken') }}</h6>
                        <div class="layer w-100 p-20">
                            <canvas id="selector-of-chart-line" height="220"></canvas>
                        </div>
                        {{--<div id="filter-of-chart-line">
                        </div>--}}
                    </div>
                </div>
            </div>
            {{--<!--Bar Chart-->--}}
            {{--<div class="col-md-6">--}}
                {{--<div class="bgc-white p-20 bd">--}}
                    {{--<h6 id="title-of-chart-bar" class="c-grey-900">Bar Chart</h6>--}}
                    {{--<div class="mT-30">--}}
                        {{--<canvas id="selector-of-chart-bar" height="220"></canvas>--}}
                    {{--</div>--}}
                    {{--<div id="filter-of-chart-bar">--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<!--Area Chart-->--}}
            {{--<div class="col-md-6">--}}
                {{--<div class="bgc-white p-20 bd">--}}
                    {{--<h6 id="title-of-chart-area" class="c-grey-900">Area Chart</h6>--}}
                    {{--<div class="mT-30">--}}
                        {{--<canvas id="selector-of-chart-area" height="220"></canvas>--}}
                    {{--</div>--}}
                    {{--<div id="filter-of-chart-area">--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<!--Scatter Chart-->--}}
            {{--<div class="col-md-6">--}}
                {{--<div class="bgc-white p-20 bd">--}}
                    {{--<h6 id="title-of-chart-scatter" class="c-grey-900">Scatter Chart</h6>--}}
                    {{--<div class="mT-30">--}}
                        {{--<canvas id="selector-of-chart-scatter" height="220"></canvas>--}}
                    {{--</div>--}}
                    {{--<div id="filter-of-chart-scatter">--}}
                    {{--</div>--}}

                {{--</div>--}}
            {{--</div>--}}


            {{--<div class=" col-md-6">--}}
                {{--<!--Easy Pie Chart-->--}}
                {{--<div class="bgc-white p-20 bd">--}}
                    {{--<h6 id="title-of-chart-easy-pie" class="c-grey-900">Easy Pie Charts</h6>--}}
                    {{--<div class="mT-30">--}}
                        {{--<div id="selector-of-chart-easy-pie" class="peers mT-20 fxw-nw@lg+  ta-c gap-10">--}}

                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div id="filter-of-chart-easy-pie">--}}
                    {{--</div>--}}
                {{--</div>--}}

                {{--<!--Sparkline Chart-->--}}
                {{--<div class="bgc-white p-20 bd">--}}
                    {{--<h6 id="title-of-chart-sparkline" class="c-grey-900">Sparkline</h6>--}}
                    {{--<div class="mT-30">--}}
                        {{--<div class="peers ai-c jc-sb fxw-nw bdB pY-15">--}}
                            {{--<span class="" id="selector-of-chart-sparkline"--}}
                                  {{--style="width: 100%;display: block">&nbsp;</span>--}}
                        {{--</div>--}}
                        {{--<div id="filter-of-chart-sparkline">--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}


        </div>
    </div>
@endsection

@section('scripts')
    <script>
        window.counter = [
            {
                selector: '#selector-of-count1',
                type: 'counter',
                filter: '#filter-of-count1',
                filterProvider: 'complaints',
                data: {{ $complaintsCount }}
            },
            {
                selector: '#selector-of-count2',
                type: 'counter',
                filter: '#filter-of-count2',
                filterProvider: 'complaint_reactions',
                data: {{ $complaintReactionsCount }}
            },
            {
                selector: '#selector-of-count3',
                type: 'counter',
                filter: '#filter-of-count3',
                filterProvider: 'added_suspicious',
                data: {{ $addedSuspiciousCount }}
            },
            {
                selector: '#selector-of-count4',
                type: 'counter',
                filter: '#filter-of-count4',
                filterProvider: 'financial_sanctions',
                data: {{ $financialSanctionsSum }}
            }
        ];

        window.charts = [
            // {
            //     selector: '#selector-of-chart-bar',
            //     filter: '#filter-of-chart-bar',
            //     filterProvider: 'adding_addresses',
            //     type: 'bar', // тип графика
            //     titleSelector: '#title-of-chart-bar',
            //     title: 'Статистика жалоб/обработок',
            //     labels: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            //     data: [
            //         {
            //             type: 'complaints',
            //             label: 'Жалобы',
            //             data: [60, 50, 70, 60, 50, 70, 60]
            //         }, {
            //             type: 'treatments',
            //             label: 'Обработка',
            //             data: [70, 20.5, 75, 85, 70, 75, 85, 70]
            //         }
            //     ]
            // }
            // ,
            {
                selector: '#selector-of-chart-line',
                filter: '#filter-of-chart-line',
                filterProvider: 'complaint_reactions',
                type: 'line', // тип графика
                titleSelector: '#title-of-chart-line',
                title: '{{ __('dashboard::analytics.statistic_complaints_and_action_taken') }}',
                labels: {!! json_encode(array_values(__('dashboard::datetime.months')), JSON_UNESCAPED_UNICODE) !!},
                data: [
                    {
                        type: 'complaints',
                        label: '{{ __('dashboard::analytics.complaints') }}',
                        data: [{{ $chartComplaints }}]//[60, 50, 70, 60, 50, 70, 60]
                    }, {
                        type: 'treatments',
                        label: '{{ __('dashboard::analytics.processed') }}',
                        data: [{{ $chartTreatments }}]//[70, 20.5, 75, 85, 70, 75, 85, 70]
                    }
                ]
            }
            // , {
            //     selector: '#selector-of-chart-area',
            //     filter: '#filter-of-chart-area',
            //     type: 'area', // тип графика
            //     titleSelector: '#title-of-chart-area',
            //     title: 'Статистика жалоб/обработок',
            //     labels: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            //     data: [
            //         {
            //             type: 'complaints',
            //             label: 'Жалобы',
            //             data: [60, 50, 70, 60, 50, 70, 60]
            //         }, {
            //             type: 'treatments',
            //             label: 'Обработка',
            //             data: [70, 20.5, 75, 85, 70, 75, 85, 70]
            //         }
            //     ]
            // },{
            //     selector: '#selector-of-chart-scatter',
            //     filter: '#filter-of-chart-scatter',
            //     type: 'scatter', // тип графика
            //     titleSelector: '#title-of-chart-scatter',
            //     title: 'Статистика жалоб/обработок',
            //     labels: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            //     data: [
            //         {
            //             type: 'complaints',
            //             label: 'Жалобы',
            //             data: [
            //                 { x: 10, y: 20 },
            //                 { x: 30, y: 40 },
            //                 { x: 50, y: 60 },
            //                 { x: 70, y: 80 },
            //                 { x: 90, y: 100 },
            //                 { x: 110, y: 120 },
            //                 { x: 130, y: 140 }
            //             ]
            //         }, {
            //             type: 'treatments',
            //             label: 'Обработка',
            //             data: [
            //                 { x: 150, y: 160 },
            //                 { x: 170, y: 180 },
            //                 { x: 190, y: 200 },
            //                 { x: 210, y: 220 },
            //                 { x: 230, y: 240 },
            //                 { x: 250, y: 260 },
            //                 { x: 270, y: 280 }
            //             ]
            //         }
            //     ]
            // }, {
            //     selector: '#selector-of-chart-easy-pie',
            //     filter: '#filter-of-chart-easy-pie',
            //     filterProvider: 'percent_reactions',
            //     type: 'easy_pie', // тип графика
            //     titleSelector: '#title-of-chart-easy-pie',
            //     title: 'Статистика жалоб/обработок',
            //     data: [
            //         {
            //             type: 'complaints',
            //             label: 'Жалобы',
            //             data: 60
            //         }, {
            //             type: 'treatments',
            //             label: 'Обработка',
            //             data: 40
            //         }
            //     ]
            // }, {
            //     selector: '#selector-of-chart-sparkline',
            //     filter: '#filter-of-chart-sparkline',
            //     filterProvider: 'suspicious_to_active_addresses',
            //     type: 'sparkline', // тип графика
            //     titleSelector: '#title-of-chart-sparkline',
            //     title: 'Статистика жалоб/обработок',
            //     data: [5, 4, 5, -2, 0, 3, -5, 6, 7, 9, 9, 5, -3, -2, 2, -4]
            // }
        ];
    </script>
@endsection