@extends('dashboard::layouts.master')

@section('content')
    <div class="full-container">
        <div class="bgc-white bd bdrs-3 p-20 mB-20">
            {{--<nav class="breadcrumb"><a class="breadcrumb-item" href="#">Главная</a> <a class="breadcrumb-item active"--}}
            {{--href="#">Список адресов</a></nav>--}}
            <h4 class="c-grey-900 mB-20">{{ __('dashboard::bookkeeping.page_title') }}</h4>
            <form id="formAccountingEdit">
                <div class="form-group"><label>{{ __('dashboard::bookkeeping.examination_of_excisable_goods') }}</label>
                    <input type="number" name="examination_of_excisable_goods"
                           value="{{ isset($bookkeeping) ? $bookkeeping->examination_of_excisable_goods : '0' }}"
                           class="form-control">
                    <div class="invalid-feedback"></div>
                </div>
                <div class="form-group"><label>{{ __('dashboard::bookkeeping.transferred_to_the_oy') }}</label> <input
                            type="number" name="transferred_to_the_oy"
                            value="{{ isset($bookkeeping) ? $bookkeeping->transferred_to_the_oy : '0' }}"
                            class="form-control">
                    <div class="invalid-feedback"></div>
                </div>
                <div class="form-group"><label>{{ __('dashboard::bookkeeping.sale_to_minors') }}</label> <input
                            type="number"
                            value="{{ isset($bookkeeping) ? $bookkeeping->sale_to_minors : '0' }}"
                            name="sale_to_minors"
                            class="form-control">
                    <div class="invalid-feedback"></div>
                </div>
                <div class="form-group alert alert-danger invisible" id="formError"></div>
                <div class="form-group">
                    <button type="reset" class="btn btn-primary">{{ __('dashboard::bookkeeping.reset_form') }}</button>
                    <button type="submit" class="btn btn-primary">{{ __('dashboard::popup.save') }}</button>
                </div>
            </form>
        </div>
    </div>
@endsection