@extends('dashboard::layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="row">
            {{--<div class="col-md-12">--}}
                {{--<nav class="breadcrumb"><a class="breadcrumb-item" href="#">Главная</a> <a class="breadcrumb-item active"--}}
                                                                                           {{--href="#">История</a></nav>--}}
            {{--</div>--}}
            <div class="col-md-12">
                <div class="bgc-white bd bdrs-3 p-20 mB-20"><h4 class="c-grey-900 mB-20">{{ __('dashboard::history.events') }}</h4>
                    <table id="addressHistoryListTable" class="table table-striped table-bordered" cellspacing="0"
                           width="100%">
                        <thead>
                        <tr>
                            <th>{{ __('dashboard::history.user') }}</th>
                            <th>{{ __('dashboard::history.user_group') }}</th>
                            <th>{{ __('dashboard::history.action') }}</th>
                            <th>{{ __('dashboard::history.date') }}</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th><input type="search" class="form-control" placeholder="{{ __('dashboard::history.user') }}" id="user-filter"></th>
                            <th>
                                <select id="inputStatus" name="status" class="form-control">
                                    <option>&nbsp;</option>
                                    <option value="{{ __('dashboard::history.user_groups.sfs') }}">{{ __('dashboard::history.user_groups.sfs') }}</option>
                                    <option value="{{ __('dashboard::history.user_groups.police') }}">{{ __('dashboard::history.user_groups.police') }}</option>
                                </select>
                            </th>
                            <th>{{ __('dashboard::history.action') }}</th>
                            <th>{{ __('dashboard::history.date') }}</th>
                        </tr>
                        </tfoot>
                        <tbody>
                            <!-- Server-side processing... -->
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection