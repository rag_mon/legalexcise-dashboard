<?php

namespace Modules\Dashboard\Charts\Factory;

/**
 * Interface IFactory
 *
 * @package Modules\Dashboard\Charts\Factory
 */
interface IFactory
{
    /**
     * @return mixed
     */
    public function make();
}