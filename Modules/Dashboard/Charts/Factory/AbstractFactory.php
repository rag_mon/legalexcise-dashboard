<?php

namespace Modules\Dashboard\Charts\Factory;

use Carbon\Carbon;

/**
 * Class AbstractFactory
 *
 * @package Modules\Dashboard\Charts\Factory
 */
abstract class AbstractFactory implements IFactory
{
    /**
     * @var Carbon
     */
    protected $startAt;

    /**
     * @var Carbon
     */
    protected $endAt;

    /**
     * @var int
     */
    protected $parts;

    /**
     * AbstractFactory constructor.
     *
     * @param null|Carbon $startAt
     * @param null|Carbon $endAt
     * @param int $parts
     */
    public function __construct($startAt = null, $endAt = null, $parts = 1)
    {
        $this->startAt = $startAt;
        $this->endAt = $endAt;
        $this->parts = $parts;
    }

    /**
     * @return mixed
     */
    public abstract function make();

    public abstract function getType();

    public abstract function getLabel();
}