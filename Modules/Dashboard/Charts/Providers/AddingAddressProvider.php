<?php

namespace Modules\Dashboard\Charts\Providers;

use Modules\Dashboard\Charts\Chart;
use Modules\Dashboard\Charts\Factory\Address\AddingActiveAddressFactory;
use Modules\Dashboard\Charts\Factory\Address\AddingSuspiciousAddressFactory;

/**
 * Class AddingAddressProvider
 *
 * @package Modules\Dashboard\Charts\Providers
 */
class AddingAddressProvider extends AbstractProvider
{
    /**
     * Register data provider factories.
     *
     * @return void
     */
    public function register()
    {
        $this->registerFactory(AddingActiveAddressFactory::class);
        $this->registerFactory(AddingSuspiciousAddressFactory::class);
    }

    /**
     * Get provider key.
     *
     * @return string
     */
    public function getKey()
    {
        return 'adding_addresses';
    }

    public function getSelector()
    {
        return '';
    }

    public function getType()
    {
        return Chart::TYPE_BAR;
    }

    public function getTitle()
    {
        return __('');
    }
}