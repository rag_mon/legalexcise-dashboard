<?php

namespace Modules\Dashboard\Charts\Providers;

use Illuminate\Support\Collection;
use Modules\Dashboard\Charts\Factory;
use Modules\Dashboard\Charts\IFactory;

/**
 * Class AbstractProvider
 *
 * @package Modules\Dashboard\Charts\Providers
 */
abstract class AbstractProvider implements IProvider
{
    /**
     * @var Collection
     */
    protected $factories;

    /**
     * AbstractProvider constructor.
     */
    public function __construct()
    {
        $this->factories = new Collection();
    }

    /**
     * Register data provider factories.
     *
     * @return void
     */
    public abstract function register();

    /**
     * Get provider key.
     *
     * @return string
     */
    public abstract function getKey();

    /**
     * Get provider data.
     *
     * @return array
     */
    public function getData()
    {
        $data = [];

        foreach ($this->factories->sortBy('order') as $factoryData) {
            $data[] = $this->createFactory($factoryData['factory'])->make();
        }

        return $data;
    }

    /**
     * Create factory.
     *
     * @param array $data
     * @return IFactory
     */
    private function createFactory($data)
    {
        return new $data['factory']();
    }

    /**
     * Register factory.
     *
     * @param string|IFactory $factory
     * @param array $meta
     * @param int $order
     * @return AbstractProvider
     */
    protected function registerFactory($factory, $meta = [], $order = 0)
    {
        $this->factories->push([
            'factory' => $factory,
            'meta' => $meta,
            'order' => $order,
        ]);

        return $this;
    }

    /**
     * Get provider factory collection.
     *
     * @return Collection
     */
    public function getFactories()
    {
        return $this->factories;
    }

    /**
     * @return string
     */
    public abstract function getSelector();

    /**
     * @return string
     */
    public abstract function getType();

    /**
     * @return string
     */
    public abstract function getTitle();
}