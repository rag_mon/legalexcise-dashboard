<?php

namespace Modules\Dashboard\Charts\Providers;

/**
 * Interface IProvider
 *
 * @package Modules\Dashboard\Charts\Providers
 */
interface IProvider
{
    /**
     * Register data provider factories.
     *
     * @return void
     */
    public function register();

    /**
     * Get provider key.
     *
     * @return string
     */
    public function getKey();

    /**
     * Get provider data.
     *
     * @return array
     */
    public function getData();
}