<?php

namespace Modules\Dashboard\Charts\Builder;

use Modules\Dashboard\Charts\Providers\IProvider;

/**
 * Class AbstractBuilder
 *
 * @package Modules\Dashboard\Charts\Builder
 */
abstract class AbstractBuilder implements IBuilder
{
    /**
     * @var IProvider
     */
    protected $provider;

    /**
     * Build chart data by provider.
     *
     * @return array
     */
    public abstract function build();

    /**
     * Set chart data provider.
     *
     * @param IProvider|string $provider
     * @return static
     */
    public function setProvider($provider)
    {
        if (is_string($provider)) {
            $this->provider = new $provider();
        } else {
            $this->provider = $provider;
        }

        return $this;
    }

    /**
     * Get chart data provider.
     *
     * @return IProvider
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * @return mixed
     */
    protected abstract function getLabels();

    /**
     * @return mixed
     */
    protected abstract function getData();
}