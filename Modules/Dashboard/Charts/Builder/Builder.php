<?php

namespace Modules\Dashboard\Charts\Builder;

/**
 * Class Builder
 *
 * @package Modules\Dashboard\Charts\Builder
 */
class Builder extends AbstractBuilder
{
    /**
     * @var string
     */
    protected $selector;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $title;

    /**
     * Builder constructor.
     */
    public function __construct()
    {
        //
    }

    /**
     * Create builder new instance.
     *
     * @return static
     */
    public static function newInstance()
    {
        return new static();
    }

    /**
     * Build chart data by provider.
     *
     * @return array
     */
    public function build()
    {
        return [
            'selector' => $this->getSelector(),
            'type' => $this->getType(),
            'title' => $this->getTitle(),
            'provider' => $this->getProvider()->getKey(),
            'labels' => $this->getLabels(),
            'data' => $this->getData(),
        ];
    }

    protected function getLabels()
    {
        return ;
    }

    protected function getData()
    {
        return $this->getProvider()->getData();
    }

    /**
     * Get chart selector.
     *
     * @return string
     */
    public function getSelector()
    {
        return $this->selector;
    }

    /**
     * Set chart selector.
     *
     * @param string $selector
     * @return static
     */
    public function setSelector($selector)
    {
        $this->selector = $selector;

        return $this;
    }

    /**
     * Get chart type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set chart type.
     *
     * @param string $type
     * @return static
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get chart title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set chart title.
     *
     * @param string $title
     * @return static
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }
}