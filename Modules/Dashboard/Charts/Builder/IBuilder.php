<?php

namespace Modules\Dashboard\Charts\Builder;

/**
 * Interface IBuilder
 *
 * @package Modules\Dashboard\Charts\Builder
 */
interface IBuilder
{
    public function setProvider($provider);
    public function getProvider();
}