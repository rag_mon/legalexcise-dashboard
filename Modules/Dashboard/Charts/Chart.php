<?php

namespace Modules\Dashboard\Charts;

/**
 * Class Chart
 *
 * @package Modules\Dashboard\Charts
 */
class Chart
{
    const TYPE_AREA = 'area';
    const TYPE_BAR = 'bar';
    const TYPE_EASY_PIE = 'easy_pie';
    const TYPE_LINE = 'line';
    const TYPE_SCATTER = 'scatter';
    const TYPE_SPARK_LINE = 'sparkline';

    /**
     * Support chart types list.
     *
     * @var array
     */
    public $types = [
        self::TYPE_AREA,
        self::TYPE_BAR,
        self::TYPE_EASY_PIE,
        self::TYPE_LINE,
        self::TYPE_SCATTER,
        self::TYPE_SPARK_LINE,
    ];
}