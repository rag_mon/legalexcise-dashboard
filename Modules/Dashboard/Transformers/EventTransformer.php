<?php

namespace Modules\Dashboard\Transformers;

use League\Fractal\TransformerAbstract;
use Modules\Dashboard\Models\Event;

/**
 * Class EventTransformer
 *
 * @package Modules\Dashboard\Transformers
 */
class EventTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'user',
    ];

    /**
     * Include resources without needing it to be requested.
     *
     * @var array
     */
    protected $defaultIncludes = [
        'user',
    ];

    /**
     * @param Event $event
     * @return array
     */
    public function transform(Event $event)
    {
        return [
            'id' => $event->id,
            'user_id' => $event->user_id,
            'type' => $event->type,
            'lang_key' => $event->lang_key,
            'message' => __($event->lang_key, $event->data),
            'created_at' => $event->created_at->format('d.m.Y h:i:s'),
        ];
    }

    /**
     * Include User
     *
     * @param Event $event
     * @return \League\Fractal\Resource\Item
     */
    public function includeUser(Event $event)
    {
        $user = $event->user;

        return isset($user) ? $this->item($user, new UserTransformer()) : null;
    }
}