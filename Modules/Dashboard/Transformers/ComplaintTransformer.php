<?php

namespace Modules\Dashboard\Transformers;

use League\Fractal\TransformerAbstract;
use Modules\Dashboard\Models\ActionTaken;
use Modules\Dashboard\Models\Complaint;

/**
 * Class ComplaintTransformer
 *
 * @package Modules\Dashboard\Transformers
 */
class ComplaintTransformer extends TransformerAbstract
{
    /**
     * @param Complaint $complaint
     * @return array
     */
    public function transform(Complaint $complaint)
    {
        $address = $complaint->addressEntry ? $complaint->addressEntry->address : $complaint->address;
        $confiscated_goods = $complaint->actionTaken()->sum('confiscated_goods');
        if ($protocol_drawn_up = $complaint->actionTaken()->orderByDesc('protocol_drawn_up')->first()) {
            $protocol_drawn_up = $protocol_drawn_up->protocol_drawn_up
                ? $protocol_drawn_up->protocol_drawn_up->format(ActionTaken::PROTOCOL_DRAWN_UP_DATE_VIEW_FORMAT)
                : '-';
        }
        $financial_sanctions = $complaint->actionTaken()->sum('financial_sanctions');
        $danger = (
            $complaint->created_at->diffInDays(date('Y-m-d H:i:s'), false) >= 30 &&
            $complaint->status != 'verification_impossible' &&
            $complaint->status != 'transferred_to_oy' &&
            $complaint->status != 'processed' ? 1 : 0);

        return [
            'id' => $complaint->id,
            'name' => $complaint->name,
            'telephone' => $complaint->telephone,
            'company' => $complaint->company,
            'email' => $complaint->email,
            'message' => $complaint->message,
            'type' => $complaint->type,
            'is_anonymously' => $complaint->is_anonymously,
            'address_id' => $complaint->address_id,
            'address' => $address,
            'lng' => $complaint->lng,
            'lat' => $complaint->lat,
            'status' => $complaint->status,
            'np_code' => $complaint->np_code ?: '-',
            'to_code' => $complaint->to_code ?: '-',
            'dfs_department_code' => $complaint->dfs_department_code,
            'dfs_executor' => $complaint->dfs_executor,
            'confiscated_goods' => $confiscated_goods,
            'protocol_drawn_up' => $protocol_drawn_up ?: '-',
            'financial_sanctions' => $financial_sanctions,
            'created_at' => $complaint->created_at->format('d.m.Y h:i:s'),
            'danger' => $danger,
        ];
    }
}
