<?php

namespace Modules\Dashboard\Transformers;

use League\Fractal\TransformerAbstract;
use Modules\Dashboard\Models\Report;

/**
 * Class ReportTransformer
 *
 * @package Modules\Dashboard\Transformers
 */
class ReportTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'user',
    ];

    /**
     * Include resources without needing it to be requested.
     *
     * @var array
     */
    protected $defaultIncludes = [
        'user',
    ];

    /**
     * @param Report $report
     * @return array
     */
    public function transform(Report $report)
    {
        return [
            'id' => $report->id,
            'user_id' => $report->user_id,
            'name' => $report->name,
            'status' => $report->status,                    //__("dashboard::report.status.{$report->status}"),
            'created_at' => $report->created_at->format('d.m.Y h:i:s'),
        ];
    }

    /**
     * Include User
     *
     * @param Report $report
     * @return \League\Fractal\Resource\Item
     */
    public function includeUser(Report $report)
    {
        $user = $report->user;

        return isset($user) ? $this->item($user, new UserTransformer()) : null;
    }
}