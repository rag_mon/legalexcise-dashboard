<?php

namespace Modules\Dashboard\Transformers;

use League\Fractal\TransformerAbstract;
use Modules\Dashboard\Models\AddressUpdated;

/**
 * Class AddressUpdatedTransformer
 *
 * @package Modules\Dashboard\Transformers
 */
class AddressUpdatedTransformer extends TransformerAbstract
{
    /**
     * @param AddressUpdated $addressUpdated
     * @return array
     */
    public function transform(AddressUpdated $addressUpdated)
    {
        return [
            'id' => $addressUpdated->id,
            'timestamp' => $addressUpdated->timestamp->format('d.m.Y'),
        ];
    }
}