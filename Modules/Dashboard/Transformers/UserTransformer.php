<?php

namespace Modules\Dashboard\Transformers;

use App\Transformers\RoleTransformer;
use League\Fractal\TransformerAbstract;
use Modules\Dashboard\Models\User;

/**
 * Class UserTransformer
 *
 * @package Modules\Dashboard\Transformers
 */
class UserTransformer extends TransformerAbstract
{
    /**
     * Resources that can be included if requested.
     *
     * @var array
     */
    protected $availableIncludes = [
        'roles',
    ];

    /**
     * Include resources without needing it to be requested.
     *
     * @var array
     */
    protected $defaultIncludes = [
        'roles',
    ];

    /**
     * @param User $user
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'is_notify' => $user->is_notify,
            'created_at' => $user->created_at,
            'updated_at' => $user->updated_at,
        ];
    }

    /**
     * Include Roles
     *
     * @param User $user
     * @return \League\Fractal\Resource\Collection
     */
    public function includeRoles(User $user)
    {
        $roles = $user->roles;

        return isset($roles) ? $this->collection($roles, new RoleTransformer()) : null;
    }
}