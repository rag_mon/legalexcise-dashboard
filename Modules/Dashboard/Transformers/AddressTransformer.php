<?php

namespace Modules\Dashboard\Transformers;

use League\Fractal\TransformerAbstract;
use Modules\Dashboard\Models\Address;

/**
 * Class AddressTransformer
 *
 * @package Modules\Dashboard\Transformers
 */
class AddressTransformer extends TransformerAbstract
{
    /**
     * @param Address $address
     * @return array
     */
    public function transform(Address $address)
    {
        return [
            'id' => $address->id,
            'id_code' => $address->id_code,
            'address' => $address->address,
            'lng' => $address->lng,
            'lat' => $address->lat,
            'license' => $address->license,
            'company' => $address->company,
            'company_type' => $address->company_type,
            'license_start_at' => $address->license_start_at ?: '-',
            'license_end_at' => $address->license_end_at ?: '-',
            'license_type' => $address->license_type,
            'status' => __("dashboard::address.status.{$address->status}"),
            'public_notices' => $address->publicNotices,
            'comment' => $address->comment,
            'created_at' => $address->created_at,
            'updated_at' => $address->updated_at,
        ];
    }
}
