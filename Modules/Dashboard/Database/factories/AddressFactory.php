<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Modules\Dashboard\Models\AddressImport::class, function (Faker $faker) {
    return [
        'type' => $faker->randomElement(\Modules\Dashboard\Models\AddressImport::$types),
        'uploaded_filename' => str_random() . '.xlsx',
        // TODO: you must using real *.xlsx file with correct and valid spreadsheet structure
        'storage_filename' => null,
        'meta_data' => json_encode([]),
        'configs' => json_encode([]),
        'parsed_at' => $faker->boolean ? $faker->dateTimeBetween('-2 years', '-1 years') : null,
        'geocoded_at' => $faker->boolean ? $faker->dateTimeBetween('-1 years') : null,
    ];
});

$factory->define(Modules\Dashboard\Models\Address::class, function (Faker $faker) {
    return [
        'region_id' => 0,
        'id_code' => $faker->randomNumber(6),
        'address' => $faker->address,
        'lng' => $faker->longitude,
        'lat' => $faker->latitude,
        'license' => $faker->randomNumber(6),
        'company' => $faker->company,
        'company_type' => $faker->companySuffix,
        'license_start_at' => $faker->dateTimeBetween('-2 years'),
        'license_end_at' => $faker->dateTimeBetween('now', '+1 years'),
        'license_type' => $faker->randomElement(\Modules\Dashboard\Models\Address::$types),
        'status' => $faker->randomElement(\Modules\Dashboard\Models\Address::$statuses),
        'public_notices' => <<<EOF
{
    "confiscated_goods":"{$faker->randomNumber(3)}",
    "protocol_drawn_up":"{$faker->date('d.m.Y')}",
    "financial_sanctions":"{$faker->randomNumber(6)}грн"
}
EOF
        ,
        'comment' => $faker->boolean ? $faker->text : null,
        'geocoding_payload' => null,
        'geocoding_at' => $faker->boolean ? $faker->dateTime() : null,
        'created_at' => $faker->dateTime(),
    ];
});
