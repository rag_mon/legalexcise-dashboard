<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressImportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('address_imports', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type', ['licenses', 'suspicious'])->comment('Enum type of addresses import.');
            $table->string('uploaded_filename')->comment('Original uploaded filename.');
            $table->string('storage_filename')->comment('Storage file path.');
            $table->text('meta_data')->comment('Json format meta data field describe uploaded file.');
            $table->text('configs')->comment('Json format field describe import configuration data.');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('parsed_at')->nullable()->default(NULL)->comment('Parsed file timestamp.');
            $table->timestamp('geocoded_at')->nullable()->default(NULL)->comment('Geocoded addresses timestamp.');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('address_imports');
    }
}
