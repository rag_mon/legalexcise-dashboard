<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActionTakenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('action_taken', function (Blueprint $table) {
            $table->increments('id');
            $table->morphs('resource');
            $table->unsignedInteger('confiscated_goods')->nullable();
            $table->timestamp('protocol_drawn_up')->nullable();
            $table->unsignedInteger('financial_sanctions')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('action_taken');
    }
}
