<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateComplaintsAddCommonAndActionTakenInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('complaints', function (Blueprint $table) {
            // SPD section
            $table->string('spd_name')->nullable()->after('dfs_department_code');
            $table->string('spd_code')->nullable()->after('dfs_department_code');
            $table->string('spd_address')->nullable()->after('dfs_department_code');
            $table->string('spd_license')->nullable()->after('dfs_department_code');
            // Action taken section
            $table->timestamp('verified_at')->nullable()->after('dfs_department_code');
            $table->string('violations')->nullable()->after('dfs_department_code');
            $table->string('protocol')->nullable()->after('dfs_department_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('complaints', function (Blueprint $table) {
            $table->dropColumn([
                'spd_name', 'spd_code', 'spd_address', 'spd_license',
                'verified_at', 'violations', 'protocol'
            ]);
        });
    }
}
