<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class UpdateComplaintsAddStatusesProcessingTransferredToOy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `complaints` CHANGE `status` `status` ENUM('processed', 'not_processed', 'transferred_to_oy', 'verification_possible', 'verification_impossible', 'in_processing') NOT NULL DEFAULT 'not_processed'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE `complaints` CHANGE `status` `status` ENUM('processed', 'not_processed') NOT NULL DEFAULT 'not_processed'");
    }
}
