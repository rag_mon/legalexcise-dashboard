<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookkeepingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookkeeping', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('examination_of_excisable_goods')->default(0);
            $table->unsignedInteger('transferred_to_the_oy')->default(0);
            $table->unsignedInteger('sale_to_minors')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookkeeping');
    }
}
