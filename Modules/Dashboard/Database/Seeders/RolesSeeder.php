<?php

namespace Modules\Dashboard\Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::unguard();

        // Super admin
        Role::create([
            'name' => 'root',
            'display_name' => 'Технический адмистратор',
            'description' => 'Технический адмистратор',
        ]);
        // Common Administrator
        Role::create([
            'name' => 'admin',
            'display_name' => 'Администратор',
            'description' => 'Администратор системы',
        ]);

        // Police
        Role::create([
            'name' => 'police',
            'display_name' => 'Нац. полиция',
            'description' => 'Национальная полиция',
        ]);
        // DFS
        Role::create([
            'name' => 'dfs',
            'display_name' => 'ДФС',
            'description' => 'Державная фискальная служба',
        ]);
    }
}
