<?php

namespace Modules\Dashboard\Widgets;

use Modules\Dashboard\Models\Address;

/**
 * Class TotalActiveAddresses
 *
 * @package Modules\Dashboard\Widgets
 */
class TotalActiveAddresses extends AbstractTotalBaseWidget
{
    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     *
     * @param string $title
     * @param string $value
     * @param string $color
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function render($title = null, $value = null, $color = null)
    {
        return parent::render(
            __('dashboard::address.widget.title_total_active'),
            isset($this->config['value']) ? $this->config['value'] : $this->getValue(),
            config('dashboard::address.widget.active_value_color', 'green')
        );
    }

    /**
     * Get total active addresses value.
     *
     * @return int
     */
    private function getValue()
    {
        return Address::active()->count();
    }
}