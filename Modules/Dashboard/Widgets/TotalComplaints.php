<?php

namespace Modules\Dashboard\Widgets;

use Modules\Dashboard\Models\Complaint;

/**
 * Class TotalComplaints
 *
 * @package Modules\Dashboard\Widgets
 */
class TotalComplaints extends AbstractTotalBaseWidget
{
    /**
     * Return view() or other content to display.
     *
     * @param string $title
     * @param string $value
     * @param string $color
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function render($title = null, $value = null, $color = null)
    {
        return parent::render(
            __('dashboard::complaint.widget.title_total'),
            isset($this->config['value']) ? $this->config['value'] : $this->getValue(),
            config('dashboard::complaint.widget.total_value_color', 'purple')
        );
    }

    /**
     * Get total complaints count value.
     *
     * @return int
     */
    private function getValue()
    {
        return Complaint::count();
    }
}