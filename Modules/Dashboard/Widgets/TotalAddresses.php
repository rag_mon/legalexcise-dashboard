<?php

namespace Modules\Dashboard\Widgets;

use Modules\Dashboard\Models\Address;

/**
 * Class TotalAddresses
 *
 * @package Modules\Dashboard\Widgets
 */
class TotalAddresses extends AbstractTotalBaseWidget
{
    /**
     * Return view() or other content to display.
     *
     * @param string $title
     * @param string $value
     * @param string $color
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function render($title = null, $value = null, $color = null)
    {
        return parent::render(
            __('dashboard::address.widget.title_total'),
            isset($this->config['value']) ? $this->config['value'] : $this->getValue(),
            config('dashboard::address.widget.total_value_color', 'purple')
        );
    }

    /**
     * Get total addresses count value.
     *
     * @return int
     */
    private function getValue()
    {
        return Address::count();
    }
}