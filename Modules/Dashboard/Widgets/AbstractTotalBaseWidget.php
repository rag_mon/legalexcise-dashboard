<?php

namespace Modules\Dashboard\Widgets;

/**
 * Class AbstractTotalWidget
 *
 * @package Modules\Dashboard\Widgets
 */
abstract class AbstractTotalBaseWidget extends BaseWidget
{
    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     *
     * NOTICE: This method using in child classes as API for render total_layout.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function run()
    {
        return static::render();
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     *
     * NOTICE: This method using in child classes as API for render total_layout.
     *
     * @param string $title
     * @param string $value
     * @param string $color
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function render($title = null, $value = null, $color = null)
    {
        return view('dashboard::widgets.layout_meter', compact('title', 'value', 'color'));
    }
}