<?php

namespace Modules\Dashboard\Widgets;

use Modules\Dashboard\Charts\Chart;
use Modules\Dashboard\Charts\Providers\AddingAddressProvider;
use Modules\Dashboard\Charts\Builder\Builder as ChartBuilder;

/**
 * Class ChartAddingAddressWidget
 *
 * @package Modules\Dashboard\Widgets
 */
class ChartAddingAddressWidget extends ChartBaseWidget
{
    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     *
     * @param \Modules\Dashboard\Charts\Builder\Builder $builder
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function run(ChartBuilder $builder)
    {
        $chart = $builder
            ->setProvider(AddingAddressProvider::class)
            ->setType(Chart::TYPE_BAR)
            ->setSelector('')
            ->setTitle(__('dashboard::'))
            ->build();

        return view('dashboard::', compact('chart'));
    }
}