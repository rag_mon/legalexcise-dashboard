<?php

namespace Modules\Dashboard\Widgets;

use Arrilot\Widgets\AbstractWidget;

/**
 * Class Widget
 *
 * @package Modules\Dashboard\Widgets
 */
abstract class BaseWidget extends AbstractWidget
{
    //
}