<?php

namespace Modules\Dashboard\Widgets;

use Modules\Dashboard\Models\Address;

/**
 * Class TotalSuspiciousAddresses
 *
 * @package Modules\Dashboard\Widgets
 */
class TotalSuspiciousAddresses extends AbstractTotalBaseWidget
{
    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     *
     * @param string $title
     * @param string $value
     * @param string $color
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function render($title = null, $value = null, $color = null)
    {
        return parent::render(
            __('dashboard::address.widget.title_total_suspicious'),
            isset($this->config['value']) ? $this->config['value'] : $this->getValue(),
            config('dashboard::address.widget.total_suspicious_value_color', 'red')
        );
    }

    /**
     * Get total complaints count value.
     *
     * @return int
     */
    private function getValue()
    {
        return Address::suspicious()->count();
    }
}