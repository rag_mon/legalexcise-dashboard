<?php

namespace Modules\Dashboard\Widgets;


/**
 * Class AddressMap
 *
 * @package Modules\Dashboard\Widgets
 */
class AddressMap extends BaseWidget
{
    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        return view('dashboard::widgets.address_map');
    }
}