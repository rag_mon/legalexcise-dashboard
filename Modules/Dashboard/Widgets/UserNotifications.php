<?php

namespace Modules\Dashboard\Widgets;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

/**
 * Class UserNotifications
 *
 * @package Modules\Dashboard\Widgets
 */
class UserNotifications extends BaseWidget
{
    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $notifications = $this->getNotifications();

        return view('dashboard::widgets.user_notifications', compact('notifications'));
    }

    /**
     * Get user notification collection.
     *
     * @return Collection
     */
    private function getNotifications()
    {
        return Auth::guard('dashboard')
            ->user()
            ->unreadNotifications;
    }
}