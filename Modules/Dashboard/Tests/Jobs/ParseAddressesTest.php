<?php

namespace Modules\Dashboard\Tests\Jobs;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Event;
use Modules\Dashboard\Events\Address\AddressesParsed;
use Modules\Dashboard\Jobs\ParseAddresses;
use Modules\Dashboard\Models\Address;
use Modules\Dashboard\Models\AddressImport;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use Tests\TestCase;

class ParseAddressesTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp()
    {
        parent::setUp();

        Address::truncate();
    }

    /**
     * Test the job.
     *
     * @return void
     */
    public function testRun()
    {
        $addressesDontTouch = [
            [
                'region_id' => 0,
                'id_code' => '2550305806',
                'address' => 'Ізмаільський р-н, с. Першотравневе, вул. Шкільна, 100, магазин-кафетерій "Наталі"',
                'lng' => null,
                'lat' => null,
                'license' => '151516640311',
                'company' => 'Чумаченко Наталія Дмитрівна',
                'company_type' => 'магазин-кафетерій "Наталі"',
                'license_start_at' => '2018-02-10',
                'license_end_at' => '2017-01-30',
                'license_type' => Address::TYPE_ALCOHOL,
                'status' => Address::STATUS_ACTIVE,
                'comment' => null,
            ],
        ];
        $addressesWillBeRemoved = [
            [
                'region_id' => 0,
                'id_code' => '43534764544',
                'address' => 'Ізмаільський р-н, с. Утконосівка, вул. Шевченка, 70, магазин',
                'lng' => null,
                'lat' => null,
                'license' => '46345345654',
                'company' => 'Чепой Григорій Григорович',
                'company_type' => 'магазин',
                'license_start_at' => '2018-02-10',
                'license_end_at' => '2018-02-04',
                'license_type' => Address::TYPE_TOBACCO,
                'status' => Address::STATUS_ACTIVE,
                'comment' => null,
            ]
        ];
        // NOTICE: Will be added from *.xlsx file in 'AddressesParseJob'
        $addressesAdded = [
            [
                'region_id' => 0,
                'id_code' => '1838112318',
                'address' => 'Ізмаільський р-н, с. Утконосівка, вул. Шевченка, 41, магазин',
                'lng' => null,
                'lat' => null,
                'license' => '161516640360',
                'company' => 'Чепой Григорій Григорович',
                'company_type' => 'магазин',
                'license_start_at' => '2018-02-10',
                'license_end_at' => '2018-02-04',
                'license_type' => Address::TYPE_BEER,
                'status' => Address::STATUS_ACTIVE,
                'comment' => null,
            ],
            [
                'region_id' => 0,
                'id_code' => '1838112318',
                'address' => 'Ізмаільський р-н, с. Утконосівка, вул. Шевченка, 41, магазин',
                'lng' => null,
                'lat' => null,
                'license' => '161516660253',
                'company' => 'Чепой Григорій Григорович',
                'company_type' => 'магазин',
                'license_start_at' => '2018-02-10',
                'license_end_at' => '2018-02-04',
                'license_type' => Address::TYPE_TOBACCO,
                'status' => Address::STATUS_ACTIVE,
                'comment' => null,
            ],
        ];

        // Don't merge with "$addressesAdded" it's must be using only in method "assertAddressesRemoved"
        Address::insert(array_merge($addressesDontTouch, $addressesWillBeRemoved));

        /** @var AddressImport $addressImport */
        $addressImport = factory(AddressImport::class)->create([
            'type' => 'licenses',
            'uploaded_filename' => 'licenses.xlsx',
            'storage_filename' => __DIR__ . '/../stubs/licenses.xlsx',
            'meta_data' => json_encode([]),
            'configs' => json_encode([]),
            'parsed_at' => null,
            'geocoded_at' => null,
        ]);

        Event::fake();

        try {
            (new ParseAddresses($addressImport))->handle();
        } catch (Exception $e) {
            $this->fail("Job failed with exception: {$e->getMessage()}");
        } catch (\PhpOffice\PhpSpreadsheet\Exception $e) {
            $this->fail("Job failed with exception: {$e->getMessage()}");
        }

        $this->assertAddressImportParsedAt($addressImport);

        $this->assertAddressesDontTouch($addressesDontTouch);
        $this->assertAddressesAdded($addressesAdded);
        $this->assertAddressesRemoved($addressesWillBeRemoved);

        Event::assertDispatched(AddressesParsed::class, function ($e) use ($addressImport) {
            /** @var AddressesParsed $e */
            return count(array_diff_assoc($e->addressImport->toArray(), $addressImport->toArray())) == 0;
        });
    }

    /**
     * Assert addresses don't touch entities.
     *
     * @param array $addresses
     */
    protected function assertAddressesDontTouch(array $addresses)
    {
        foreach ($addresses as $address) {
            $this->assertDatabaseHas('addresses', $address);
        }
    }

    /**
     * Assert addresses added entities.
     *
     * @param array $addresses
     */
    protected function assertAddressesAdded(array $addresses)
    {
        foreach ($addresses as $address) {
            $this->assertDatabaseHas('addresses', $address);
        }
    }

    /**
     * Assert addresses removed entities.
     *
     * @param array $addresses
     */
    protected function assertAddressesRemoved(array $addresses)
    {
        foreach ($addresses as $address) {
            $this->assertDatabaseMissing('addresses', $address);
        }
    }

    /**
     * Assert address import parsed at updated.
     *
     * @param AddressImport $addressImport
     */
    protected function assertAddressImportParsedAt($addressImport)
    {
        $this->assertNotNull($addressImport->refresh()->parsed_at, 'Address import "parsed_at" must be set.');
    }
}
