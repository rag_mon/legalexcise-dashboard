<?php

namespace Modules\Dashboard\Tests\Jobs;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Event;
use Modules\Dashboard\Events\Address\AddressesGeocoded;
use Modules\Dashboard\Jobs\GeocodingAddresses;
use Modules\Dashboard\Models\Address;
use Modules\Dashboard\Models\AddressImport;
use Tests\TestCase;

class GeocodingAddressesTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp()
    {
        parent::setUp();

        Address::truncate();
    }

    /**
     * Test the job.
     *
     * @return void
     */
    public function testRun()
    {
        $addressesData = [
            [
                'region_id' => 0,
                'id_code' => '2550305806',
                'address' => 'Ізмаільський р-н, с. Першотравневе, вул. Шкільна, 100, магазин-кафетерій "Наталі"',
                'lng' => null,
                'lat' => null,
                'license' => '151516640311',
                'company' => 'Чумаченко Наталія Дмитрівна',
                'company_type' => 'магазин-кафетерій "Наталі"',
                'license_start_at' => '2018-02-10',
                'license_end_at' => '2017-01-30',
                'license_type' => Address::TYPE_ALCOHOL,
                'status' => Address::STATUS_ACTIVE,
                'comment' => null,
            ],
            [
                'region_id' => 0,
                'id_code' => '1838112318',
                'address' => 'Ізмаільський р-н, с. Утконосівка, вул. Шевченка, 41, магазин',
                'lng' => null,
                'lat' => null,
                'license' => '161516640360',
                'company' => 'Чепой Григорій Григорович',
                'company_type' => 'магазин',
                'license_start_at' => '2018-02-10',
                'license_end_at' => '2018-02-04',
                'license_type' => Address::TYPE_BEER,
                'status' => Address::STATUS_ACTIVE,
                'comment' => null,
            ],
            [
                'region_id' => 0,
                'id_code' => '1838112318',
                'address' => 'Ізмаільський р-н, с. Утконосівка, вул. Шевченка, 41, магазин',
                'lng' => null,
                'lat' => null,
                'license' => '161516660253',
                'company' => 'Чепой Григорій Григорович',
                'company_type' => 'магазин',
                'license_start_at' => '2018-02-10',
                'license_end_at' => '2018-02-04',
                'license_type' => Address::TYPE_TOBACCO,
                'status' => Address::STATUS_ACTIVE,
                'comment' => null,
            ],
        ];
        /** @var Collection $addresses */
        Address::insert($addressesData);
        /** @var AddressImport $addressImport */
        $addressImport = factory(AddressImport::class)->make([
            'type' => 'licenses',
            'uploaded_filename' => 'licenses.xlsx',
            'storage_filename' => __DIR__ . '/../stubs/licenses.xlsx',
            'meta_data' => json_encode([]),
            'configs' => json_encode([]),
            'parsed_at' => null,
            'geocoded_at' => null,
        ]);

        Event::fake();

        (new GeocodingAddresses($addressImport))->handle();

        $this->assertAddressesCoordinates($addressesData);

        Event::assertDispatched(AddressesGeocoded::class, function ($e) use ($addressImport) {
            /** @var AddressesGeocoded $e */
            return count(array_diff_assoc($e->addressImport->toArray(), $addressImport->toArray())) == 0;
        });
    }

    /**
     * Assert addresses coordinates is set (lat, lng).
     *
     * @param array $addresses
     */
    protected function assertAddressesCoordinates(array $addresses)
    {
        foreach ($addresses as $address) {
            /** @var Address $address */
            $this->assertDatabaseMissing('addresses', array_merge(
                $address,
                [
                    'lat' => null,
                    'lng' => null,
                ]
            ));
        }
    }
}
