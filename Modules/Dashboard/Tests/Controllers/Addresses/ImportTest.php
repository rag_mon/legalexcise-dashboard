<?php

namespace Modules\Dashboard\Tests\Controllers\Addresses;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Event;
use Modules\Dashboard\Events\Address\AddressesUploaded;
use Modules\Dashboard\Models\User;
use Tests\TestCase;

class ImportTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test address import create.
     */
    public function testCreate()
    {
        /** @var User $user */
        $user = factory(User::class)->create();

        $this->be($user);

        $response = $this->get('/imports/create');

        $response
            ->assertStatus(Response::HTTP_OK)
            ->assertViewIs('dashboard::address.import.create');
    }

    /**
     * Test store licenses.
     */
    public function testStoreLicenses()
    {
        $name = 'licenses.xlsx';
        $path = __DIR__ . '/../../stubs/licenses.xlsx';
        $mimeType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
        $addressImport = [
            'type' => 'licenses',
            'uploaded_file' => new UploadedFile($path, $name, $mimeType, filesize($path), null, true),
        ];
        /** @var User $user */
        $user = factory(User::class)->create();

        Event::fake();

        $this->be($user);

        $response = $this->post('/imports', $addressImport);

        $response
            ->assertStatus(Response::HTTP_FOUND)
            ->assertRedirect('/imports/create');

        $this->assertDatabaseHas('address_imports', [
            'uploaded_filename' => $addressImport['uploaded_file']->getClientOriginalName(),
            'type' => 'licenses',
        ]);

        Event::assertDispatched(AddressesUploaded::class, function ($e) use ($addressImport) {
            /** @var AddressesUploaded $e */
            return $e->addressImport->uploaded_filename == $addressImport['uploaded_file']->getClientOriginalName()
                && $e->addressImport->type == 'licenses';
        });
    }

    /**
     * Test store suspicious.
     */
    public function testStoreSuspicious()
    {
        $name = 'suspicious.xlsx';
        $path = __DIR__ . '/../../stubs/suspicious.xlsx';
        $mimeType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
        $addressImport = [
            'type' => 'suspicious',
            'uploaded_file' => new UploadedFile($path, $name, $mimeType, filesize($path), null, true),
        ];
        /** @var User $user */
        $user = factory(User::class)->create();

        Event::fake();

        $this->be($user);

        $response = $this->post('/imports', $addressImport);

        $response
            ->assertStatus(Response::HTTP_FOUND)
            ->assertRedirect('/imports/create');

        $this->assertDatabaseHas('address_imports', [
            'uploaded_filename' => $addressImport['uploaded_file']->getClientOriginalName(),
            'type' => 'suspicious',
        ]);

        Event::assertDispatched(AddressesUploaded::class, function ($e) use ($addressImport) {
            /** @var AddressesUploaded $e */
            return $e->addressImport->uploaded_filename == $addressImport['uploaded_file']->getClientOriginalName()
                && $e->addressImport->type == 'suspicious';
        });
    }
}
