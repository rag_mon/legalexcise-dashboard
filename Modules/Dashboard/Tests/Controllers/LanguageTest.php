<?php

namespace Modules\Dashboard\Tests\Controllers;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Modules\Dashboard\Models\User;
use Tests\TestCase;

class LanguageTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test get current locale value.
     */
    public function testGetCurrentLocale()
    {
        $locale = 'ru';

        /** @var User $user */
        $user = factory(User::class)->create(compact('locale'));

        $this->be($user);

        $response = $this->get('/locale');

        $response
            ->assertStatus(Response::HTTP_OK)
            ->assertSeeText($locale);
    }

    /**
     * Test get support locales list values.
     */
    public function testGetSupportList()
    {
        /** @var User $user */
        $user = factory(User::class)->create();

        $this->be($user);

        App::setLocale('ru');

        $response = $this->get('/lang_list');

        $response
            ->assertStatus(Response::HTTP_OK)
            ->assertJson(['ru', 'uk']);
    }

    /**
     * Test change current locale.
     */
    public function testChangeLanguage()
    {
        /** @var User $user */
        $user = factory(User::class)->create();

        $this->be($user);

        $locale = 'ru';

        $response = $this->post('/change_lang', [
            'lang' => $locale,
        ]);

        $response
            ->assertStatus(Response::HTTP_OK)
            ->assertJson([]);

        $this->assertLocale($locale);
    }

    /**
     * Assert current application locale.
     *
     * @param string $locale
     */
    protected function assertLocale($locale)
    {
        $currentLocale = App::getLocale();

        $this->assertEquals($locale, App::getLocale(), "Current application locale \"{$currentLocale}\" is not equal to actual \"{$locale}\".");
    }
}
