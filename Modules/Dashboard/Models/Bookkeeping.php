<?php

namespace Modules\Dashboard\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Bookkeeping
 *
 * @package Modules\Dashboard\Models
 *
 * @property int id
 * @property int examination_of_excisable_goods
 * @property int transferred_to_the_oy
 * @property int sale_to_minors
 * @property Carbon created_at
 * @property Carbon updated_at
 *
 * @method $this actual
 */
class Bookkeeping extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'bookkeeping';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'examination_of_excisable_goods',
        'transferred_to_the_oy',
        'sale_to_minors',
    ];

    /**
     * Scope query to include only actual (last) row data.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActual($query)
    {
        return $query->orderByDesc('created_at');
    }
}
