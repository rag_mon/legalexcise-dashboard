<?php

namespace Modules\Dashboard\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ActionTaken
 *
 * @package Modules\Dashboard\Models
 *
 * @property int id
 * @property string resource_type
 * @property int resource_id
 * @property int confiscated_goods
 * @property Carbon protocol_drawn_up
 * @property int financial_sanctions
 * @property Carbon created_at
 * @property Carbon updated_at
 *
 * @method addresses
 * @method complaints
 */
class ActionTaken extends Model
{
    const PROTOCOL_DRAWN_UP_DATE_VIEW_FORMAT = 'd.m.Y';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'action_taken';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'confiscated_goods',
        'protocol_drawn_up',
        'financial_sanctions',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['protocol_drawn_up'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function taking()
    {
        return $this->morphTo();
    }

    /**
     * Get action keys.
     *
     * @return array
     */
    public static function getActionKeys()
    {
        return [
            'confiscated_goods',
            'protocol_drawn_up',
            'financial_sanctions',
        ];
    }

    public function scopeAddresses($query)
    {
        return $query->where('resource_type', 'addresses');
    }

    public function scopeComplaints($query)
    {
        return $query->where('resource_type', 'complaints');
    }
}
