<?php

namespace Modules\Dashboard\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Report
 *
 * @package Modules\Dashboard\Models
 *
 * @property int id
 * @property int user_id
 * @property string name
 * @property string filename
 * @property string status
 * @property Carbon from_dt
 * @property Carbon to_dt
 * @property Carbon created_at
 * @property User|null user
 * @property string download_url
 */
class Report extends Model
{
    const VIEW_DATETIME_FORMAT = 'd.m.Y H:i:s';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'filename',
        'from_dt',
        'to_dt',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'download_url'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'from_dt', 'to_dt'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('Modules\Dashboard\Models\Report');
    }

    /**
     * Get download url attribute value.
     *
     * @return string
     */
    public function getDownloadUrlAttribute()
    {
        return route('dashboard.report.download', [$this->getKey()]);
    }
}
