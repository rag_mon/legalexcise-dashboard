<?php

namespace Modules\Dashboard\Models;

/**
 * Interface IActionTaken
 *
 * @package Modules\Dashboard\Models
 */
interface IActionTaken
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function actionTaken();
}