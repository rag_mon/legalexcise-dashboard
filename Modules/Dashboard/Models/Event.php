<?php

namespace Modules\Dashboard\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Event of history
 *
 * @package Modules\Dashboard\Models
 *
 * @property int id
 * @property int user_id
 * @property string type
 * @property string lang_key
 * @property array data
 * @property User user
 * @property Carbon created_at
 * @property string message
 *
 * @method Builder type
 */
class Event extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'lang_key',
        'data',
        'user_id',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('Modules\Dashboard\Models\User');
    }

    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $type
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeType($query, $type)
    {
        return $query->where('type', $type);
    }

    /**
     * Get data attribute value.
     *
     * @param string $value
     * @return array|null
     */
    public function getDataAttribute($value)
    {
        return isset($value) ? json_decode($value, true) : null;
    }

    /**
     * Set data attribute value.
     *
     * @param array|object|string $value
     */
    public function setDataAttribute($value)
    {
        if (is_array($value) || is_object($value)) {
            $this->attributes['data'] = json_encode($value);
        } else {
            $this->attributes['data'] = $value;
        }
    }

    /**
     * Get message attribute value.
     *
     * @return null|string
     */
    public function getMessageAttribute()
    {
        return __($this->lang_key, $this->data);
    }
}
