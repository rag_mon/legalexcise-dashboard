<?php

namespace Modules\Dashboard\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AddressComment
 *
 * @package Modules\Dashboard\Models
 *
 * @property int id
 * @property string message
 * @property Carbon created_at
 * @property Carbon updated_at
 * @property Address address
 */
class AddressComment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'message',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function address()
    {
        return $this->belongsTo('Modules\Dashboard\Models\Address');
    }
}
