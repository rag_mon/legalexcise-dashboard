<?php

namespace Modules\Dashboard\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AddressImport
 *
 * @package Modules\Dashboard\Models
 *
 * @property int id
 * @property string type
 * @property string uploaded_filename
 * @property string storage_filename
 * @property string|array meta_data
 * @property string|array configs
 * @property Carbon created_at
 * @property Carbon parsed_at
 * @property Carbon geocoded_at
 * @property string type_title
 *
 * @method $this parsed
 * @method $this geocoded
 */
class AddressImport extends Model
{
    /**
     * Types
     */
    const TYPE_LICENSES = 'licenses';
    const TYPE_SUSPICIOUS = 'suspicious';

    public static $types = [
        self::TYPE_LICENSES,
        self::TYPE_SUSPICIOUS,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'uploaded_filename',
        'storage_filename',
        'meta_data',
        'configs',
        'parsed_at',
        'geocoded_at',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'parsed_at',
        'geocoding_at',
    ];

    /**
     * Get meta_data json format string decoded to associative array.
     *
     * @param string $value
     * @return array
     */
    public function getMetaDataAttribute($value)
    {
        return json_decode($value, true);
    }

    /**
     * Set meta_data attribute value.
     *
     * @param $value
     */
    public function setMetaDataAttribute($value)
    {
        $this->attributes['meta_data'] = json_encode($value, JSON_UNESCAPED_UNICODE);
    }

    /**
     * Get configs json format string decoded to associative array.
     *
     * @param string $value
     * @return array
     */
    public function getConfigsAttribute($value)
    {
        return json_decode($value, true);
    }

    /**
     * Set meta_data attribute value.
     *
     * @param $value
     */
    public function setConfigsAttribute($value)
    {
        $this->attributes['configs'] = json_encode($value, JSON_UNESCAPED_UNICODE);
    }

    /**
     * Scope query to include only parsed imports.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param bool $isParsed
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeParsed($query, $isParsed = true)
    {
        return $isParsed ? $query->whereNotNull('parsed_at') : $query->whereNull('parsed_at');
    }

    /**
     * Scope query to include only geocoded imports.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param bool $isGeocoded
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGeocoded($query, $isGeocoded = true)
    {
        return $isGeocoded ? $query->whereNotNull('geocoded_at') : $query->whereNull('geocoded_at');
    }

    /**
     * Get type title attribute value.
     *
     * @return null|string
     */
    public function getTypeTitle()
    {
        return __("dashboard::address.import.types.{$this->attributes['type']}");
    }
}
