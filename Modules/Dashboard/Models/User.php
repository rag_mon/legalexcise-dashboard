<?php

namespace Modules\Dashboard\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

/**
 * Class User
 *
 * @package Modules\Dashboard\Models
 *
 * @property int id
 * @property string name
 * @property string email
 * @property string password
 * @property string configs
 * @property bool is_notify
 * @property string locale
 * @property string remember_token
 * @property Carbon created_at
 * @property Carbon updated_at
 * @property Collection events
 * @property Collection roles
 */
class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'locale',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function events()
    {
        return $this->hasMany('Modules\Dashboard\Models\Event');
    }
}
