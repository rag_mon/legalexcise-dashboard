<?php

namespace Modules\Dashboard\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AddressUpdated
 *
 * @package Modules\Dashboard\Models
 *
 * @property int id
 * @property Carbon timestamp
 */
class AddressUpdated extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'address_updated';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['timestamp'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['timestamp'];
}
