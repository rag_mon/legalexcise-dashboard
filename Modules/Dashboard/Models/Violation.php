<?php

namespace Modules\Dashboard\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Violation
 *
 * @package Modules\Dashboard\Models
 *
 * @property int id
 * @property string name
 * @property string description
 */
class Violation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
    ];
}
