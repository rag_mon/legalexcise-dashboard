<?php

namespace Modules\Dashboard\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Class Complaint
 *
 * @package App\Models
 *
 * @property int id
 * @property string type
 * @property string name
 * @property string telephone
 * @property string company
 * @property string email
 * @property string message
 * @property string address
 * @property integer address_id
 * @property float lat
 * @property float lng
 * @property Carbon geocoding_at
 * @property Address addressEntry
 * @property Carbon created_at
 * @property string mapLink
 * @property bool is_anonymously
 * @property string status
 * @property string to_code
 * @property string np_code
 * @property string dfs_department_code
 * @property string dfs_executor
 * @property Collection actionTaken
 * @property string violations
 *
 * @method $this notProcessed
 * @method $this processed
 * @method $this verificationPossible
 * @method $this verificationImpossible
 */
class Complaint extends Model implements IActionTaken
{
    const STATUS_PROCESSED = 'processed';
    const STATUS_NOT_PROCESSED = 'not_processed';
    const STATUS_TRANSFERRED_TO_OY = 'transferred_to_oy';
    const STATUS_VERIFICATION_POSSIBLE = 'verification_possible';
    const STATUS_VERIFICATION_IMPOSSIBLE = 'verification_impossible';
    const STATUS_IN_PROCESSING = 'in_processing';

    /**
     * Support status list.
     *
     * @var array
     */
    public static $statuses = [
        self::STATUS_PROCESSED,
        self::STATUS_NOT_PROCESSED,
        self::STATUS_TRANSFERRED_TO_OY,
        self::STATUS_VERIFICATION_POSSIBLE,
        self::STATUS_VERIFICATION_IMPOSSIBLE,
        self::STATUS_IN_PROCESSING,
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'verified_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'name',
        'telephone',
        'company',
        'email',
        'message',
        'is_anonymously',
        'address',
        'lng',
        'lat',
        'to_code',
        'np_code',
        'dfs_department_code',
        'dfs_executor',
        'status',
        'spd_name',
        'spd_code',
        'spd_address',
        'spd_license',
        'verified_at',
        'violations',
        'protocol',
        'comment',
    ];

//    /**
//     * @return \Illuminate\Database\Eloquent\Relations\HasMany
//     */
//    public function images()
//    {
//        return $this->hasMany('Modules\\Dashboard\\Models\\Image');
//    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function addressEntry()
    {
        return $this->belongsTo('Modules\\Dashboard\\Models\\Address', 'address_id');
    }

    /**
     * Return is complaint with geo coordinates.
     *
     * @return bool
     */
    public function isWithCoordinates()
    {
        return isset($this->attributes['lat'], $this->attributes['lng']);
    }

    /**
     * @return string
     */
    public function getMapLinkAttribute()
    {
        return "https://maps.google.com/maps?q={$this->lat},{$this->lng}";
    }

    // ----------------------- ## END of backend copy-past ------------------------------------------------------------

    /**
     * Scope query to include only not processed addresses.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNotProcessed($query)
    {
        return $query->where('status', self::STATUS_NOT_PROCESSED);
    }

    /**
     * Scope query to include only processed addresses.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeProcessed($query)
    {
        return $query->where('status', self::STATUS_PROCESSED);
    }

    /**
     * Scope query to include only verification possible complaints.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeVerificationPossible($query)
    {
        return $query->where('status', 'verification_possible');
    }

    /**
     * Scope query to include only verification impossible complaints.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeVerificationImpossible($query)
    {
        return $query->where('status', 'verification_impossible');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function actionTaken()
    {
        return $this->morphMany('Modules\\Dashboard\\Models\\ActionTaken', 'resource');
    }

    public function inViolations($violationId)
    {
        return in_array($violationId, explode(';', $this->violations));
    }
}
