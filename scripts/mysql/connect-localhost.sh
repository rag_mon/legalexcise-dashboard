#!/usr/bin/env bash

echo "Connect to mysql (localhost)..."
echo "NOTIFICATION: Default docker user/password: legalexcise/legalexcise"
mysql -h 127.0.0.1 -u legalexcise -plegalexcise legalexcise
echo "Connection to database is terminated."