#!/usr/bin/env bash

echo "Connect to mysql (localhost)..."
echo "NOTIFICATION: Default docker user/password: legalexcise/legalexcise"
mysql -h 127.0.0.1 -u legalexcise -plegalexcise -Nse '; show tables' legalexcise \
    | while read table; \
    do mysql -h 127.0.0.1 -u legalexcise -plegalexcise -e "SET FOREIGN_KEY_CHECKS=0; truncate table $table" legalexcise; \
    done
echo "Database tables truncation is complete."