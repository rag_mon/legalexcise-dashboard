# Legalexcise dashboard application
Version 1.0 (release)

> Warning: It's only the dashboard for "Legalexcise" project.<br>
> This project can't working correct without other parts (services) of "Legalexcise" (see below).<br>
> If you want to run complete dashboard application - you also need run other parts of project.
 
## Project services
- [+] **Dashboard application**
- [-] Backend API
- [-] Client application (phonegap application)

## Micro-services
- queue-worker
- database (linked to `legalexcise_database`)

## Administration capabilities
* addresses
* addresses action taken
* complaints
* dashboard users
* addresses map update release
* addresses imports
