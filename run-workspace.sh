#!/usr/bin/env bash

# Make sure only root can run our script
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

# Check is other critical parts of project is running
# TODO: complete this verification script part

echo "Switch CLI I/O to legalexcise_dashboard_workspace_1 container..."
docker exec -it legalexcise_dashboard_workspace_1 bash